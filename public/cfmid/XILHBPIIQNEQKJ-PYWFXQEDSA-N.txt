#In-silico ESI-MS/MS [M+H]+ Spectra
#PREDICTED BY CFM-ID 4.0.8
#SMILES=c12c(C(C(C(C1)O)O)O)c1c3c(c2Nc2[nH]c(=O)c4c(n2)n(cn4)[C@H]2C[C@H]([C@H](O2)CO)O)ccc2c3c(cc1)ccc2N
#InChiKey=XILHBPIIQNEQKJ-PYWFXQEDSA-N
energy0
451.15131 100.00 10 2 55 61 (44.111 13.32 4.6113 0.023337)
469.16188 41.00 54 90 63 75 104 89 126 131 88 147 116 135 152 142 110 146 109 143 121 138 141 145 155 154 156 (24.494 0.26951 0.19205 0.14821 0.072268 0.06338 0.042865 0.029406 0.02785 0.020302 0.017707 0.015112 0.010251 0.0093077 0.0084726 0.0050886 0.0047704 0.0040278 0.0040152 0.0037404 0.002013 0.00092696 0.00085951 0.00016106 7.6486e-05)
energy1
451.15131 100.00 2 10 55 61 (52.493 7.6062 6.8431 0.11035)
469.16188 20.65 54 63 75 90 131 88 126 135 89 147 121 116 104 138 109 110 146 143 152 142 141 145 154 156 155 (12.167 0.36218 0.25381 0.21425 0.16801 0.14127 0.13836 0.094216 0.077345 0.067711 0.038178 0.032861 0.026148 0.015434 0.014281 0.0129 0.0061539 0.0055372 0.0029887 0.0029376 0.0024139 0.002008 0.001535 0.00098799 0.00017585)
energy2
29.03858 3.24 288 (0.72603)
45.03349 17.32 40 (3.8827)
55.01784 10.86 278 (2.4341)
57.03349 15.21 47 (3.4101)
59.04914 7.31 277 (1.6396)
73.02841 7.01 214 43 (1.3571 0.21444)
101.05971 3.49 266 (0.78229)
152.05669 4.52 15 (1.014)
317.12845 5.69 20 21 (1.0749 0.2004)
333.12337 4.88 18 (1.0953)
335.13902 12.48 19 22 23 24 (2.6175 0.062818 0.059834 0.058039)
365.11454 4.04 100 (0.90546)
379.13019 4.58 95 (1.0261)
409.14075 12.82 78 69 (1.877 0.9975)
427.14008 3.35 129 (0.75099)
439.15131 5.47 66 83 64 (0.77579 0.33235 0.11807)
451.15131 51.39 2 10 61 55 (8.7123 1.6885 0.84122 0.28236)
452.13533 10.42 62 127 134 105 115 (2.053 0.13133 0.074519 0.043097 0.035248)
453.16696 13.58 3 11 29 8 (1.894 0.63022 0.39835 0.12219)
469.16188 100.00 54 90 63 75 131 138 121 116 104 110 147 126 135 88 109 152 142 89 156 154 141 145 155 143 146 (5.9434 3.28 2.7094 1.9009 1.4144 0.95483 0.93911 0.74415 0.69448 0.62832 0.59278 0.46977 0.41285 0.24422 0.24187 0.18469 0.15103 0.14458 0.13291 0.12659 0.125 0.1237 0.099164 0.096123 0.069204)
473.19318 4.33 158 (0.97113)
475.20883 4.03 159 (0.90314)
479.18262 3.54 251 250 (0.44855 0.3455)
483.17753 3.71 242 217 (0.77668 0.055143)
495.17753 5.90 248 246 247 212 190 (0.45495 0.37586 0.2603 0.2 0.032477)
497.15679 3.84 241 (0.86)
499.17244 3.43 240 (0.7698)
501.22448 3.68 209 255 (0.51605 0.30886)
525.18809 7.97 297 260 261 170 259 296 258 192 184 191 183 (0.61051 0.26341 0.23176 0.1654 0.15071 0.10864 0.10478 0.082998 0.057468 0.0071 0.0048518)
549.18809 9.50 267 5 171 165 (0.94344 0.66126 0.50633 0.01916)

0 585.2092236881 Nc1ccc2ccc3c4c(c([NH2+]c5nc6c(ncn6C6CC(O)C(CO)O6)c(=O)[nH]5)c5ccc1c2c53)CC(O)C(O)C4O
1 567.1986590041 NC1CCC2CCC3=C4C(=C([NH+]=C5N=C(O)C6=C(N5)N(C5=CC(O)=C(C=O)O5)C=N6)C5=CC=C(O)C(O)=C53)C=CC1C42
2 451.1513148881 NC1C=CC2=CCC3=C4C(=C([NH+]=C5N=C(O)C6=C(N=CN6)N5)C5=CC=C(O)C(O)=C53)C=CC1=C24
3 453.1669649521 NC1CCC2=CCC3=C4C(=C([NH+]=C5N=C(O)C6=C(N=CN6)N5)C5=CC=C(O)C(O)=C53)C=CC1=C24
4 567.1986590041 NC1CCC2CCC3=C4C(=C([NH+]=C5N=C(O)C6=C(N5)N(C(O)=C=C(O)C#CO)C=N6)C5=CC=C(O)C(O)=C53)C=CC1C42 Intermediate Fragment 
5 549.1880943201 NC1CCC2CCC3=C4C(=C([NH+]=C5N=C(O)C6=C(N5)N(C#CC(O)=C=C=O)C=N6)C5=CC=C(O)C(O)=C53)C=CC1C42
6 567.1986590041 NC1CCC2CCC3=C4C(=C([NH+]=C5N=C(O)C6=C(N5)N(C(=C=C=O)OC#CO)C=N6)C5=CC=C(O)C(O)=C53)CCC1C42 Intermediate Fragment 
7 567.1986590041 NC1CCC2CCC3=C4C(=C([NH+]=C5N=C(O)C6=C(N5)N(C5=CC(O)=C(C=O)O5)C=N6)C5=CC(O)=CC(O)=C53)C=CC1C42
8 453.1669649521 NC1CCC2=CCC3=C4C(=C([NH+]=C5N=C(O)C6=C(N=CN6)N5)C5=CC(O)=CC(O)=C53)C=CC1=C24
9 567.1986590041 NC1CCC2CCC3=C4C(=C([NH+]=C5N=C(O)C6=C(N5)N(C5=CC(O)=C(C=O)O5)C=N6)C5=CC(O)=C(O)C=C53)C=CC1C42
10 451.1513148881 NC1C=CC2=CCC3=C4C(=C([NH+]=C5N=C(O)C6=C(N=CN6)N5)C5=CC(O)=C(O)C=C53)C=CC1=C24
11 453.1669649521 NC1CCC2=CCC3=C4C(=C([NH+]=C5N=C(O)C6=C(N=CN6)N5)C5=CC(O)=C(O)C=C53)C=CC1=C24
12 567.1986590041 NC1CCC2CCC3=C4C(=C([NH+]=C5N=C(O)C6=C(N5)N(C(O)=C=C(O)C#CO)C=N6)C5=CC(O)=C(O)C=C53)C=CC1C42 Intermediate Fragment 
13 268.1040303481 [NH2+]=C1N=C(O)C2=C(N1)N(C1CC(O)C(CO)O1)C=N2
14 150.0410361681 [NH2+]=C1N=C(O)C2=NC=NC2=N1
15 152.0566862321 [NH2+]=C1N=C(O)C2NC=NC2=N1
16 268.1040303481 [NH2+]=C1N=C(O)C2=C(N1)N(C=CC(O)C(O)CO)C=N2 Intermediate Fragment 
17 331.1077187601 NC1CCC2=C3C1=CC=C1C(=[NH2+])C4=C(C(=O)C(=O)C(O)=C4)C(=C13)C=C2
18 333.1233688241 NC1CCC2=C3C1=CC=C1C(=[NH2+])C4=C(C(=O)C(=O)C(O)=C4)C(=C13)CC2
19 335.1390188881 NC1CCC2CCC3=C4C(=CC=C1C42)C(=[NH2+])C1=C3C(=O)C(=O)C(O)=C1
20 317.1284542041 NC1C=CC2=CCC3=C4C(=C([NH3+])C5=CC=C(O)C(O)=C53)C=CC1=C24
21 317.1284542041 NC1C=CC2=CCC3=C4C(=C([NH3+])C5=CC(O)=C(O)C=C53)C=CC1=C24
22 335.1390188881 NC1CCC2=CCC3=C4C(=C([NH3+])C(CC(O)=C=O)=C3C=O)C=CC1=C24 Intermediate Fragment 
23 335.1390188881 NC1CCC2CCC3=C4C(=C([NH3+])C(C=C=O)=C3C(O)=C=O)C=CC1=C42 Intermediate Fragment 
24 335.1390188881 NC1CCC2=CCC3=C4C(=C([NH3+])C(C=C(O)C(O)=C=O)=C3)C=CC1=C24 Intermediate Fragment 
25 337.1546689521 NC1CCC2CCC3=C4C(=CCC1C42)C(=[NH2+])C1=C3C(=O)C(=O)C(O)=C1
26 339.1703190161 NC1CCC2CCC3=C4C(CCC1C42)C(=[NH2+])C1=C3C(=O)C(=O)C(O)=C1
27 341.1859690801 NC1CCC2CCC3=C4C(CCC1C42)C([NH3+])C1=C3C(=O)C(=O)C(O)=C1
28 567.1986590041 NC1CCC2CCC3=C4C(=C([NH+]=C5N=CC6=C(N5)N(C5=CC(O)=C(C=O)O5)C=N6)C5=CC(O)=C(O)C(O)=C53)C=CC1C42
29 453.1669649521 NC1CCC2=CCC3=C4C(=C([NH+]=C5N=CC6=C(N=CN6)N5)C5=CC(O)=C(O)C(O)=C53)C=CC1=C24
30 119.0702706321 OC1CCOC1C[OH2+]
31 119.0702706321 C=CC(O)C(O)C[OH2+] Intermediate Fragment 
32 117.0546205681 OC1C=COC1C[OH2+]
33 85.0284058201 [OH2+]C1=COC=C1
34 87.0440558841 [OH2+]C1C=COC1
35 89.0597059481 [OH2+]C1CCOC1
36 99.0440558841 CC1=C([OH2+])C=CO1
37 99.0440558841 [OH2+]CC1=CC=CO1
38 117.0546205681 C=C(O)C(=C[OH2+])OC Intermediate Fragment 
39 43.0178411361 C#C[OH2+]
40 45.0334912001 C=C[OH2+]
41 117.0546205681 C=COC(=CO)C[OH2+] Intermediate Fragment 
42 89.0597059481 C=C[OH+]CCO
43 73.0284058201 OC#CC[OH2+]
44 75.0440558841 OC=CC[OH2+]
45 91.0389705041 OC=C([OH2+])CO
46 117.0546205681 OC=C=COCC[OH2+] Intermediate Fragment 
47 57.0334912001 C#CC[OH2+]
48 117.0546205681 OC=C=C(O)CC[OH2+] Intermediate Fragment 
49 89.0597059481 C=C(O)CC[OH2+]
50 99.0440558841 C#CC(O)=CC[OH2+]
51 117.0546205681 C=C=C(O)C(O)C[OH2+] Intermediate Fragment 
52 115.0389705041 OC1=C(C[OH2+])OC=C1
53 467.1462295081 NC1C=CC2=CCC3=C4C(=C([NH+]=C5N=C(O)C6=C(N=CN6)N5)C5=CC(O)=C(O)C(O)=C53)C=CC1=C24
54 469.1618795721 NC1CCC2=CCC3=C4C(=C([NH+]=C5N=C(O)C6=C(N=CN6)N5)C5=CC(O)=C(O)C(O)=C53)C=CC1=C24
55 451.1513148881 NC1C=CC2=CCC3=C4C(=C([NH+]=C5N=C(O)C6=C(N=CN6)N5)C5=CC(O)=CC(O)=C53)C=CC1=C24
56 156.0879863601 [NH2+]=C1N=C(O)C2NCNC2N1
57 320.1281198561 [NH3+]C1CCC2CCC3=C4C(=CC5=C3C(=O)C(=O)C(O)=C5)C=CC1=C42
58 137.0457872001 [OH+]=C1N=CN=C2N=CNC12
59 135.0301371361 [OH+]=C1N=CN=C2N=CN=C12
60 327.0764186321 N=C1C=CC2=C3C1=CC=C1C(=[NH2+])C4=C(C(=O)C(=O)C(O)=C4)C(=C13)C=C2
61 451.1513148881 NC1C=CC2=CCC3=C4C(=C([NH+]=C5N=CC6=C(N=CN6)N5)C5=CC(O)=C(O)C(O)=C53)C=CC1=C24
62 452.1353304761 OC1=NC(=[NH+]C2=C3C=CC4=C5C(=CCC(=C35)C3=C(O)C(O)=C(O)C=C23)C=CC4)NC2=C1NC=N2
63 469.1618795721 NC1CCC2=CCC3=C4C(=C([NH+]=C5N=C(O)C6=C(N=CN6)N5)C(CC(O)=C=O)=C3C=O)C=CC1=C24 Intermediate Fragment 
64 439.1513148881 NC1C=CC2=CCC3=C4C(=C([NH+]=C5N=C(O)C6=C(N=CN6)N5)C(CC(O)=C=O)=C3)C=CC1=C24
65 441.1669649521 NC1CCC2CCC3=C4C(=C([NH+]=C5N=C(O)C6=C(N=CN6)N5)C(C#CO)=C3C=O)C=CC1=C42
66 439.1513148881 NC1CCC2=CCC3=C4C(=C([NH+]=C5N=C(O)C6=C(N=CN6)N5)C(C#CO)=C3C=O)C=CC1=C24
67 413.1720503321 CC1=C(C=O)C2=C3C(=C1[NH+]=C1N=C(O)C4=C(N=CN4)N1)C=CC1=C3C(=CC2)CCC1N
68 411.1564002681 CC1=C(C=O)C2=C3C(=C1[NH+]=C1N=C(O)C4=C(N=CN4)N1)C=CC1=C3C(=CC2)C=CC1N
69 409.1407502041 CC1=C(C=O)C2=C3C(=C1[NH+]=C1N=C(O)C4=C(N=CN4)N1)C=CC1=C3C(=CC2)C=CC1=N
70 407.1251001401 CC1=C(C#[O+])C2=C3C(=C1N=C1N=C(O)C4=C(N=CN4)N1)C=CC1=C3C(=CC2)C=CC1=N
71 395.1251001401 N=C1C=CC2=CCC3=C4C(=C([NH+]=C5N=C(O)C6=C(N=CN6)N5)C=C3C=O)C=CC1=C24
72 393.1094500761 N=C1C=CC2=CCC3=C4C(=C(N=C5N=C(O)C6=C(N=CN6)N5)C=C3C#[O+])C=CC1=C24
73 75.0440558841 CC([OH2+])=CO
74 77.0597059481 CC([OH2+])CO
75 469.1618795721 NC1CCC2CCC3=C4C(=C([NH+]=C5N=C(O)C6=C(N=CN6)N5)C(C=C=O)=C3C(O)=C=O)C=CC1=C42 Intermediate Fragment 
76 63.0440558841 OCC[OH2+]
77 407.1251001401 N=C1C=CC2=CCC3=C4C(=C([NH+]=C5N=C(O)C6=C(N=CN6)N5)C(C#CO)=C3)C=CC1=C24
78 409.1407502041 NC1C=CC2=CCC3=C4C(=C([NH+]=C5N=C(O)C6=C(N=CN6)N5)C(C#CO)=C3)C=CC1=C24
79 411.1564002681 NC1CCC2=CCC3=C4C(=C([NH+]=C5N=C(O)C6=C(N=CN6)N5)C(C#CO)=C3)C=CC1=C24
80 413.1720503321 NC1CCC2CCC3=C4C(=C([NH+]=C5N=C(O)C6=C(N=CN6)N5)C(C#CO)=C3)C=CC1=C42
81 437.1356648241 NC1C=CC2=CCC3=C4C(=C([NH+]=C5N=C(O)C6=C(N=CN6)N5)C(C#CO)=C3C=O)C=CC1=C24
82 441.1669649521 CC1=C(C(O)=C=O)C2=C3C(=C1[NH+]=C1N=C(O)C4=C(N=CN4)N1)C=CC1=C3C(=CC2)CCC1N
83 439.1513148881 CC1=C(C(O)=C=O)C2=C3C(=C1[NH+]=C1N=C(O)C4=C(N=CN4)N1)C=CC1=C3C(=CC2)C=CC1N
84 437.1356648241 CC1=C(C(O)=C=O)C2=C3C(=C1[NH+]=C1N=C(O)C4=C(N=CN4)N1)C=CC1=C3C(=CC2)C=CC1=N
85 425.1356648241 NC1C=CC2=CCC3=C4C(=C([NH+]=C5N=C(O)C6=C(N=CN6)N5)C=C3C(O)=C=O)C=CC1=C24
86 423.1200147601 N=C1C=CC2=CCC3=C4C(=C([NH+]=C5N=C(O)C6=C(N=CN6)N5)C=C3C(O)=C=O)C=CC1=C24
87 467.1462295081 NC1CCC2=CCC3=C4C(=C([NH+]=C5N=C(O)C6=C(N=CN6)N5)C(C#CO)=C3C(O)=C=O)C=CC1=C24
88 469.1618795721 CC1=C(C(O)=C(O)C=O)C2=C3C(=C1[NH+]=C1N=C(O)C4=C(N=CN4)N1)C=CC1=C3C(=CC2)C=CC1N Intermediate Fragment 
89 469.1618795721 C=C(O)C(O)=C(O)C1=CC([NH+]=C2N=C(O)C3=C(N=CN3)N2)=C2C=CC3=C4C(=CCC1=C24)C=CC3N Intermediate Fragment 
90 469.1618795721 NC1CCC2=CCC3=C4C(=C([NH+]=C5N=C(O)C6=C(N=CN6)N5)C(C=C(O)C(O)=C=O)=C3)C=CC1=C24 Intermediate Fragment 
91 437.1356648241 N=C1C=CC2=CCC3=C4C(=C([NH+]=C5N=C(O)C6=C(N=CN6)N5)C(CC(O)=C=O)=C3)C=CC1=C24
92 385.1771357121 CC1=CC2=C3C(=C1[NH+]=C1N=C(O)C4NC=NC4=N1)C=CC1=C3C(=CC2)CCC1N
93 383.1614856481 CC1=CC2=C3C(=C1[NH+]=C1N=C(O)C4NC=NC4=N1)C=CC1=C3C(=CC2)C=CC1N
94 381.1458355841 CC1=CC2=C3C(=C1[NH+]=C1N=C(O)C4NC=NC4=N1)C=CC1=C3C(=CC2)C=CC1=N
95 379.1301855201 CC1=CC2=C3C(=C1[NH+]=C1N=C(O)C4=NC=NC4=N1)C=CC1=C3C(=CC2)C=CC1=N
96 377.1145354561 [CH2+]C1=CC2=C3C(=C1N=C1N=C(O)C4=NC=NC4=N1)C=CC1=C3C(=CC2)C=CC1=N
97 89.0233204401 OC=C(O)C=[OH+]
98 369.1458355841 NC1CCC2=CC=C3C=CC(=[NH+]C4=NC(=O)C5NC=NC5=N4)C4=C3C2=C1C=C4
99 367.1301855201 NC1C=CC2=CC=C3C=CC(=[NH+]C4=NC(=O)C5NC=NC5=N4)C4=C3C2=C1C=C4
100 365.1145354561 N=C1C=CC2=C3C1=CC=C1C(=[NH+]C4=NC(=O)C5NC=NC5=N4)C=CC(=C13)C=C2
101 363.0988853921 N=C1C=CC2=C3C1=CC=C1C(=[NH+]C4=NC(=O)C5=NC=NC5=N4)C=CC(=C13)C=C2
102 105.0546205681 CC([OH2+])C(O)=CO
103 107.0702706321 CC([OH2+])C(O)CO
104 469.1618795721 NC(N=C1N=CNC1=C=O)=[NH+]C1=C2C=CC3=C4C(=CCC(=C24)C2=C(O)C(O)=C(O)C=C12)CCC3N Intermediate Fragment 
105 452.1353304761 NC1C=CC2=CCC3=C4C(=C([NH+]=CN=C5N=CNC5=C=O)C5=CC(O)=C(O)C(O)=C53)C=CC1=C24
106 441.1669649521 NC(N=C1CNC=N1)=[NH+]C1=C2C=CC3=C4C(=CCC(=C24)C2=C(O)C(O)=C(O)C=C12)C=CC3N
107 375.1451668881 N=C(N)[NH+]=C1C2=CC=C3C4=C(CCC(=C24)C2=C1C=C(O)C(=O)C2=O)CCC3N
108 360.1342678561 N=C=[NH+]C1=C2C=CC3=C4C(=CCC(=C24)C2=C(O)C(O)=C(O)C=C12)CCC3N
109 469.1618795721 NC1CCC2=CCC3=C4C(=C([NH+]=C(N=C=O)NC5=CNC=N5)C5=CC(O)=C(O)C(O)=C53)C=CC1=C24 Intermediate Fragment 
110 469.1618795721 NC(N=C(O)C1=CN=CN1)=[NH+]C1=C2C=CC3=C4C(=CCC(=C24)C2=C(O)C(O)=C(O)C=C12)C=CC3N Intermediate Fragment 
111 358.1186177921 NC=[NH+]C1=C2C=CC3=C4C(=CCC(=C24)C2=C1C=C(O)C(=O)C2=O)C=CC3N
112 95.0239891361 [O+]#CC1=CN=CN1
113 399.1087813801 NC(N=C=O)=[NH+]C1=C2C=CC3=C4C(=CCC(=C24)C2=C1C=C(O)C(=O)C2=O)C=CC3N
114 401.1244314441 NC(N=C=O)=[NH+]C1=C2C=CC3=C4C(=CCC(=C24)C2=C1C=C(O)C(=O)C2=O)CCC3N
115 452.1353304761 NC1C=CC2=CCC3=C4C(=C([NH+]=C=NC(=O)C5=CN=CN5)C5=CC(O)=C(O)C(O)=C53)C=CC1=C24
116 469.1618795721 NC1=C(C(=O)N=C=[NH+]C2=C3C=CC4=C5C(=CCC(=C35)C3=C(O)C(O)=C(O)C=C23)CCC4N)NC=N1 Intermediate Fragment 
117 345.1233688241 C=[NH+]C1=C2C=CC3=C4C(=CCC(=C24)C2=C(O)C(O)=C(O)C=C12)C=CC3N
118 362.1499179201 N=C=[NH+]C1=C2C=CC3=C4C2=C(CCC4CCC3N)C2=C(O)C(O)=C(O)C=C12
119 386.1135324121 NC1C=CC2=CCC3=C4C(=C([NH+]=C=NC=O)C5=CC(O)=C(O)C(O)=C53)C=CC1=C24
120 388.1291824761 NC1CCC2=CCC3=C4C(=C([NH+]=C=NC=O)C5=CC(O)=C(O)C(O)=C53)C=CC1=C24
121 469.1618795721 N=C(O)C1=C(NC=[NH+]C2=C3C=CC4=C5C(=CCC(=C35)C3=C(O)C(O)=C(O)C=C23)C=CC4N)N=CN1 Intermediate Fragment 
122 426.1560659201 NC1C=CC2=CCC3=C4C(=C([NH+]=CNC5=CNC=N5)C5=CC(O)=C(O)C(O)=C53)C=CC1=C24
123 424.1404158561 N=C1C=CC2=CCC3=C4C(=C([NH+]=CNC5=CNC=N5)C5=CC(O)=C(O)C(O)=C53)C=CC1=C24
124 356.1029677281 N=C1C=CC2=CCC3=C4C(=C([NH+]=CN)C5=C3C(=O)C(=O)C(O)=C5)C=CC1=C24
125 343.1077187601 C=[NH+]C1=C2C=CC3=C4C(=CCC(=C24)C2=C(O)C(O)=C(O)C=C12)C=CC3=N
126 469.1618795721 N=C=NC1=CNC(=[NH+]C2=C3C=CC4=C5C(=CCC(=C35)C3=C(O)C(O)=C(O)C=C23)CCC4N)N=C1O Intermediate Fragment 
127 452.1353304761 C=NC1=CNC(=[NH+]C2=C3C=CC4=C5C(=CCC(=C35)C3=C(O)C(O)=C(O)C=C23)C=CC4=N)N=C1O
128 442.1509805401 NC1C=NC(=[NH+]C2=C3C=CC4=C5C(=CCC(=C35)C3=C(O)C(O)=C(O)C=C23)C=CC4N)N=C1O
129 427.1400815081 NC1CCC2=CCC3=C4C(=C([NH+]=C5N=C(O)C=CN5)C5=C3C(=O)C(=O)C(O)=C5)C=CC1=C24
130 425.1244314441 NC1C=CC2=CCC3=C4C(=C([NH+]=C5N=C(O)C=CN5)C5=C3C(=O)C(=O)C(O)=C5)C=CC1=C24
131 469.1618795721 N=C=NC1=CC(O)=NC(=[NH+]C2=C3C=CC4=C5C(=CCC(=C35)C3=C(O)C(O)=C(O)C=C23)CCC4N)N1 Intermediate Fragment 
132 423.1087813801 N=C1C=CC2=CCC3=C4C(=C([NH+]=C5N=C(O)C=CN5)C5=C3C(=O)C(=O)C(O)=C5)C=CC1=C24
133 442.1509805401 NC1=CC(O)=NC(=[NH+]C2=C3C=CC4=C5C(=CCC(=C35)C3=C2C=C(O)C(=O)C3=O)CCC4N)N1
134 452.1353304761 C=NC1=CC(O)=NC(=[NH+]C2=C3C=CC4=C5C(=CCC(=C35)C3=C(O)C(O)=C(O)C=C23)C=CC4=N)N1
135 469.1618795721 C=NC1=C(N)C(O)=NC(=[NH+]C2=C3C=CC4=C5C(=CCC(=C35)C3=C(O)C(O)=C(O)C=C23)C=CC4N)N1 Intermediate Fragment 
136 438.1196804121 N=C1C=NC(=[NH+]C2=C3C=CC4=C5C(=CCC(=C35)C3=C(O)C(O)=C(O)C=C23)C=CC4=N)N=C1O
137 440.1353304761 N=C1C=CC2=CCC3=C4C(=C([NH+]=C5N=CC(N)C(O)=N5)C5=CC(O)=C(O)C(O)=C53)C=CC1=C24
138 469.1618795721 C=NC1=C(N)NC(=[NH+]C2=C3C=CC4=C5C(=CCC(=C35)C3=C(O)C(O)=C(O)C=C23)C=CC4N)N=C1O Intermediate Fragment 
139 440.1353304761 NC1=CC(O)=NC(=[NH+]C2=C3C=CC4=C5C(=CCC(=C35)C3=C2C=C(O)C(=O)C3=O)C=CC4N)N1
140 438.1196804121 N=C1C=CC2=CCC3=C4C(=C([NH+]=C5N=C(O)C=C(N)N5)C5=C3C(=O)C(=O)C(O)=C5)C=CC1=C24
141 469.1618795721 C=C1C2=C(C=CC3=C2C(C)=C([NH+]=C2N=C(O)C4=C(N=CN4)N2)C2=CC(O)=C(O)C(O)=C23)C=CC1N Intermediate Fragment 
142 469.1618795721 C=CC1=C([NH+]=C2N=C(O)C3=C(N=CN3)N2)C2=CC(O)=C(O)C(O)=C2C2=C1C1=CC(N)C=CC1=CC2 Intermediate Fragment 
143 469.1618795721 C=C=C1C2=C(C=CC3=C2C=C([NH+]=C2N=C(O)C4=C(N=CN4)N2)C2=CC(O)=C(O)C(O)=C23)CCC1N Intermediate Fragment 
144 443.1462295081 NC1C=C2C(=CCC3=C2C=C([NH+]=C2N=C(O)C4=C(N=CN4)N2)C2=C3C(=O)C(=O)C(O)=C2)CC1
145 469.1618795721 C=C=C1CCC(N)C2=C1C1=CC3=C(O)C(O)=C(O)C=C3C([NH+]=C3N=C(O)C4=C(N=CN4)N3)=C1C=C2 Intermediate Fragment 
146 469.1618795721 C=CC1=CCC2=C3C(=C([NH+]=C4N=C(O)C5=C(N=CN5)N4)C4=CC(O)=C(O)C(O)=C42)C=CC(CN)=C13 Intermediate Fragment 
147 469.1618795721 NCC=CC1=CCC2=C3C1=CC=CC3=C([NH+]=C1N=C(O)C3=C(N=CN3)N1)C1=CC(O)=C(O)C(O)=C12 Intermediate Fragment 
148 416.1353304761 O=C1C(=O)C2=C(C=C1O)C([NH+]=C1N=C(O)C3NC=NC3=N1)=C1C=CCC3CCCC2=C13
149 428.1353304761 CC1C=CC2=C3C1=CC=CC3=C([NH+]=C1N=C(O)C3=C(N=CN3)N1)C1=CC(O)=C(O)C(O)=C12
150 440.1353304761 C=CC1=CCC2=C3C1=CC=CC3=C([NH+]=C1N=C(O)C3=C(N=CN3)N1)C1=CC(O)=C(O)C(O)=C12
151 442.1509805401 CCC1=CCC2=C3C1=CC=CC3=C([NH+]=C1N=C(O)C3=C(N=CN3)N1)C1=CC(O)=C(O)C(O)=C12
152 469.1618795721 C=CC(N)C1=C2C=CCC3=C2C(=C([NH+]=C2N=C(O)C4=C(N=CN4)N2)C2=CC(O)=C(O)C(O)=C23)C=C1 Intermediate Fragment 
153 410.0883802841 O=C1C(=O)C2=C(C=C1O)C([NH+]=C1N=C(O)C3=NC=NC3=N1)=C1C=CC=C3C=CCC2=C31
154 469.1618795721 NC1CCC2=CCC(C3=CC=C(O)C(O)=C3O)=C3C(=C=[NH+]C4=NC(=O)C5=C(N=CN5)N4)C=CC1=C23 Intermediate Fragment 
155 469.1618795721 NC1CCC2=CC=C(C3=C(O)C(O)=C(O)C=C3C=[NH+]C3=NC(=O)C4=C(N=CN4)N3)C3=CC=CC1=C23 Intermediate Fragment 
156 469.1618795721 NC1C=CC2=CCC=C3C(=C([NH+]=C4N=C(O)C5=C(N=CN5)N4)C4=CC(O)=C(O)C(O)=C4)C=CC1=C23 Intermediate Fragment 
157 471.1775296361 NC1CCC2CCC3=C4C(=C([NH+]=C5N=C(O)C6=C(N=CN6)N5)C5=CC(O)=C(O)C(O)=C53)C=CC1=C42
158 473.1931797001 NC1CCC2CCC3=C4C(=C([NH+]=C5N=C(O)C6=C(N=CN6)N5)C5=CC(O)=C(O)C(O)=C53)C=CC1C42
159 475.2088297641 NC1CCC2CCC3=C4C(=C([NH+]=C5N=C(O)C6=C(N=CN6)N5)C5=CC(O)=C(O)C(O)=C53)CCC1C42
160 553.1830089401 NC1CCC2CCC3=C4C(=C([NH+]=C5N=C(O)C6=C(N5)N(C5=CC(O)=CO5)C=N6)C5=CC(O)=C(O)C(O)=C53)C=CC1=C42
161 555.1986590041 NC1CCC2CCC3=C4C(=C([NH+]=C5N=C(O)C6=C(N5)N(C5=CC(O)=CO5)C=N6)C5=CC(O)=C(O)C(O)=C53)C=CC1C42
162 537.1880943201 NC1CCC2CCC3=C4C(=C([NH+]=C5N=C(O)C6=C(N5)N(C5=CC=CO5)C=N6)C5=CC(O)=C(O)C(O)=C53)C=CC1=C42
163 557.2143090681 NC1CCC2CCC3=C4C(=C([NH+]=C5N=C(O)C6=C(N5)N(C5=CC(O)=CO5)C=N6)C5=CC(O)=C(O)C(O)=C53)CCC1C42
164 567.1986590041 CC1=C(O)C=C(N2C=NC3=C2NC(=[NH+]C2=C4C=CC5=C6C4=C(CCC6CCC5N)C4=C(O)C(O)=C(O)C=C24)N=C3O)O1
165 549.1880943201 CC1=C(O)C=C(N2C=NC3=C2NC(=[NH+]C2=C4C=CC5=C6C(=CCC(=C46)C4=CC(O)=C(O)C=C42)CCC5N)N=C3O)O1
166 567.1986590041 CC1=C(O)C=C(N2C=NC3=C2NC(=[NH+]C2=C4C=CC5=C6C4=C(CCC6CCC5N)C(C=O)=C2CC(O)=C=O)N=C3O)O1 Intermediate Fragment 
167 567.1986590041 C=C(O)C(=C)OCN1C=NC2=C1NC(=[NH+]C1=C3C=CC4=C5C(=CCC(=C35)C3=C(O)C(O)=C(O)C=C13)CCC4N)N=C2O Intermediate Fragment 
168 567.1986590041 CC(=O)C(O)=C=CN1C=NC2=C1NC(=[NH+]C1=C3C=CC4=C5C3=C(CCC5CCC4N)C3=C(O)C(O)=C(O)C=C13)N=C2O Intermediate Fragment 
169 493.1618795721 C#CN1C=NC2=C1NC(=[NH+]C1=C3C=CC4=C5C(=CCC(=C35)C3=C(O)C(O)=C(O)C=C13)CCC4N)N=C2O
170 525.1880943201 NC1CCC2CCC3=C4C(=C([NH+]=C5N=C(O)C6=C(N5)N(C#CC=O)C=N6)C5=CC(O)=C(O)C(O)=C53)C=CC1C42
171 549.1880943201 C=C=C(O)C#CN1C=NC2=C1NC(=[NH+]C1=C3C=CC4=C5C3=C(CCC5CCC4N)C3=C(O)C(O)=C(O)C=C13)N=C2O
172 567.1986590041 NC1CCC2CCC3=C4C(=C([NH+]=C5N=C(O)C6=C(N5)N(C5=CC=C(C=O)O5)C=N6)C5=CC(O)=C(O)C(O)=C53)C=CC1C42
173 568.1826745921 O=CC1=C(O)C=C(N2C=NC3=C2NC(=[NH+]C2=C4C=CC5CCCC6CCC(=C4C56)C4=C(O)C(O)=C(O)C=C24)N=C3O)O1
174 454.1509805401 OC1=NC(=[NH+]C2=C3C=CC4=C5C3=C(C=CC5CCC4)C3=C(O)C(O)=C(O)C=C23)NC2=C1NC=N2
175 568.1826745921 OC#CC(O)=C=C(O)N1C=NC2=C1NC(=[NH+]C1=C3C=CC4CCCC5CCC(=C3C45)C3=C(O)C(O)=C(O)C=C13)N=C2O Intermediate Fragment 
176 568.1826745921 O=C=C=C(OC#CO)N1C=NC2=C1NC(=[NH+]C1=C3CCC4CCCC5CCC(=C3C45)C3=C(O)C(O)=C(O)C=C13)N=C2O Intermediate Fragment 
177 585.2092236881 NC1CCC2CCC3=C4C(=C([NH+]=C5N=C(O)C6=C(N5)N(C5=CC(O)=C(C=O)O5)C=N6)C(CC(O)=C=O)=C3C=O)CCC1C42 Intermediate Fragment 
178 557.2143090681 NC1CCC2CCC3=C4C(=C([NH+]=C5N=C(O)C6=C(N5)N(C5=CC(O)=C(CO)O5)C=N6)C(C#CO)=C3C=O)CCC1C42
179 555.1986590041 NC1CCC2CCC3=C4C(=C([NH+]=C5N=C(O)C6=C(N5)N(C5=CC(O)=C(C=O)O5)C=N6)C(C#CO)=C3C=O)CCC1C42
180 555.1986590041 NC1CCC2CCC3=C4C(=C([NH+]=C5N=C(O)C6=C(N5)N(C(O)=C=C(O)C#CO)C=N6)C(C#CO)=C3C=O)CCC1C42 Intermediate Fragment 
181 527.2037443841 CC1=C(C=O)C2=C3C(=C1[NH+]=C1N=C(O)C4=C(N1)N(C1=CC(O)=C(C=O)O1)C=N4)C=CC1C(N)CCC(CC2)C31
182 527.2037443841 CC1=C(C=O)C2=C3C(=C1[NH+]=C1N=C(O)C4=C(N1)N(C(O)=C=C(O)C#CO)C=N4)C=CC1C(N)CCC(CC2)C31 Intermediate Fragment 
183 525.1880943201 CC1=C(C=O)C2=C3C(=C1[NH+]=C1N=C(O)C4=C(N1)N(C1=CC(O)=C(C=O)O1)C=N4)C=CC1=C3C(CC2)CCC1N
184 525.1880943201 CC1=C(C=O)C2=C3C(=C1[NH+]=C1N=C(O)C4=C(N1)N(C(O)=C=C(O)C#CO)C=N4)C=CC1=C3C(CC2)CCC1N Intermediate Fragment 
185 523.1724442561 CC1=C(C=O)C2=C3C(=C1[NH+]=C1N=C(O)C4=C(N1)N(C1=CC(O)=C(C=O)O1)C=N4)C=CC1=C3C(=CC2)CCC1N
186 511.1724442561 NC1CCC2CCC3=C4C(=C([NH+]=C5N=C(O)C6=C(N5)N(C5=CC(O)=C(C=O)O5)C=N6)C=C3C=O)C=CC1=C42
187 509.1567941921 NC1CCC2=CCC3=C4C(=C([NH+]=C5N=C(O)C6=C(N5)N(C5=CC(O)=C(C=O)O5)C=N6)C=C3C=O)C=CC1=C24
188 585.2092236881 NC1CCC2CCC3=C4C(=C([NH+]=C5N=C(O)C6=C(N5)N(C5=CC(O)=C(CO)O5)C=N6)C(C=C=O)=C3C(O)=C=O)CCC1C42 Intermediate Fragment 
189 523.1724442561 NC1CCC2CCC3=C4C(=C([NH+]=C5N=C(O)C6=C(N5)N(C5=CC(O)=C(C=O)O5)C=N6)C(C#CO)=C3)C=CC1=C42
190 495.1775296361 CC1=CC2=C3C(=C1[NH+]=C1N=C(O)C4=C(N1)N(C1=CC(O)=C(C=O)O1)C=N4)C=CC1=C3C(=CC2)CCC1N
191 525.1880943201 NC1CCC2CCC3=C4C(=C([NH+]=C5N=C(O)C6=C(N5)N(C5=CC(O)=C(C=O)O5)C=N6)C(C#CO)=C3)C=CC1C42
192 525.1880943201 NC1CCC2CCC3=C4C(=C([NH+]=C5N=C(O)C6=C(N5)N(C(O)=C=C(O)C#CO)C=N6)C(C#CO)=C3)C=CC1C42 Intermediate Fragment 
193 527.2037443841 NC1CCC2CCC3=C4C(=C([NH+]=C5N=C(O)C6=C(N5)N(C5=CC(O)=C(C=O)O5)C=N6)C(C#CO)=C3)CCC1C42
194 557.2143090681 CC1=C(C(O)=C=O)C2=C3C(=C1[NH+]=C1N=C(O)C4=C(N1)N(C1=CC(O)=C(C=O)O1)C=N4)CCC1C(N)CCC(CC2)C31
195 557.2143090681 CC1=C(C(O)=C=O)C2=C3C(=C1[NH+]=C1N=C(O)C4=C(N1)N(C(O)=C=C(O)C#CO)C=N4)CCC1C(N)CCC(CC2)C31 Intermediate Fragment 
196 555.1986590041 CC1=C(C(O)=C=O)C2=C3C(=C1[NH+]=C1N=C(O)C4=C(N1)N(C1=CC(O)=C(C=O)O1)C=N4)C=CC1C(N)CCC(CC2)C31
197 555.1986590041 CC1=C(C(O)=C=O)C2=C3C(=C1[NH+]=C1N=C(O)C4=C(N1)N(C(O)=C=C(O)C#CO)C=N4)C=CC1C(N)CCC(CC2)C31 Intermediate Fragment 
198 541.1830089401 NC1CCC2CCC3=C4C(=C([NH+]=C5N=C(O)C6=C(N5)N(C5=CC(O)=C(C=O)O5)C=N6)C=C3C(O)=C=O)C=CC1C42
199 539.1673588761 NC1CCC2CCC3=C4C(=C([NH+]=C5N=C(O)C6=C(N5)N(C5=CC(O)=C(C=O)O5)C=N6)C=C3C(O)=C=O)C=CC1=C42
200 585.2092236881 CC1=C(C(O)=C(O)C=O)C2=C3C(=C1[NH+]=C1N=C(O)C4=C(N1)N(C1=CC(O)=C(C=O)O1)C=N4)C=CC1C(N)CCC(CC2)C31 Intermediate Fragment 
201 585.2092236881 C=C(O)C(O)=C(O)C1=CC([NH+]=C2N=C(O)C3=C(N2)N(C2=CC(O)=C(C=O)O2)C=N3)=C2C=CC3C(N)CCC4CCC1=C2C43 Intermediate Fragment 
202 585.2092236881 NC1CCC2CCC3=C4C(=C([NH+]=C5N=C(O)C6=C(N5)N(C5=CC(O)=C(C=O)O5)C=N6)C(C=C(O)C(O)=C=O)=C3)CCC1C42 Intermediate Fragment 
203 557.2143090681 NC1CCC2CCC3=C4C(=C([NH+]=C5N=C(O)C6=C(N5)N(C5=CC(O)=C(C=O)O5)C=N6)C(CC(O)=C=O)=C3)CCC1C42
204 555.1986590041 NC1CCC2CCC3=C4C(=C([NH+]=C5N=C(O)C6=C(N5)N(C5=CC(O)=C(C=O)O5)C=N6)C(CC(O)=C=O)=C3)C=CC1C42
205 555.1986590041 NC1CCC2CCC3=C4C(=C([NH+]=C5N=C(O)C6=C(N5)N(C=C=C(O)C(=O)C=O)C=N6)C(CC(O)=C=O)=C3)C=CC1C42 Intermediate Fragment 
206 555.1986590041 NC1CCC2CCC3=C4C(=C([NH+]=C5N=C(O)C6=C(N5)N(C(O)=C=C(O)C#CO)C=N6)C(CC(O)=C=O)=C3)C=CC1C42 Intermediate Fragment 
207 553.1830089401 NC1CCC2CCC3=C4C(=C([NH+]=C5N=C(O)C6=C(N5)N(C5=CC(O)=C(C=O)O5)C=N6)C(CC(O)=C=O)=C3)C=CC1=C42
208 529.2193944481 NC1CCC2CCC3=C4C(=C([NH+]=C5N=C(O)C6=C(N5)N(C5=CC(O)=C(CO)O5)C=N6)C(C#CO)=C3)CCC1C42
209 501.2244798281 CC1=CC2=C3C(=C1[NH+]=C1N=C(O)C4=C(N1)N(C1=CC(O)=C(C=O)O1)C=N4)CCC1C(N)CCC(CC2)C31
210 499.2088297641 CC1=CC2=C3C(=C1[NH+]=C1N=C(O)C4=C(N1)N(C1=CC(O)=C(C=O)O1)C=N4)C=CC1C(N)CCC(CC2)C31
211 497.1931797001 CC1=CC2=C3C(=C1[NH+]=C1N=C(O)C4=C(N1)N(C1=CC(O)=C(C=O)O1)C=N4)C=CC1=C3C(CC2)CCC1N
212 495.1775296361 CC1=CC2=C3C(=C1[NH+]=C1N=C(O)C4=C(N1)N(C(O)=C=C(O)C#CO)C=N4)C=CC1=C3C(=CC2)CCC1N Intermediate Fragment 
213 493.1618795721 CC1=CC2=C3C(=C1[NH+]=C1N=C(O)C4=C(N1)N(C1=CC(O)=C(C=O)O1)C=N4)C=CC1=C3C(=CC2)C=CC1N
214 73.0284058201 CC([OH2+])=C=O
215 93.0546205681 OCC(O)C[OH2+]
216 485.1931797001 NC1CCC2CCC3=C4C(=C([NH+]=C5N=C(O)C6=C(N5)N(C5=CC(O)=C(C=O)O5)C=N6)C=C3)C=CC1C42
217 483.1775296361 NC1CCC2CCC3=C4C(=C([NH+]=C5N=C(O)C6=C(N5)N(C5=CC(O)=C(C=O)O5)C=N6)C=C3)C=CC1=C42
218 481.1618795721 NC1CCC2=CCC3=C4C(=C([NH+]=C5N=C(O)C6=C(N5)N(C5=CC(O)=C(C=O)O5)C=N6)C=C3)C=CC1=C24
219 479.1462295081 NC1C=CC2=CCC3=C4C(=C([NH+]=C5N=C(O)C6=C(N5)N(C5=CC(O)=C(C=O)O5)C=N6)C=C3)C=CC1=C24
220 585.2092236881 NC1CCC2CCC3=C4C(=C([NH+]=C(N=C=O)NC5=CN=CN5C5=CC(O)=C(C=O)O5)C5=CC(O)=C(O)C(O)=C53)CCC1C42 Intermediate Fragment 
221 542.2034100361 NC1CCC2CCC3=C4C(=C([NH+]=CNC5=CN=CN5C5=CC(O)=C(C=O)O5)C5=CC(O)=C(O)C(O)=C53)C=CC1C42
222 557.2143090681 NC(NC1=CN=CN1C1=CC(O)=C(C=O)O1)=[NH+]C1=C2C=CC3C(N)CCC4CCC(=C2C43)C2=C(O)C(O)=C(O)C=C12
223 585.2092236881 NC(N=C(O)C1=CN(C2=CC(O)=C(C=O)O2)C=N1)=[NH+]C1=C2C=CC3C(N)CCC4CCC(=C2C43)C2=C(O)C(O)=C(O)C=C12 Intermediate Fragment 
224 585.2092236881 NC1=C(C(=O)N=C=[NH+]C2=C3CCC4C(N)CCC5CCC(=C3C54)C3=C(O)C(O)=C(O)C=C23)N=CN1C1=CC(O)=C(C=O)O1 Intermediate Fragment 
225 390.1448325401 NC1CCC2CCC3=C4C(=C([NH+]=C=NC=O)C5=CC(O)=C(O)C(O)=C53)C=CC1=C42
226 392.1604826041 NC1CCC2CCC3=C4C(=C([NH+]=C=NC=O)C5=CC(O)=C(O)C(O)=C53)C=CC1C42
227 585.2092236881 N=C(O)C1=C(NC=[NH+]C2=C3C=CC4C(N)CCC5CCC(=C3C54)C3=C(O)C(O)=C(O)C=C23)N(C2=CC(O)=C(C=O)O2)C=N1 Intermediate Fragment 
228 585.2092236881 C=NC1=C(N=C2C=C(O)C(=C=O)O2)NC(=[NH+]C2=C3CCC4C(N)CCC5CCC(=C3C54)C3=C(O)C(O)=C(O)C=C23)N=C1O Intermediate Fragment 
229 585.2092236881 NC1CCC2CCC3=C4C(=C([NH+]=C5N=C(O)C(N=C=NC6=CC(O)=C(C=O)O6)=CN5)C5=CC(O)=C(O)C(O)=C53)CCC1C42 Intermediate Fragment 
230 456.1666306041 C=NC1=CNC(=[NH+]C2=C3C=CC4=C5C(=CCC(=C35)C3=C(O)C(O)=C(O)C=C23)CCC4N)N=C1O
231 438.1560659201 C=NC1=CNC(=[NH+]C2=C3C=CC4=C5C(=CCC(=C35)C3=C(O)C(O)=C(O)C=C23)C=CC4N)N=C1
232 454.1509805401 C=NC1=CNC(=[NH+]C2=C3C=CC4=C5C(=CCC(=C35)C3=C(O)C(O)=C(O)C=C23)C=CC4N)N=C1O
233 144.0655196001 C=[NH+]C1=CC(O)C(CO)O1
234 144.0655196001 C=[NH+]C(O)=C=C(O)CCO Intermediate Fragment 
235 146.0811696641 C=[NH+]C1CC(O)C(CO)O1
236 146.0811696641 C=[NH+]C=CC(O)C(O)CO Intermediate Fragment 
237 429.1557315721 NC1CCC2CCC3=C4C(=C([NH+]=C5N=C(O)C=CN5)C5=C3C(=O)C(=O)C(O)=C5)C=CC1=C42
238 585.2092236881 CN(C1=CC(O)=C(C=O)O1)C1=C(N)C(O)=NC(=[NH+]C2=C3C=CC4=C5C3=C(CCC5CCC4N)C3=C(O)C(O)=C(O)C=C23)N1 Intermediate Fragment 
239 585.2092236881 C=C(O)C(=C=O)OCN1C=NC2=C1NC(=[NH+]C1=C3C=CC4C(N)CCC5CCC(=C3C54)C3=C(O)C(O)=C(O)C=C13)N=C2O Intermediate Fragment 
240 499.1724442561 NC1CCC2=CCC3=C4C(=C([NH+]=C5N=C(O)C6=C(N5)N(CO)C=N6)C5=CC(O)=C(O)C(O)=C53)C=CC1=C24
241 497.1567941921 NC1C=CC2=CCC3=C4C(=C([NH+]=C5N=C(O)C6=C(N5)N(CO)C=N6)C5=CC(O)=C(O)C(O)=C53)C=CC1=C24
242 483.1775296361 CN1C=NC2=C1NC(=[NH+]C1=C3C=CC4=C5C(=CCC(=C35)C3=C(O)C(O)=C(O)C=C13)CCC4N)N=C2O
243 481.1618795721 CN1C=NC2=C1NC(=[NH+]C1=C3C=CC4=C5C(=CCC(=C35)C3=C(O)C(O)=C(O)C=C13)C=CC4N)N=C2O
244 585.2092236881 NC1CCC2CCC3=C4C(=C([NH+]=C5N=C(O)C6=C(N5)N(C=C=C(O)C(=O)C=O)C=N6)C5=CC(O)=C(O)C(O)=C53)CCC1C42 Intermediate Fragment 
245 493.1618795721 C#CN1C=NC2=C1NC(=[NH+]C1=C3C=CC4=C5C(=CCC(=C35)C=C1C=C(O)C(O)=C=O)CCC4N)N=C2O Intermediate Fragment 
246 495.1775296361 C#CN1C=NC2=C1NC(=[NH+]C1=C3C=CC4=C5C3=C(CCC5CCC4N)C3=C(O)C(O)=C(O)C=C13)N=C2O
247 495.1775296361 C#CN1C=NC2=C1NC(=[NH+]C1=C3C=CC4C(N)CCC5CCC(=C3C54)C(C(O)=C=O)=C1C=C=O)N=C2O Intermediate Fragment 
248 495.1775296361 C#CN1C=NC2=C1NC(=[NH+]C1=C3C=CC4=C5C3=C(C=C1C=C(O)C(O)=C=O)CCC5CCC4N)N=C2O Intermediate Fragment 
249 497.1931797001 C#CN1C=NC2=C1NC(=[NH+]C1=C3C=CC4C(N)CCC5CCC(=C3C54)C3=C(O)C(O)=C(O)C=C13)N=C2O
250 479.1826150161 C#CN1C=NC2=C1NC(=[NH+]C1=C3C=CC4=C5C3=C(CCC5CCC4N)C3=C(O)C(O)=CC=C13)N=C2O
251 479.1826150161 C#CN1C=NC2=C1NC(=[NH+]C1=C3C=CC4=C5C3=C(CCC5CCC4N)C3=C(O)C(O)=C(O)C=C13)N=C2
252 497.1931797001 C#CN1C=NC2=C1NC(=[NH+]C1=C3CCC4C(N)CCC5CCC(=C3C54)C(C(O)=C=O)=C1C=C=O)N=C2O Intermediate Fragment 
253 497.1931797001 C#CN1C=NC2=C1NC(=[NH+]C1=C3C=CC4C(N)CCC5CCC(=C3C54)C=C1C=C(O)C(O)=C=O)N=C2O Intermediate Fragment 
254 499.2088297641 C#CN1C=NC2=C1NC(=[NH+]C1=C3CCC4C(N)CCC5CCC(=C3C54)C3=C(O)C(O)=C(O)C=C13)N=C2O
255 501.2244798281 C=CN1C=NC2=C1NC(=[NH+]C1=C3CCC4C(N)CCC5CCC(=C3C54)C3=C(O)C(O)=C(O)C=C13)N=C2O
256 523.1724442561 NC1CCC2CCC3=C4C(=C([NH+]=C5N=C(O)C6=C(N5)N(C#CC=O)C=N6)C5=CC(O)=C(O)C(O)=C53)C=CC1=C42
257 507.1775296361 NC1CCC2CCC3=C4C(=C([NH+]=C5N=C(O)C6=C(N5)N(C#CC=O)C=N6)C5=CC=C(O)C(O)=C53)C=CC1=C42
258 525.1880943201 NC1CCC2CCC3=C4C(=C([NH+]=C5N=C(O)C6=C(N5)N(C#CC=O)C=N6)C(CC(O)=C=O)=C3C=O)C=CC1C42 Intermediate Fragment 
259 525.1880943201 NC1CCC2CCC3=C4C(=C([NH+]=C5N=C(O)C6=C(N5)N(C#CC=O)C=N6)C(C=C=O)=C3C(O)=C=O)CCC1C42 Intermediate Fragment 
260 525.1880943201 NC1CCC2CCC3=C4C(=C([NH+]=C5N=C(O)C6=C(N5)N(C#CC=O)C=N6)C(C=C(O)C(O)=C=O)=C3)C=CC1C42 Intermediate Fragment 
261 525.1880943201 NC1CCC2CCC3=C4C(=C([NH+]=C5N=C(O)C(N=C=NC#CC=O)=CN5)C5=CC(O)=C(O)C(O)=C53)C=CC1C42 Intermediate Fragment 
262 527.2037443841 NC1CCC2CCC3=C4C(=C([NH+]=C5N=C(O)C6=C(N5)N(C#CC=O)C=N6)C5=CC(O)=C(O)C(O)=C53)CCC1C42
263 509.1931797001 NC1CCC2CCC3=C4C(=C([NH+]=C5N=C(O)C6=C(N5)N(C#CC=O)C=N6)C5=CC=C(O)C(O)=C53)C=CC1C42
264 529.2193944481 NC1CCC2CCC3=C4C(=C([NH+]=C5N=C(O)C6=C(N5)N(C#CCO)C=N6)C5=CC(O)=C(O)C(O)=C53)CCC1C42
265 567.1986590041 NC1CCC2CCC3=C4C(=C([NH+]=C5N=C(O)C6=C(N5)N(C#CC(O)=C=C=O)C=N6)C5=CC(O)=C(O)C(O)=C53)CCC1C42
266 101.0597059481 C#CC(O)CC[OH2+]
267 549.1880943201 NC1CCC2CCC3=C4C(=C([NH+]=C5N=C(O)C6=C(N5)N(C#CC#CC=O)C=N6)C5=CC(O)=C(O)C(O)=C53)C=CC1C42
268 585.2092236881 NC1CCC2CCC3=C4C(=C([NH+]=C5N=C(O)C6=C(N5)N(C(O)=C=C(O)C#CO)C=N6)C5=CC(O)=C(O)C(O)=C53)CCC1C42 Intermediate Fragment 
269 91.0753560121 CC(O)CC[OH2+]
270 73.0647913281 C=C([OH2+])CC
271 71.0491412641 C=CC(=C)[OH2+]
272 71.0491412641 C#CCC[OH2+]
273 87.0440558841 C=C(O)C=C[OH2+]
274 495.1411441281 N=C1C=CC2=CCC3=C4C(=C([NH+]=C5N=C(O)C6=C(N5)N(CO)C=N6)C5=CC(O)=C(O)C(O)=C53)C=CC1=C24
275 501.1880943201 NC1CCC2CCC3=C4C(=C([NH+]=C5N=C(O)C6=C(N5)N(CO)C=N6)C5=CC(O)=C(O)C(O)=C53)C=CC1=C42
276 77.0597059481 OCCC[OH2+]
277 59.0491412641 CC=C[OH2+]
278 55.0178411361 [CH2+]C#CO
279 71.0127557561 OC#CC=[OH+]
280 68.9971056921 [O+]#CC#CO
281 41.0021910721 [C+]#CO
282 509.1567941921 C=C(O)N1C=NC2=C1NC(=[NH+]C1=C3C=CC4=C5C(=CCC(=C35)C3=C(O)C(O)=C(O)C=C13)C=CC4N)N=C2O
283 511.1724442561 C=C(O)N1C=NC2=C1NC(=[NH+]C1=C3C=CC4=C5C(=CCC(=C35)C3=C(O)C(O)=C(O)C=C13)CCC4N)N=C2O
284 513.1880943201 C=C(O)N1C=NC2=C1NC(=[NH+]C1=C3C=CC4=C5C3=C(CCC5CCC4N)C3=C(O)C(O)=C(O)C=C13)N=C2O
285 515.2037443841 C=C(O)N1C=NC2=C1NC(=[NH+]C1=C3C=CC4C(N)CCC5CCC(=C3C54)C3=C(O)C(O)=C(O)C=C13)N=C2O
286 517.2193944481 C=C(O)N1C=NC2=C1NC(=[NH+]C1=C3CCC4C(N)CCC5CCC(=C3C54)C3=C(O)C(O)=C(O)C=C13)N=C2O
287 47.0491412641 CC[OH2+]
288 29.0385765801 C=[CH3+]
289 539.1673588761 NC1CCC2CCC3=C4C(=C([NH+]=C5N=C(O)C6=C(N5)N(C(O)=C=C=O)C=N6)C5=CC(O)=C(O)C(O)=C53)C=CC1=C42
290 541.1830089401 NC1CCC2CCC3=C4C(=C([NH+]=C5N=C(O)C6=C(N5)N(C(O)=C=C=O)C=N6)C5=CC(O)=C(O)C(O)=C53)C=CC1C42
291 523.1724442561 NC1CCC2CCC3=C4C(=C([NH+]=C5N=C(O)C6=C(N5)N(C(O)=C=C=O)C=N6)C5=CC=C(O)C(O)=C53)C=CC1=C42
292 541.1830089401 NC1CCC2CCC3=C4C(=C([NH+]=C5N=C(O)C6=C(N5)N(C(O)=C=C=O)C=N6)C(C=C=O)=C3C(O)=C=O)CCC1C42 Intermediate Fragment 
293 541.1830089401 NC1CCC2CCC3=C4C(=C([NH+]=C5N=C(O)C6=C(N5)N(C(O)=C=C=O)C=N6)C(C=C(O)C(O)=C=O)=C3)C=CC1C42 Intermediate Fragment 
294 541.1830089401 NC1CCC2CCC3=C4C(=C([NH+]=C5N=C(O)C(N=C=NC(O)=C=C=O)=CN5)C5=CC(O)=C(O)C(O)=C53)C=CC1C42 Intermediate Fragment 
295 543.1986590041 NC1CCC2CCC3=C4C(=C([NH+]=C5N=C(O)C6=C(N5)N(C(O)=C=C=O)C=N6)C5=CC(O)=C(O)C(O)=C53)CCC1C42
296 525.1880943201 NC1CCC2CCC3=C4C(=C([NH+]=C5N=C(O)C6=C(N5)N(C(O)=C=C=O)C=N6)C5=CC=C(O)C(O)=C53)C=CC1C42
297 525.1880943201 C=C=C(O)N1C=NC2=C1NC(=[NH+]C1=C3C=CC4=C5C3=C(CCC5CCC4N)C3=C(O)C(O)=C(O)C=C13)N=C2O
298 583.1935736241 NC1CCC2CCC3=C4C(=C([NH+]=C5N=C(O)C6=C(N5)N(C(O)=C=C(O)C#CO)C=N6)C5=CC(O)=C(O)C(O)=C53)C=CC1C42
299 585.2092236881 NC1CCC2CCC3=C4C(=C([NH+]=C5N=C(O)C6=C(N5)N(C(=C=CO)OC#CO)C=N6)C5=CC(O)=C(O)C(O)=C53)CCC1C42 Intermediate Fragment 
300 539.1673588761 NC1CCC2=CCC3=C4C(=C([NH+]=C5N=C(O)C6=C(N5)N(COC#CO)C=N6)C5=CC(O)=C(O)C(O)=C53)C=CC1=C24
301 541.1830089401 NC1CCC2CCC3=C4C(=C([NH+]=C5N=C(O)C6=C(N5)N(COC#CO)C=N6)C5=CC(O)=C(O)C(O)=C53)C=CC1=C42
302 543.1986590041 NC1CCC2CCC3=C4C(=C([NH+]=C5N=C(O)C6=C(N5)N(COC#CO)C=N6)C5=CC(O)=C(O)C(O)=C53)C=CC1C42
303 555.1986590041 C=C(OC#CO)N1C=NC2=C1NC(=[NH+]C1=C3C=CC4C(N)CCC5CCC(=C3C54)C3=C(O)C(O)=C(O)C=C13)N=C2O
304 557.2143090681 C=C(OC#CO)N1C=NC2=C1NC(=[NH+]C1=C3CCC4C(N)CCC5CCC(=C3C54)C3=C(O)C(O)=C(O)C=C13)N=C2O
305 585.2092236881 C=C(OC(=C=O)C=O)N1C=NC2=C1NC(=[NH+]C1=C3CCC4C(N)CCC5CCC(=C3C54)C3=C(O)C(O)=C(O)C=C13)N=C2O Intermediate Fragment 
306 585.2092236881 C=C1C(N)CCC2CCC3=C(C(C)=C([NH+]=C4N=C(O)C5=C(N4)N(C4=CC(O)=C(C=O)O4)C=N5)C4=CC(O)=C(O)C(O)=C43)C12 Intermediate Fragment 
307 585.2092236881 C=CC1=C([NH+]=C2N=C(O)C3=C(N2)N(C2=CC(O)=C(C=O)O2)C=N3)C2=CC(O)=C(O)C(O)=C2C2=C1C1CC(N)CCC1CC2 Intermediate Fragment 
308 559.1935736241 NC1CCC2CCC3=C(C=C([NH+]=C4N=C(O)C5=C(N4)N(C4=CC(O)=C(C=O)O4)C=N5)C4=CC(O)=C(O)C(O)=C43)C2C1
309 585.2092236881 C=CC1C(N)CCC2CCC3=C(C=C([NH+]=C4N=C(O)C5=C(N4)N(C4=CC(O)=C(C=O)O4)C=N5)C4=CC(O)=C(O)C(O)=C43)C21 Intermediate Fragment 
310 585.2092236881 CCC1CCC(N)C2C=CC3=C([NH+]=C4N=C(O)C5=C(N4)N(C4=CC(O)=C(C=O)O4)C=N5)C4=CC(O)=C(O)C(O)=C4C=C3C12 Intermediate Fragment 
311 585.2092236881 CCC1=C2C(=C([NH+]=C3N=C(O)C4=C(N3)N(C3=CC(O)=C(C=O)O3)C=N4)C3=CC(O)=C(O)C(O)=C31)C=CC1C(N)CCCC21 Intermediate Fragment 
312 585.2092236881 CCC1CCC2=C3C(=C([NH+]=C4N=C(O)C5=C(N4)N(C4=CC(O)=C(C=O)O4)C=N5)C4=CC(O)=C(O)C(O)=C42)C=CC(CN)C31 Intermediate Fragment 
313 585.2092236881 NCCCC1CCC2=C3C(=C([NH+]=C4N=C(O)C5=C(N4)N(C4=CC(O)=C(C=O)O4)C=N5)C4=CC(O)=C(O)C(O)=C42)C=CCC31 Intermediate Fragment 
314 526.1357244001 O=CC1=C(O)C=C(N2C=NC3=C2NC(=[NH+]C2=C4C=CC=C5CCCC(=C54)C4=C(O)C(O)=C(O)C=C24)N=C3O)O1
315 540.1513744641 CC1CCC2=C3C1=CC=CC3=C([NH+]=C1N=C(O)C3=C(N1)N(C1=CC(O)=C(C=O)O1)C=N3)C1=CC(O)=C(O)C(O)=C12
316 542.1670245281 CC1CCC2=C3C(=C([NH+]=C4N=C(O)C5=C(N4)N(C4=CC(O)=C(C=O)O4)C=N5)C4=CC(O)=C(O)C(O)=C42)C=CCC31
317 544.1826745921 CC1CCC2=C3C(=C([NH+]=C4N=C(O)C5=C(N4)N(C4=CC(O)=C(C=O)O4)C=N5)C4=CC(O)=C(O)C(O)=C42)CCCC31
318 556.1826745921 CCC1CCC2=C3C(=C([NH+]=C4N=C(O)C5=C(N4)N(C4=CC(O)=C(C=O)O4)C=N5)C4=CC(O)=C(O)C(O)=C42)C=CCC31
319 585.2092236881 NC1CCC2CCC(C3=CC=C(O)C(O)=C3O)=C3C(=C=[NH+]C4=NC(=O)C5=C(N4)N(C4=CC(O)=C(C=O)O4)C=N5)CCC1C32 Intermediate Fragment 
320 585.2092236881 NC1CCC2CCC(C3=C(O)C(O)=C(O)C=C3C=[NH+]C3=NC(=O)C4=C(N3)N(C3=CC(O)=C(C=O)O3)C=N4)=C3C=CCC1C32 Intermediate Fragment 
321 585.2092236881 NC1CCC2CCC=C3C(=C([NH+]=C4N=C(O)C5=C(N4)N(C4=CC(O)=C(C=O)O4)C=N5)C4=CC(O)=C(O)C(O)=C4)C=CC1C32 Intermediate Fragment 
