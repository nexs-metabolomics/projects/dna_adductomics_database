#In-silico ESI-MS/MS [M+H]+ Spectra
#PREDICTED BY CFM-ID 4.0.8
#SMILES=C1(C(N(C(=O)NC1=O)[C@@H]1O[C@H](CO)[C@@H](C1)O)O)O
#InChiKey=ORRZKGUUIZYKRN-IWUYEAMXSA-N
energy0
129.02947 11.12 53 52 230 (5.4034 0.2286 0.00053071)
147.04003 47.69 51 62 54 68 (20.644 2.5359 0.74236 0.2299)
263.08738 100.00 0 182 256 191 129 138 118 93 251 187 211 212 (46.393 1.6226 1.1461 0.59392 0.25132 0.22691 0.2066 0.14166 0.027692 0.016404 0.014207 0.0024387)
energy1
44.01309 17.48 60 (1.6564)
45.03349 24.21 10 (2.2938)
57.03349 28.13 27 (2.6651)
59.01276 28.89 29 (2.7366)
59.04914 11.07 26 (1.049)
61.02841 20.33 30 (1.9261)
72.00800 17.01 57 70 (1.5442 0.067646)
76.03930 15.04 59 69 (1.4193 0.005127)
87.01890 42.50 55 (4.0266)
87.04406 22.49 14 39 (2.0964 0.034667)
99.04406 66.44 24 21 22 (3.7001 1.7926 0.80186)
102.01857 12.63 61 72 (0.91056 0.28579)
104.03422 30.85 66 188 (2.905 0.017528)
114.05495 24.58 144 147 145 (1.4467 0.48944 0.39234)
117.05462 64.67 17 23 34 37 40 (3.2326 2.2127 0.51152 0.12627 0.043891)
129.02947 100.00 53 52 230 (9.1695 0.274 0.030115)
130.01348 16.37 67 (1.5508)
132.06552 54.62 96 150 146 156 153 157 (2.792 1.0842 0.65138 0.29765 0.27302 0.075858)
134.08117 17.40 97 142 140 (1.0184 0.34502 0.28538)
147.04003 97.30 51 62 54 68 (4.6149 2.7096 1.575 0.31831)
159.04003 13.22 209 102 (1.2161 0.035906)
173.05568 9.62 194 205 124 236 235 (0.68644 0.14214 0.039128 0.034804 0.0091591)
190.07100 7.03 107 130 (0.46061 0.20494)
203.06625 10.08 99 197 101 (0.81936 0.085698 0.049558)
220.08156 8.83 116 128 (0.50257 0.33435)
233.07681 12.06 79 183 190 184 249 (0.94095 0.15616 0.034053 0.0079318 0.0038208)
245.07681 12.31 82 192 92 3 86 195 85 87 1 2 89 90 (0.39996 0.18671 0.17418 0.14442 0.12468 0.058534 0.030653 0.024817 0.014296 0.0060043 0.0013892 0.00022743)
263.08738 61.90 0 182 138 93 256 187 212 118 191 129 211 251 (3.648 0.55218 0.47195 0.44608 0.36778 0.10146 0.09218 0.054897 0.053795 0.046328 0.024126 0.0051796)
energy2
43.01784 12.95 12 (1.9455)
44.01309 14.53 60 (2.1832)
45.03349 16.41 10 (2.4654)
47.04914 7.03 11 (1.0565)
56.01309 10.12 105 (1.5208)
57.03349 20.43 27 (3.069)
59.04914 21.37 26 (3.2108)
61.02841 17.56 30 (2.6375)
69.99235 7.57 58 (1.137)
72.00800 25.22 57 70 (3.78 0.0085249)
87.04406 13.80 14 39 (1.9848 0.088124)
99.04406 100.00 21 22 24 (11.399 2.5959 1.0267)
101.05971 20.59 5 6 8 (1.587 1.4939 0.012279)
114.05495 11.14 147 144 145 (1.1578 0.33529 0.1801)
115.01382 12.70 103 (1.9079)
116.03422 8.04 172 171 170 228 (0.59087 0.25601 0.22259 0.13855)
117.05462 22.45 17 23 37 40 34 (1.2898 1.0881 0.57347 0.21833 0.20313)
118.04987 9.11 113 (1.3682)
129.02947 12.32 230 52 53 (1.061 0.6494 0.13951)
130.01348 6.63 67 (0.99622)
131.04512 7.80 75 76 242 (1.0843 0.066594 0.021183)
132.06552 9.84 150 153 146 96 156 157 (0.92987 0.2689 0.11548 0.089732 0.047408 0.02641)
147.04003 9.15 68 62 51 54 (0.46048 0.44712 0.24301 0.22439)
157.06077 8.13 199 (1.2207)
158.04478 8.41 135 (1.2639)
162.07608 14.23 132 (2.1383)
164.09173 9.70 186 (1.4572)
173.05568 25.45 205 235 124 194 236 (2.5089 0.82335 0.25035 0.18691 0.052959)
175.07133 8.53 123 203 202 198 204 200 100 (0.31507 0.25927 0.19906 0.19842 0.17109 0.10772 0.031404)
177.08698 8.27 122 214 (1.0222 0.22061)

0 263.0873772401 O=C1NC(=[OH+])N(C2CC(O)C(CO)O2)C(O)C1O
1 245.0768125561 OCC1OC(N2C=NC(=[OH+])C(O)=C2O)CC1O
2 245.0768125561 OC=C(O)C(=[OH+])N=C=NC1CC(O)C(CO)O1 Intermediate Fragment 
3 245.0768125561 OCC1OC(N2C(=[OH+])N=CC(O)=C2O)CC1O
4 119.0702706321 OC1CCOC1C[OH2+]
5 101.0597059481 C=C1OCCC1[OH2+]
6 101.0597059481 [OH2+]C=C1CCCO1
7 119.0702706321 O=CCC(O)CC[OH2+] Intermediate Fragment 
8 101.0597059481 CCC(O)C#C[OH2+]
9 73.0284058201 OC=CC=[OH+]
10 45.0334912001 C=C[OH2+]
11 47.0491412641 CC[OH2+]
12 43.0178411361 C=C=[OH+]
13 75.0440558841 OCC=C[OH2+]
14 87.0440558841 CC(O)C#C[OH2+]
15 89.0597059481 CC(O)C=C[OH2+]
16 91.0753560121 CC(O)CC[OH2+]
17 117.0546205681 OC=CC(O)CC=[OH+]
18 119.0702706321 OCCCOC=C[OH2+] Intermediate Fragment 
19 119.0702706321 CCOC(=C[OH2+])CO Intermediate Fragment 
20 85.0284058201 [OH2+]C1=COC=C1
21 99.0440558841 C=C1OCC=C1[OH2+]
22 99.0440558841 [OH+]=C=C1CCCO1
23 117.0546205681 CCC(O)C(=O)C=[OH+] Intermediate Fragment 
24 99.0440558841 CC=C(O)C#C[OH2+]
25 61.0647913281 CCC[OH2+]
26 59.0491412641 CC=C[OH2+]
27 57.0334912001 CC#C[OH2+]
28 55.0178411361 [CH2+]C#CO
29 59.0127557561 OC=C=[OH+]
30 61.0284058201 OCC=[OH+]
31 31.0542266441 C[CH4+]
32 29.0385765801 C=[CH3+]
33 91.0389705041 OCC(=[OH+])CO
34 117.0546205681 OCCCOC#C[OH2+] Intermediate Fragment 
35 89.0597059481 CC[OH+]C=CO
36 73.0284058201 C[OH+]C#CO
37 117.0546205681 CCOC(=C=[OH+])CO Intermediate Fragment 
38 71.0127557561 O=CC#C[OH2+]
39 87.0440558841 CC[OH+]C#CO
40 117.0546205681 COC(=C=[OH+])C(C)O Intermediate Fragment 
41 115.0389705041 OC1CCOC1=C=[OH+]
42 97.0284058201 [CH+]=C1OCC=C1O
43 97.0284058201 [OH+]=C=C1C=CCO1
44 115.0389705041 CC=C(O)C(=O)C=[OH+] Intermediate Fragment 
45 97.0284058201 C=C=C(O)C#C[OH2+]
46 115.0389705041 O=CCC(O)C#C[OH2+] Intermediate Fragment 
47 115.0389705041 OC=CCOC#C[OH2+] Intermediate Fragment 
48 115.0389705041 CCOC(=C=[OH+])C=O Intermediate Fragment 
49 113.0233204401 OC1=CCOC1=C=[OH+]
50 145.0243830601 OC1=NC([OH2+])=NC(O)=C1O
51 147.0400331241 OC1=C(O)C(O)NC([OH2+])=N1
52 129.0294684401 OC1=C(O)N=C([OH2+])N=C1
53 129.0294684401 OC1=CC(O)=NC([OH2+])=N1
54 147.0400331241 OC=NC([OH2+])=NC(O)=CO Intermediate Fragment 
55 87.0189037561 [NH2+]=C(O)N=C=O
56 74.0236547881 O=C=[NH+]CO
57 72.0080047241 O=C=[NH+]C=O
58 69.9923546601 O=C=[N+]=C=O
59 76.0393048521 [NH2+]=C(O)CO
60 44.0130901041 [NH+]#CO
61 102.0185694081 O=C=[NH+]C(O)=CO
62 147.0400331241 NC([OH2+])=NC(O)=C(O)C=O Intermediate Fragment 
63 89.0345538201 NC(=O)[NH+]=CO
64 56.9971056921 O=C=C=[OH+]
65 87.0076703761 O=C=C(O)C=[OH+]
66 104.0342194721 [NH2+]=C(O)C(O)C=O
67 130.0134840281 O=C=[NH+]C(O)=C(O)C=O
68 147.0400331241 NC(O)C(O)=C(O)N=C=[OH+] Intermediate Fragment 
69 76.0393048521 OC=[NH+]CO
70 72.0080047241 N=C(O)C#[O+]
71 74.0236547881 [NH2+]=C(O)C=O
72 102.0185694081 [NH2+]=C(O)C(=O)C=O
73 46.0287401681 [NH2+]=CO
74 149.0556831881 OC1=C(O)C(O)NC([OH2+])N1
75 131.0451185041 OC1=CN=C([OH2+])NC1O
76 131.0451185041 OC1=CC(O)NC([OH2+])=N1
77 151.0713332521 OC1NC([OH2+])NC(O)C1O
78 231.0611624921 OC1=NC(=[OH+])N(C2CC(O)CO2)C(O)=C1O
79 233.0768125561 OC1=C(O)N(C2CC(O)CO2)C(=[OH+])NC1O
80 215.0662478721 OC1=CN(C2CC(O)CO2)C(=[OH+])N=C1O
81 235.0924626201 OC1=C(O)N(C2CC(O)CO2)C([OH2+])NC1O
82 245.0768125561 CC1OC(N2C(=[OH+])N=C(O)C(O)=C2O)CC1O
83 103.0753560121 CC1OCCC1[OH2+]
84 227.0662478721 C=C1OC(N2C=C(O)C(O)=NC2=[OH+])CC1O
85 245.0768125561 C=C1OC(N=C([OH2+])N=C(O)C(O)=CO)CC1O Intermediate Fragment 
86 245.0768125561 OCC1CCC(N2C(=[OH+])N=C(O)C(O)=C2O)O1
87 245.0768125561 OCC1OC(N2C=C(O)C(O)=NC2=[OH+])CC1O
88 127.0138183761 O=C1C=NC(=[OH+])N=C1O
89 245.0768125561 N=C(O)C(O)=CN(C=[OH+])C1CC(O)C(=CO)O1 Intermediate Fragment 
90 245.0768125561 O=C(CO)C(O)CCN1C=C(O)C(O)=NC1=[OH+] Intermediate Fragment 
91 227.0662478721 OC#CC(O)CCN1C=C(O)C(O)=NC1=[OH+]
92 245.0768125561 OCC1OC(N2C(=[OH+])N=C(O)C=C2O)CC1O
93 263.0873772401 OC=NC(=[OH+])N(C(O)=CO)C1CC(O)C(CO)O1 Intermediate Fragment 
94 235.0924626201 O=C(N=CO)[NH+](CO)C1CC(O)C(CO)O1
95 205.0818979361 OC=NC(O)=[NH+]C1CC(O)C(CO)O1
96 132.0655196001 [NH2+]=C1CC(O)C(CO)O1
97 134.0811696641 [NH3+]C1CC(O)C(CO)O1
98 205.0818979361 O=C=NC(O)=[NH+]C(O)CC(O)CCO Intermediate Fragment 
99 203.0662478721 O=C=NC(O)=[NH+]C1CC(O)C(CO)O1
100 175.0713332521 NC(O)=[NH+]C1CC(O)C(=CO)O1
101 203.0662478721 O=C=NC(O)=[NH+]C(=O)CC(O)CCO Intermediate Fragment 
102 159.0400331241 O=C=NC(O)=[NH+]C(=O)CCO
103 115.0138183761 O=C=NC(O)=[NH+]C=O
104 201.0505978081 O=C=NC(O)=[NH+]C1CC(O)C(=CO)O1
105 56.0130901041 C=[N+]=C=O
106 188.0553488401 OC=C(O)[NH+]=C1CC(O)C(=CO)O1
107 190.0709989041 OC=C(O)[NH+]=C1CC(O)C(CO)O1
108 192.0866489681 OC=C(O)[NH2+]C1CC(O)C(CO)O1
109 192.0866489681 OC=C(O)[NH+]=C(O)CC(O)CCO Intermediate Fragment 
110 148.0604342201 OC=C(O)[NH+]=C(O)CCO
111 146.0447841561 OC=CC(O)=[NH+]C(O)=CO
112 120.0655196001 CC(O)[NH2+]C(O)=CO
113 118.0498695361 CC(O)=[NH+]C(O)=CO
114 216.0502634601 O=C=C1OC([NH+](C=O)C(O)=CO)CC1O
115 218.0659135241 O=C[NH+](C(O)=CO)C1CC(O)C(=CO)O1
116 220.0815635881 O=C[NH+](C(O)=CO)C1CC(O)C(CO)O1
117 235.0924626201 NC(=O)[NH+](C(O)=CO)C1CC(O)C(CO)O1
118 263.0873772401 NC(=[OH+])N(C(O)=C(O)C=O)C1CC(O)C(CO)O1 Intermediate Fragment 
119 207.0975480001 NC(=O)[NH+](CO)C1CC(O)C(CO)O1
120 205.0818979361 NC(=O)[NH+](CO)C1CC(O)C(=CO)O1
121 201.0505978081 NC(=O)[NH+](CO)C1C=C(O)C(=C=O)O1
122 177.0869833161 NC(O)=[NH+]C1CC(O)C(CO)O1
123 175.0713332521 NC(O)=[NH+]C(=O)CC(O)C=CO Intermediate Fragment 
124 173.0556831881 NC(O)=[NH+]C1CC(O)C(=C=O)O1
125 171.0400331241 NC(O)=[NH+]C1C=C(O)C(=C=O)O1
126 171.0400331241 NC(O)=[NH+]C=C=C(O)C(=O)C=O Intermediate Fragment 
127 171.0400331241 NC(O)=[NH+]C(=O)C=C(O)C#CO Intermediate Fragment 
128 220.0815635881 O=CC(O)=C(O)[NH2+]C1CC(O)C(CO)O1
129 263.0873772401 N=C(O)C(O)=C(O)N(C=[OH+])C1CC(O)C(CO)O1 Intermediate Fragment 
130 190.0709989041 O=C[NH+](CO)C1CC(O)C(=CO)O1
131 186.0396987761 O=C=C1OC([NH+](C=O)CO)C=C1O
132 162.0760842841 OC=[NH+]C1CC(O)C(CO)O1
133 160.0604342201 O=C=[NH+]C1CC(O)C(CO)O1
134 160.0604342201 CC(O)C(=CO)OC[NH+]=C=O Intermediate Fragment 
135 158.0447841561 O=C=[NH+]C1CC(O)C(=CO)O1
136 156.0291340921 O=C=[NH+]C1CC(O)C(=C=O)O1
137 246.0608281441 O=CC(O)=C(O)[NH+](C=O)C1CC(O)C(=CO)O1
138 263.0873772401 OCC1OC(NC(O)=C(O)C(O)N=C=[OH+])CC1O Intermediate Fragment 
139 116.0706049801 CC1OC(=[NH2+])CC1O
140 134.0811696641 [NH2+]=C(O)CC(O)CCO Intermediate Fragment 
141 116.0706049801 [NH+]#CCC(O)CCO
142 134.0811696641 CC(=[NH2+])OC(CO)CO Intermediate Fragment 
143 102.0549549161 [NH2+]=C1CC(O)CO1
144 114.0549549161 C=C1OC(=[NH2+])CC1O
145 114.0549549161 [NH2+]=C1CCC(=CO)O1
146 132.0655196001 [NH+]#CCC(O)C(O)CO Intermediate Fragment 
147 114.0549549161 [NH+]#CCC(O)C=CO
148 72.0443902321 [NH+]#CCCO
149 44.0494756121 CC=[NH2+]
150 132.0655196001 [NH2+]=C(O)CC(O)C=CO Intermediate Fragment 
151 60.0443902321 CC(=[NH2+])O
152 58.0287401681 [CH2+]C(=N)O
153 132.0655196001 [NH2+]=C(CCO)OC=CO Intermediate Fragment 
154 70.0287401681 [NH+]#CC=CO
155 88.0393048521 [NH2+]=C(O)C=CO
156 132.0655196001 CC(=[NH2+])OC(=CO)CO Intermediate Fragment 
157 132.0655196001 CC(O)C(CO)OC#[NH+] Intermediate Fragment 
158 130.0498695361 [NH2+]=C1CC(O)C(=CO)O1
159 130.0498695361 [NH+]#CCC(O)C(=O)CO Intermediate Fragment 
160 130.0498695361 [NH2+]=C(O)CC(O)C#CO Intermediate Fragment 
161 130.0498695361 [NH2+]=C(CCO)OC#CO Intermediate Fragment 
162 130.0498695361 CC(O)C(=CO)OC#[NH+] Intermediate Fragment 
163 128.0342194721 [NH2+]=C1CC(O)C(=C=O)O1
164 128.0342194721 [NH+]#CCC(O)C(=O)C=O Intermediate Fragment 
165 112.0029193441 O=C=[NH+]C(=O)C#CO
166 132.0291340921 O=C=[NH+]C(O)C(O)=CO
167 89.0233204401 OC=C(O)C=[OH+]
168 114.0185694081 O=C=[NH+]C(O)C#CO
169 134.0447841561 O=C[NH2+]C(O)C(O)=CO
170 116.0342194721 C=[NH+]C(=O)C(O)=CO
171 116.0342194721 C=C(O)C(O)[NH+]=C=O
172 116.0342194721 O=C[NH2+]C(O)C#CO
173 136.0604342201 OC=C(O)C(O)[NH2+]CO
174 138.0760842841 OC[NH2+]C(O)C(O)CO
175 120.0655196001 OC=CC(O)[NH2+]CO
176 235.0924626201 N=C(O)C(O)=C(O)[NH2+]C1CC(O)C(CO)O1
177 106.0498695361 [NH3+]C(O)C(O)=CO
178 218.0659135241 O=CC(O)=C(O)[NH+]=C1CC(O)C(CO)O1
179 119.0451185041 N=C(O)C(O)C(=[NH2+])O
180 235.0924626201 N=C(O)C(O)=C(O)[NH+]=C(O)CC(O)CCO Intermediate Fragment 
181 216.0502634601 O=CC(O)=C(O)[NH+]=C1CC(O)C(=CO)O1
182 263.0873772401 OC=C(O)C(O)=NC([OH2+])=NC1CC(O)C(CO)O1 Intermediate Fragment 
183 233.0768125561 OC=C1OC([NH+]=C(O)N=C(O)CO)CC1O
184 233.0768125561 O=C(CC(O)C=CO)[NH+]=C(O)N=C(O)CO Intermediate Fragment 
185 235.0924626201 OCC(O)=NC(O)=[NH+]C1CC(O)C(CO)O1
186 164.0917343481 OC[NH2+]C1CC(O)C(CO)O1
187 263.0873772401 OC=C1OC(N(CO)C(=[OH+])N=C(O)CO)CC1O Intermediate Fragment 
188 104.0342194721 O=C=[NH+]C(O)CO
189 188.0553488401 O=C=C1OC([NH+](C=O)CO)CC1O
190 233.0768125561 O=C=NC(=O)[NH+](CO)C1CC(O)C(CO)O1
191 263.0873772401 OCC(O)C(O)CCN1C(=[OH+])N=C(O)C(O)=C1O Intermediate Fragment 
192 245.0768125561 OC=CC(O)CCN1C(=[OH+])N=C(O)C(O)=C1O
193 201.0505978081 OC=CCN1C(=[OH+])N=C(O)C(O)=C1O
194 173.0556831881 CCN1C(=[OH+])N=C(O)C(O)=C1O
195 245.0768125561 O=C=NC(=[OH+])N(CCC(O)C=CO)C(O)=CO Intermediate Fragment 
196 205.0818979361 OCCCN1C(=[OH+])NC(O)C(O)=C1O
197 203.0662478721 OCCCN1C(=[OH+])N=C(O)C(O)=C1O
198 175.0713332521 CCN1C(=[OH+])NC(O)C(O)=C1O
199 157.0607685681 CCN1C=C(O)C(O)=NC1=[OH+]
200 175.0713332521 CCN(C(=[OH+])N=CO)C(O)=CO Intermediate Fragment 
201 113.0345538201 C=C[NH+]=C(O)N=C=O
202 175.0713332521 CCN(C=[OH+])C(O)=C(O)C(=N)O Intermediate Fragment 
203 175.0713332521 CCNC(O)=C(O)C(O)N=C=[OH+] Intermediate Fragment 
204 175.0713332521 CCN=C([OH2+])N=C(O)C(O)=CO Intermediate Fragment 
205 173.0556831881 CCNC(O)=C(O)C(=O)N=C=[OH+] Intermediate Fragment 
206 171.0400331241 C=CN1C(=[OH+])N=C(O)C(O)=C1O
207 171.0400331241 CC=NC(O)=C(O)C(=O)N=C=[OH+] Intermediate Fragment 
208 161.0556831881 CN1C(=[OH+])NC(O)C(O)=C1O
209 159.0400331241 CN1C(=[OH+])N=C(O)C(O)=C1O
210 157.0243830601 [CH2+]N1C(=O)N=C(O)C(O)=C1O
211 263.0873772401 CC(O)C(CO)OCN1C(=[OH+])N=C(O)C(O)=C1O Intermediate Fragment 
212 263.0873772401 CC(OC(CO)CO)N1C(=[OH+])N=C(O)C(O)=C1O Intermediate Fragment 
213 93.0546205681 OCC([OH2+])CO
214 177.0869833161 CCN1C(O)=C(O)C(O)NC1[OH2+]
215 77.0597059481 OCCC[OH2+]
216 187.0349477441 CC(=O)N1C(=[OH+])N=C(O)C(O)=C1O
217 169.0243830601 C#CN1C(=[OH+])N=C(O)C(O)=C1O
218 187.0349477441 CC(=O)N(C(=[OH+])N=C=O)C(O)=CO Intermediate Fragment 
219 187.0349477441 CC(O)=NC(O)=C(O)C(=O)N=C=[OH+] Intermediate Fragment 
220 187.0349477441 CC(=O)N=C([OH2+])N=C(O)C(O)=C=O Intermediate Fragment 
221 189.0505978081 CC(O)N1C(=[OH+])N=C(O)C(O)=C1O
222 189.0505978081 CC(O)N(C(=[OH+])N=C=O)C(O)=CO Intermediate Fragment 
223 189.0505978081 CC(=O)N(C=[OH+])C(O)=C(O)C(=N)O Intermediate Fragment 
224 189.0505978081 CC(O)NC(O)=C(O)C(=O)N=C=[OH+] Intermediate Fragment 
225 127.9978339641 O=C=[NH+]C(=O)C(O)=C=O
226 161.0556831881 CC(=O)[NH+]=C(O)C(O)C(=N)O
227 144.0291340921 CC(=O)[NH+]=C(O)C(=O)C=O
228 116.0342194721 CC(O)=[NH+]C(O)=C=O
229 189.0505978081 CC(=O)N=C([OH2+])N=C(O)C(O)=CO Intermediate Fragment 
230 129.0294684401 CC(=O)[NH+]=C(O)N=C=O
231 189.0505978081 CC(=O)N(C=O)C(=[OH+])N=C(O)CO Intermediate Fragment 
232 100.0029193441 O=C=[NH+]C(=O)C=O
233 90.0549549161 CC(O)[NH+]=CO
234 191.0662478721 CC(O)N1C(=[OH+])NC(O)C(O)=C1O
235 173.0556831881 CC(O)N1C=C(O)C(O)=NC1=[OH+]
236 173.0556831881 CC(O)N1C(=[OH+])N=C(O)C=C1O
237 191.0662478721 CC(O)N(C(=[OH+])N=CO)C(O)=CO Intermediate Fragment 
238 191.0662478721 CC(O)N(C(N)=[OH+])C(O)=C(O)C=O Intermediate Fragment 
239 191.0662478721 CC(O)NC(O)=C(O)C(O)N=C=[OH+] Intermediate Fragment 
240 191.0662478721 CC(O)N=C([OH2+])N=C(O)C(O)=CO Intermediate Fragment 
241 103.0502038841 CC(=O)[NH+]=C(N)O
242 131.0451185041 CC(O)[NH+]=C(O)N=C=O
243 191.0662478721 CC(=O)N(CO)C(=[OH+])N=C(O)CO Intermediate Fragment 
244 193.0818979361 CC(O)N1C(O)=C(O)C(O)NC1[OH2+]
245 193.0818979361 CC(O)N=C([OH2+])NC(O)C(O)=CO Intermediate Fragment 
246 195.0975480001 CC(O)N1C([OH2+])NC(O)C(O)C1O
247 195.0975480001 CC(O)NC([OH2+])NC(O)C(O)=CO Intermediate Fragment 
248 231.0611624921 CC(OC=CO)N1C(=[OH+])N=C(O)C(O)=C1O
249 233.0768125561 CC(OCCO)N1C(=[OH+])N=C(O)C(O)=C1O
250 235.0924626201 CC(OCCO)N1C(=[OH+])NC(O)C(O)=C1O
251 263.0873772401 OCCOC(CCO)N1C(=[OH+])N=C(O)C(O)=C1O Intermediate Fragment 
252 219.0611624921 OCCC(O)N1C(=[OH+])N=C(O)C(O)=C1O
253 219.0611624921 O=C(N=C=[OH+])C(O)=C(O)NC(O)CCO Intermediate Fragment 
254 221.0768125561 OCCC(O)N1C(=[OH+])NC(O)C(O)=C1O
255 221.0768125561 OC=C(O)C(O)=NC([OH2+])=NC(O)CCO Intermediate Fragment 
256 263.0873772401 OCCC(O)CC(O)N1C(=[OH+])N=C(O)C(O)=C1O Intermediate Fragment 
257 217.0455124281 O=C(CCO)N1C(=[OH+])N=C(O)C(O)=C1O
258 177.0505978081 OCN1C(=[OH+])NC(O)C(O)=C1O
259 177.0505978081 OC=C(O)C(O)=NC([OH2+])=NCO Intermediate Fragment 
260 175.0349477441 OCN1C(=[OH+])N=C(O)C(O)=C1O
261 175.0349477441 O=C(N=C=[OH+])C(O)=C(O)NCO Intermediate Fragment 
262 175.0349477441 O=CN=C([OH2+])N=C(O)C(O)=CO Intermediate Fragment 
263 173.0192976801 O=CN1C(=[OH+])N=C(O)C(O)=C1O
264 173.0192976801 O=C=C(O)C(O)=NC([OH2+])=NC=O Intermediate Fragment 
265 261.0717271761 OCC1OC(N2C(=[OH+])N=C(O)C(O)=C2O)CC1O
