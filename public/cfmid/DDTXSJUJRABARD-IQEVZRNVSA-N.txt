#In-silico ESI-MS/MS [M+H]+ Spectra
#PREDICTED BY CFM-ID 4.0.8
#SMILES=[nH]1c(nc2c(c1=O)ncn2[C@@H]1O[C@@H](CO)[C@@H](C1)O)N[C@H]1c2cc(c(cc2[C@@H]2[C@H](C1)[C@@H]1[C@@](CC2)(C(=O)CC1)C)O)O
#InChiKey=DDTXSJUJRABARD-NZMLQEKISA-N
energy0
418.18737 77.68 32 34 35 21 (31.17 2.4382 2.4382 0.03279)
436.19793 100.00 20 114 92 90 112 126 81 67 79 41 36 125 60 87 76 127 49 43 71 48 50 85 68 53 63 77 45 (43.107 1.3792 0.53678 0.25165 0.24069 0.22365 0.10919 0.10105 0.079394 0.053608 0.042867 0.036446 0.03311 0.032372 0.031904 0.028206 0.026225 0.024937 0.021481 0.016569 0.013177 0.013068 0.011976 0.010891 0.0098832 0.0058775 0.0041182)
energy1
81.06988 4.85 334 (1.6904)
99.04406 4.36 9 6 7 (1.2423 0.19153 0.085674)
99.08044 2.35 97 335 (0.8133 0.0078537)
121.06479 2.04 108 353 (0.50727 0.20344)
127.11174 3.42 110 (1.1936)
312.10912 8.08 105 (2.8181)
408.20302 2.59 39 359 64 70 (0.46438 0.21489 0.21173 0.012194)
418.18737 100.00 32 34 35 21 (21.287 6.7458 6.7458 0.10817)
420.16663 6.14 33 57 (2.1431 0.00021558)
420.20302 16.56 161 (5.7764)
426.14081 2.69 323 (0.93724)
436.19793 71.95 20 92 114 81 112 36 79 85 126 127 41 53 67 76 90 49 87 60 63 77 45 43 125 48 68 50 71 (15.723 4.9453 1.6921 0.57603 0.5681 0.52421 0.14881 0.14871 0.12961 0.096957 0.068572 0.062927 0.060198 0.051738 0.05147 0.042414 0.04125 0.035821 0.028026 0.021711 0.019884 0.018466 0.013092 0.011103 0.0096296 0.0079336 0.002359)
516.22415 3.99 137 162 207 (1.2669 0.087965 0.037954)
534.23471 2.10 160 136 178 174 1 206 176 165 163 172 208 177 141 139 140 164 138 (0.21233 0.11173 0.1036 0.096598 0.068735 0.037091 0.019938 0.0175 0.012162 0.012026 0.0081055 0.0078606 0.0069483 0.0062985 0.0053756 0.0049986 0.00098788)
energy2
47.04914 5.49 233 (1.0385)
59.04914 7.18 10 (1.3575)
71.04914 3.74 263 264 (0.48961 0.21707)
73.02841 7.49 225 (1.4158)
75.04406 25.47 224 (4.8131)
79.05423 4.33 332 (0.81887)
95.08553 3.77 339 (0.71313)
105.06988 3.48 354 (0.65736)
107.08553 5.49 321 (1.0385)
111.08044 3.70 88 (0.70018)
127.11174 23.67 110 (4.4743)
144.06552 7.15 202 (1.3521)
150.04104 7.76 29 (1.4668)
152.05669 4.13 30 (0.77966)
152.07060 10.54 303 (1.992)
286.09347 8.96 82 (1.6938)
312.10912 45.11 105 (8.5256)
326.12477 16.65 89 (3.1463)
380.17172 10.26 121 (1.94)
388.12516 8.04 292 (1.5196)
394.18737 3.86 364 (0.72988)
412.16155 11.34 347 343 344 (1.3214 0.42224 0.39943)
418.18737 12.53 34 35 32 21 (0.8259 0.8259 0.36382 0.35179)
419.17138 5.02 58 37 46 61 (0.7594 0.11526 0.061962 0.01291)
426.14081 17.39 323 (3.2862)
436.19793 100.00 92 53 20 114 36 45 85 81 71 60 50 90 68 79 43 63 77 41 48 87 126 125 76 67 49 127 112 (6.0853 4.4716 1.8865 1.3711 0.77194 0.36579 0.36261 0.32372 0.29489 0.27894 0.25611 0.22255 0.21767 0.21154 0.20586 0.20542 0.17012 0.16987 0.16818 0.16728 0.12365 0.11784 0.098238 0.093973 0.091847 0.089871 0.078309)
462.21358 9.19 220 229 (1.6851 0.051755)
480.22415 4.07 288 230 228 231 (0.68401 0.071233 0.0098536 0.0035617)
524.25036 7.50 180 135 268 282 360 358 281 (1.104 0.20571 0.076537 0.015611 0.013798 0.001382 0.0010184)
534.23471 3.80 1 178 208 206 140 165 138 139 174 163 136 164 141 172 160 177 176 (0.43484 0.15695 0.11165 0.011935 0.00077136 0.00037326 0.00023436 0.00013555 0.00012837 0.00010958 9.7989e-05 9.4879e-05 9.4303e-05 8.7462e-05 6.8587e-05 1.7516e-05 7.1175e-06)

0 552.2452748481 CC12CCC3c4cc(O)c(O)cc4C([NH2+]c4nc5c(ncn5C5CC(O)C(CO)O5)c(=O)[nH]4)CC3C1CCC2=O
1 534.2347101641 CC12CCC3C4CC(O)C(O)C=C4C(=[NH+]C4=NC=C5N=CN(C6C=C(O)C(=C=O)O6)C5=N4)CC3C1CCC2O
2 119.0702706321 OC1CCOC1C[OH2+]
3 119.0702706321 O=CCC(O)CC[OH2+] Intermediate Fragment 
4 117.0546205681 OC1CCOC1=C[OH2+]
5 89.0597059481 [OH2+]C1CCOC1
6 99.0440558841 C=C1OCC=C1[OH2+]
7 99.0440558841 [OH+]=C=C1CCCO1
8 117.0546205681 CCC(O)C(=O)C=[OH+] Intermediate Fragment 
9 99.0440558841 CC=C(O)C#C[OH2+]
10 59.0491412641 CC=C[OH2+]
11 57.0334912001 CC#C[OH2+]
12 117.0546205681 O=CCC(O)C=C[OH2+] Intermediate Fragment 
13 89.0597059481 CC(O)C=C[OH2+]
14 117.0546205681 OCCCOC#C[OH2+] Intermediate Fragment 
15 117.0546205681 CCOC(=C=[OH+])CO Intermediate Fragment 
16 115.0389705041 OC1CCOC1=C=[OH+]
17 434.1822806681 CC12CCC3=C(CC(=[NH+]C4=NC(O)=C5N=CNC5=N4)C4=CC(O)=C(O)C=C43)C1CCC2O
18 416.1717159841 CC12C=CC3=C(CC(=[NH+]C4=NC(O)=C5N=CNC5=N4)C4=CC(O)=C(O)C=C43)C1CCC2
19 434.1822806681 CC1(C=CC2=CCC(=[NH+]C3=NC(O)=C4N=CNC4=N3)C3=CC(O)=C(O)C=C23)CCCC1O Intermediate Fragment 
20 436.1979307321 CC12CCC3C4=CC(O)=C(O)C=C4C(=[NH+]C4=NC(O)=C5N=CNC5=N4)CC3C1CCC2O
21 418.1873660481 CC12CCC3=C(CC(=[NH+]C4=NC=C5N=CNC5=N4)C4=CC(O)=C(O)C=C43)C1CCC2O
22 302.1750700481 CC12CCC3C4=CC(O)=C(O)C=C4C(=[NH2+])CC3C1CCC2O
23 300.1594199841 CC12CCC3=C(CC(=[NH2+])C4=CC(O)=C(O)C=C43)C1CCC2O
24 294.1124697921 CC12C=CC3=C(CC(=[NH2+])C4=CC(O)=C(O)C=C43)C1C=CC2=O
25 287.1641710161 CC12CCC3=C4C=C(O)C(O)=CC4=CCC3C1CCC2[OH2+]
26 285.1485209521 CC12CCC3=C4C=C(O)C(O)=CC4=CC=C3C1CCC2[OH2+]
27 283.1328708881 CC12C=CC3=C4C=C(O)C(O)=CC4=CC=C3C1CCC2[OH2+]
28 279.1015707601 CC12C=CC3=C4C=C(O)C(O)=CC4=CC=C3C1C=CC2=[OH+]
29 150.0410361681 [NH3+]C1=NC(=O)C2=NC=NC2=N1
30 152.0566862321 [NH3+]C1=NC(=O)C2=NCNC2=N1
31 154.0723362961 [NH3+]C1=NC(=O)C2NCNC2=N1
32 418.1873660481 CC12CCCC1C1=C(CC2)C2=CC(O)=C(O)C=C2C(=[NH+]C2=NC(O)=C3N=CNC3=N2)C1
33 420.1666306041 OC1=C(O)C=C2C(=C1)C(=[NH+]C1=NC(O)=C3N=CNC3=N1)CC1=C2CCC2C(O)CCC12
34 418.1873660481 CC12CCC3=C(CC(=[NH+]C4=NC(O)=C5N=CNC5=N4)C4=CC(O)=CC=C43)C1CCC2O
35 418.1873660481 CC12CCC3=C(CC(=[NH+]C4=NC(O)=C5N=CNC5=N4)C4=CC=C(O)C=C43)C1CCC2O
36 436.1979307321 CC12CCC3C4=CC(O)=C(O)C=C4C(=[NH+]C(=N)N=C4NC=NC4=C=O)CC3C1CCC2O Intermediate Fragment 
37 419.1713816361 CC12CCC3=C(C=C(N=C=[NH+]C4=C(C=O)N=CN4)C4=CC(O)=C(O)C=C43)C1CCC2O
38 327.1703190161 CC12CCC3C4=CC(O)=C(O)C=C4C(=NC#[NH+])CC3C1CCC2O
39 408.2030161121 CC12CCC3C4=CC(O)=C(O)C=C4C(=NC(=[NH2+])N=C4C=NC=N4)CC3C1CCC2O
40 434.1822806681 CC12CCC3=C(CC(=NC(=[NH2+])N=C4NC=NC4=C=O)C4=CC(O)=C(O)C=C43)C1CCC2O
41 436.1979307321 CC12CCC3C4=CC(O)=C(O)C=C4C(=[NH+]C(N=C4C=NC=N4)=NC=O)CC3C1CCC2O Intermediate Fragment 
42 391.1764670161 CC12CCC3=C(C=C(N=C=[NH+]C4=CN=CN4)C4=CC(O)=C(O)C=C43)C1CCC2O
43 436.1979307321 CC12CCC3=C(CC(=[NH+]C(N)=NC(=O)C4=CNC=N4)C4=CC(O)=C(O)C=C43)C1CCC2O Intermediate Fragment 
44 434.1822806681 CC12C=CC3=C(CC(=NC(N)=[NH+]C(=O)C4=CNC=N4)C4=CC(O)=C(O)C=C43)C1CCC2O
45 436.1979307321 CC12CCC3=C4C=C(O)C(O)=CC4=C([NH+]=C=NC(O)=C4N=CNC4=N)CC3C1CCC2O Intermediate Fragment 
46 419.1713816361 CC12CCC3=C(C=C(N=C=[NH+]C(=O)C4=CNC=N4)C4=CC(O)=C(O)C=C43)C1CCC2O
47 434.1822806681 CC12CCC3=C(C=C(N=C=[NH+]C(O)=C4N=CNC4=N)C4=CC(O)=C(O)C=C43)C1CCC2O
48 436.1979307321 CC12CCC3=C4C=C(O)C(O)=CC4=C([NH+]=C=NC4=C(C(N)=O)N=CN4)CC3C1CCC2O Intermediate Fragment 
49 436.1979307321 CC12CCC3C4=CC(O)=C(O)C=C4C(=[NH+]C4=NC(O)=CC(N=C=N)=N4)CC3C1CCC2O Intermediate Fragment 
50 436.1979307321 CC12CCC3C4=CC(O)=C(O)C=C4C(=[NH+]C4=NC(O)=C(N=C=N)C=N4)CC3C1CCC2O Intermediate Fragment 
51 392.1604826041 CC12C=CC3=C(CC(=[NH+]C4=NC(O)=CC=N4)C4=CC(O)=C(O)C=C43)C1CCC2O
52 409.1870317001 CC12CCC3=C(CC(=NC4=NC(O)=C([NH3+])C=N4)C4=CC(O)=C(O)C=C43)C1CCC2O
53 436.1979307321 C=NC1=C(O)N=C([NH+]=C2CC3=C(CCC4(C)C(O)CCC34)C3=CC(O)=C(O)C=C32)N=C1N Intermediate Fragment 
54 405.1557315721 CC12C=CC3=C(CC(=NC4=NC(O)=CC([NH3+])=N4)C4=CC(O)=C(O)C=C43)C1CCC2=O
55 407.1713816361 CC12C=CC3=C(CC(=NC4=NC(O)=CC([NH3+])=N4)C4=CC(O)=C(O)C=C43)C1CCC2O
56 409.1870317001 CC12CCC3=C(CC(=NC4=NC(O)=CC([NH3+])=N4)C4=CC(O)=C(O)C=C43)C1CCC2O
57 420.1666306041 CC12C=CC3=C(CC(=NC4=NC(O)=C([NH3+])C(N)=N4)C4=CC(O)=C(O)C=C43)C1CCC2=O
58 419.1713816361 C=[NH+]C1=C(O)N=C(N=C2CC3=C(C=CC4(C)C(O)CCC34)C3=CC(O)=C(O)C=C32)N=C1
59 434.1822806681 C=[NH+]C1=C(O)N=C(N=C2CC3=C(C=CC4(C)C(O)CCC34)C3=CC(O)=C(O)C=C32)N=C1N
60 436.1979307321 C=NC1=NC([NH+]=C2CC3=C(CCC4(C)C(O)CCC34)C3=CC(O)=C(O)C=C32)=NC(O)=C1N Intermediate Fragment 
61 419.1713816361 C=[NH+]C1=NC(N=C2CC3=C(C=CC4(C)C(O)CCC34)C3=CC(O)=C(O)C=C32)=NC(O)=C1
62 405.1557315721 CC12C=CC3=C(CC(=NC4=NC(O)=C([NH3+])C=N4)C4=CC(O)=C(O)C=C43)C1CCC2=O
63 436.1979307321 C=C1C(=CC(O)=CO)C2=C(CC1=[NH+]C1=NC(O)=C3N=CNC3=N1)C1CCC(O)C1(C)CC2 Intermediate Fragment 
64 408.2030161121 C=C1C(=[NH+]C2=NC(O)=C3N=CNC3=N2)CC2C(CCC3(C)C(O)CCC23)C1C#CO
65 406.1873660481 C=C1C(=[NH+]C2=NC(O)=C3N=CNC3=N2)CC2C(=C1C#CO)CCC1(C)C(O)CCC21
66 378.1924514281 C=C1C(=C)C2=C(CC1=[NH+]C1=NC(O)=C3N=CNC3=N1)C1CCC(O)C1(C)CC2
67 436.1979307321 CC12CCC3C(=C=CO)C(=C=CO)C(=[NH+]C4=NC(O)=C5N=CNC5=N4)CC3C1CCC2O Intermediate Fragment 
68 436.1979307321 C=C1C(=C=C(O)CO)C(=[NH+]C2=NC(O)=C3N=CNC3=N2)CC2=C1CCC1(C)C(O)CCC21 Intermediate Fragment 
69 406.1873660481 C=C1C(=C=CO)C(=[NH+]C2=NC(O)=C3N=CNC3=N2)CC2=C1CCC1(C)C(O)CCC21
70 408.2030161121 C=C1C(=C=CO)C(=[NH+]C2=NC(O)=C3N=CNC3=N2)CC2C1CCC1(C)C(O)CCC21
71 436.1979307321 C=C(O)C(O)=C=C1C=C2CCC3(C)C(O)CCC3C2CC1=[NH+]C1=NC(O)=C2N=CNC2=N1 Intermediate Fragment 
72 360.1455012361 C=C1C=C2C=CC3(C)C(=O)CCC3C2=CC1=[NH+]C1=NC(O)=C2N=CNC2=N1
73 362.1611513001 C=C1C=C2C=CC3(C)C(O)CCC3C2=CC1=[NH+]C1=NC(O)=C2N=CNC2=N1
74 364.1768013641 C=C1C=C2CCC3(C)C(O)CCC3C2=CC1=[NH+]C1=NC(O)=C2N=CNC2=N1
75 392.1717159841 CC12CCC3=CC(=C=CO)C(=[NH+]C4=NC(O)=C5N=CNC5=N4)C=C3C1CCC2O
76 436.1979307321 C=C(O)C(O)=C=C1CC(=[NH+]C2=NC(O)=C3N=CNC3=N2)CC2=C1CCC1(C)C(O)CCC21 Intermediate Fragment 
77 436.1979307321 CC1C(=C2C=C(O)C(O)=CC2=C=[NH+]C2=NC(O)=C3N=CNC3=N2)CCC2(C)C(O)CCC12 Intermediate Fragment 
78 165.1273915841 CC1=CC=CC2(C)C([OH2+])CCC12
79 436.1979307321 CC12CCC(C3=CC(O)=C(O)C=C3)=C(CC=[NH+]C3=NC(O)=C4N=CNC4=N3)C1CCC2O Intermediate Fragment 
80 178.0723362961 CC[NH2+]C1=NC(=O)C2=NC=NC2=N1
81 436.1979307321 CC12CCC=C(CC(=[NH+]C3=NC(O)=C4N=CNC4=N3)C3=CC(O)=C(O)C=C3)C1CCC2O Intermediate Fragment 
82 286.0934656641 CC(=[NH+]C1=NC(=O)C2=NC=NC2=N1)C1=CC(O)=C(O)CC1
83 149.0960914561 CC12C=CC=CC1CCC2=[OH+]
84 272.0778156001 O=C1N=C([NH+]=C=C2C=C(O)C(O)CC2)N=C2N=CN=C12
85 436.1979307321 CC(=[NH+]C1=NC(O)=C2N=CNC2=N1)C1=CC(O)=C(O)C=C1C1=CC2CCC(O)C2(C)CC1 Intermediate Fragment 
86 151.1117415201 CC12C=CC=CC1CCC2[OH2+]
87 436.1979307321 CC1=C(C2CCC(O)C2(C)C)CC(=[NH+]C2=NC(O)=C3N=CNC3=N2)C2=CC(O)=C(O)C=C21 Intermediate Fragment 
88 111.0804413921 CC1(C)CC=CC1=[OH+]
89 326.1247657921 CC1CCC(=[NH+]C2=NC(O)=C3N=CNC3=N2)C2=CC(O)=C(O)C=C21
90 436.1979307321 CCC1(C)C(O)CCC1C1=CC2=CC(O)=C(O)C=C2C(=[NH+]C2=NC(O)=C3N=CNC3=N2)C1 Intermediate Fragment 
91 125.0960914561 C#CC1(C)CCCC1[OH2+]
92 436.1979307321 CC1(CCC2=CCC(=[NH+]C3=NC(O)=C4N=CNC4=N3)C3=CC(O)=C(O)C=C23)CCCC1O Intermediate Fragment 
93 338.1247657921 CCC1=CCC(=[NH+]C2=NC(O)=C3N=CNC3=N2)C2=CC(O)=C(O)C=C12
94 336.1091157281 C=CC1=CCC(=[NH+]C2=NC(O)=C3N=CNC3=N2)C2=CC(O)=C(O)C=C12
95 95.0491412641 CC1=CC=CC1=[OH+]
96 97.0647913281 CC1=CCCC1=[OH+]
97 99.0804413921 CC1=CCCC1[OH2+]
98 101.0960914561 CC1CCCC1[OH2+]
99 324.1091157281 CC1=CCC(=[NH+]C2=NC(O)=C3N=CNC3=N2)C2=CC(O)=C(O)C=C12
100 322.0934656641 [CH2+]C1=CCC(=NC2=NC(O)=C3N=CNC3=N2)C2=CC(O)=C(O)C=C12
101 109.0647913281 [CH2+]C1(C)CC=CC1=O
102 113.0960914561 CC1(C)CCCC1=[OH+]
103 316.1404158561 O=C1N=C([NH+]=C2CCCC3CC(O)C(O)CC23)N=C2N=CN=C12
104 314.1247657921 O=C1N=C([NH+]=C2CCCC3CC(O)C(O)C=C23)N=C2N=CN=C12
105 312.1091157281 O=C1N=C([NH+]=C2CCCC3CC(O)=C(O)C=C23)N=C2N=CN=C12
106 310.0934656641 O=C1N=C([NH+]=C2CCCC3=CC(O)=C(O)C=C32)N=C2N=CN=C12
107 308.0778156001 O=C1N=C([NH+]=C2CC=CC3=CC(O)=C(O)C=C32)N=C2N=CN=C12
108 121.0647913281 C#CC1(C)CC=CC1=[OH+]
109 123.0804413921 C#CC1(C)CCCC1=[OH+]
110 127.1117415201 C=CC1(C)CCCC1[OH2+]
111 129.1273915841 CCC1(C)CCCC1[OH2+]
112 436.1979307321 CCC1=C(C2CCC(O)C2C)CC(=[NH+]C2=NC(O)=C3N=CNC3=N2)C2=CC(O)=C(O)C=C21 Intermediate Fragment 
113 340.1404158561 CCC1CCC(=[NH+]C2=NC(O)=C3N=CNC3=N2)C2=CC(O)=C(O)C=C21
114 436.1979307321 CC1CCC2=C(CC(=[NH+]C3=NC(O)=C4N=CNC4=N3)C3=CC(O)=C(O)C=C32)C1CCCO Intermediate Fragment 
115 406.1873660481 CCC1C2=C(CCC1C)C1=CC(O)=C(O)C=C1C(=[NH+]C1=NC(O)=C3N=CNC3=N1)C2
116 404.1717159841 CCC1=C(C)CCC2=C1CC(=[NH+]C1=NC(O)=C3N=CNC3=N1)C1=CC(O)=C(O)C=C12
117 392.1717159841 CC1CCC2=C(CC(=[NH+]C3=NC(O)=C4N=CNC4=N3)C3=CC(O)=C(O)C=C32)C1C
118 390.1560659201 CC1=C(C)C2=C(CC1)C1=CC(O)=C(O)C=C1C(=[NH+]C1=NC(O)=C3N=CNC3=N1)C2
119 384.2030161121 CC1CCC2C(CC(=[NH+]C3=NC(O)=C4N=CNC4=N3)C3=CC(O)C(O)CC32)C1
120 382.1873660481 CC1CCC2C(CC(=[NH+]C3=NC(O)=C4N=CNC4=N3)C3=CC(O)=C(O)CC32)C1
121 380.1717159841 CC1CCC2C3=CC(O)=C(O)C=C3C(=[NH+]C3=NC(O)=C4N=CNC4=N3)CC2C1
122 378.1560659201 CC1CCC2=C(CC(=[NH+]C3=NC(O)=C4N=CNC4=N3)C3=CC(O)=C(O)C=C32)C1
123 376.1404158561 CC1=CC2=C(CC1)C1=CC(O)=C(O)C=C1C(=[NH+]C1=NC(O)=C3N=CNC3=N1)C2
124 61.0647913281 CCC[OH2+]
125 436.1979307321 CCC(O)C1(C)CCC2=C(CC(=[NH+]C3=NC(O)=C4N=CNC4=N3)C3=CC(O)=C(O)C=C32)C1 Intermediate Fragment 
126 436.1979307321 CC(O)C1(C)CCC2=C(CC(=[NH+]C3=NC(O)=C4N=CNC4=N3)C3=CC(O)=C(O)C=C32)C1C Intermediate Fragment 
127 436.1979307321 CCC1C2=C(CCC1(C)CO)C1=CC(O)=C(O)C=C1C(=[NH+]C1=NC(O)=C3N=CNC3=N1)C2 Intermediate Fragment 
128 438.2135807961 CC12CCC3C4CC(O)=C(O)C=C4C(=[NH+]C4=NC(O)=C5N=CNC5=N4)CC3C1CCC2O
129 440.2292308601 CC12CCC3C4CC(O)C(O)C=C4C(=[NH+]C4=NC(O)=C5N=CNC5=N4)CC3C1CCC2O
130 442.2448809241 CC12CCC3C4CC(O)C(O)CC4C(=[NH+]C4=NC(O)=C5N=CNC5=N4)CC3C1CCC2O
131 424.2343162401 CC12CCCC1C1CC(=[NH+]C3=NC(O)=C4N=CNC4=N3)C3=CC(O)C(O)CC3C1CC2
132 442.2448809241 CC1(CCC2CCC(=[NH+]C3=NC(O)=C4N=CNC4=N3)C3=CC(O)C(O)CC32)CCCC1O Intermediate Fragment 
133 520.2190601001 CC12CCC3C4=CC(O)=C(O)C=C4C(=[NH+]C4=NC(O)=C5N=CN(C6CC(O)=CO6)C5=N4)CC3C1CCC2O
134 522.2347101641 CC12CCC3C4CC(O)=C(O)C=C4C(=[NH+]C4=NC(O)=C5N=CN(C6CC(O)=CO6)C5=N4)CC3C1CCC2O
135 524.2503602281 CC12CCC3C4CC(O)C(O)C=C4C(=[NH+]C4=NC(O)=C5N=CN(C6CC(O)=CO6)C5=N4)CC3C1CCC2O
136 534.2347101641 C=C1OC(N2C=NC3=C(O)N=C([NH+]=C4CC5C(CCC6(C)C(O)CCC56)C5CC(O)=C(O)C=C45)N=C32)C=C1O
137 516.2241454801 C=C1OC(N2C=NC3=C(O)N=C([NH+]=C4CC5C(CCC6(C)CCCC56)C5=CC(O)=C(O)C=C54)N=C32)C=C1O
138 534.2347101641 CC(=O)C(O)=CCN1C=NC2=C(O)N=C([NH+]=C3CC4C(CCC5(C)C(O)CCC45)C4=CC(O)=C(O)C=C43)N=C21 Intermediate Fragment 
139 534.2347101641 C=C(C=O)OC(C)N1C=NC2=C(O)N=C([NH+]=C3CC4C(CCC5(C)C(O)CCC45)C4=CC(O)=C(O)C=C43)N=C21 Intermediate Fragment 
140 534.2347101641 C=C1OC(N2C=NC3=C(O)N=C([NH+]=C4CCC(CCC5(C)CCCC5O)C5=CC(O)=C(O)C=C54)N=C32)C=C1O Intermediate Fragment 
141 534.2347101641 C=C1OC(N2C=NC3=C(O)N=C([NH+]=C4CC5C(CCC(C)C5CCCO)C5=CC(O)=C(O)C=C54)N=C32)C=C1O Intermediate Fragment 
142 306.2063701761 CC12CCC3C4CC(O)C(O)C=C4C(=[NH2+])CC3C1CCC2O
143 304.1907201121 CC12CCC3C4CC(O)=C(O)C=C4C(=[NH2+])CC3C1CCC2O
144 284.1645053641 CC12CCCC1C1=C(CC2)C2=CC(O)=C(O)C=C2C(=[NH2+])C1
145 302.1750700481 CC1(CCC2=CCC(=[NH2+])C3=CC(O)=C(O)C=C23)CCCC1O Intermediate Fragment 
146 300.1594199841 CC1(C=CC2=CCC(=[NH2+])C3=CC(O)=C(O)C=C23)CCCC1O Intermediate Fragment 
147 291.1954711441 CC12CCC3C4CC(O)C(O)=CC4=CCC3C1CCC2[OH2+]
148 291.1954711441 CC1(CCC2CCC=C3C=C(O)C(O)=CC32)CCCC1[OH2+] Intermediate Fragment 
149 289.1798210801 CC12CCC3C4C=C(O)C(O)=CC4=CCC3C1CCC2[OH2+]
150 289.1798210801 CC1(CCC2=C3C=C(O)C(O)=CC3=CCC2)CCCC1[OH2+] Intermediate Fragment 
151 269.1536063321 CC12CCCC1C1=CC=C3C=C(O)C([OH2+])=CC3=C1CC2
152 287.1641710161 CC1(CCC2=C3C=C(O)C(O)=CC3=CC=C2)CCCC1[OH2+] Intermediate Fragment 
153 267.1379562681 CC12C=CC3=C4C=C([OH2+])C(O)=CC4=CC=C3C1CCC2
154 285.1485209521 CC1(C=CC2=C3C=C(O)C(O)=CC3=CC=C2)CCCC1[OH2+] Intermediate Fragment 
155 265.1223062041 CC12C=CCC1C1=CC=C3C=C(O)C([OH2+])=CC3=C1C=C2
156 266.0883802841 [NH3+]C1=NC(O)=C2N=CN(C3CC(O)C(=CO)O3)C2=N1
157 268.1040303481 [NH3+]C1=NC(O)=C2N=CN(C3CC(O)C(CO)O3)C2=N1
158 250.0934656641 [NH3+]C1=NC(O)=C2N=CN(C3CCC(=CO)O3)C2=N1
159 268.1040303481 [NH3+]C1=NC(O)=C2N=CN(C(=O)CC(O)CCO)C2=N1 Intermediate Fragment 
160 534.2347101641 CC12CCCC1C1CC(=[NH+]C3=NC(O)=C4N=CN(C5C=C(O)C(=C=O)O5)C4=N3)C3=CC(O)C(O)CC3C1CC2
161 420.2030161121 CC12CCCC1C1CC(=[NH+]C3=NC(O)=C4N=CNC4=N3)C3=CC(O)=C(O)C=C3C1CC2
162 516.2241454801 CC12CCCC1C1CC(=[NH+]C3=NC(O)=C4N=CN(C5C=CC(=C=O)O5)C4=N3)C3=CC(O)=C(O)CC3C1CC2
163 534.2347101641 CC12CCCC1C1CC(=[NH+]C3=NC(O)=C4N=CN(CC=C(O)C(=O)C=O)C4=N3)C3=CC(O)=C(O)CC3C1CC2 Intermediate Fragment 
164 534.2347101641 CC12CCCC1C1CC(=[NH+]C3=NC(O)=C4N=CN(C(=O)C=C(O)C#CO)C4=N3)C3=CC(O)C(O)CC3C1CC2 Intermediate Fragment 
165 534.2347101641 CC12CCCC(CC(=[NH+]C3=NC(O)=C4N=CN(C5C=C(O)C(=C=O)O5)C4=N3)C3=CC(O)=C(O)CC3)C1CCC2 Intermediate Fragment 
166 404.1564598441 CC([NH2+]C1=NC(O)=C2N=CN(C3C=C(O)C(=C=O)O3)C2=N1)C1CCC(O)C(O)C1
167 402.1408097801 CC(=[NH+]C1=NC(O)=C2N=CN(C3C=C(O)C(=C=O)O3)C2=N1)C1CCC(O)C(O)C1
168 400.1251597161 CC(=[NH+]C1=NC(O)=C2N=CN(C3C=C(O)C(=C=O)O3)C2=N1)C1=CC(O)C(O)CC1
169 133.1011768361 [CH4+]C12C=CC=CC1CC=C2
170 135.1168269001 [CH4+]C12C=CC=CC1CCC2
171 390.1408097801 O=C=C1OC(N2C=NC3=C(O)N=C([NH2+]CC4CCC(O)C(O)C4)N=C32)C=C1O
172 534.2347101641 CCC1(C)CCCC1C1CC(=[NH+]C2=NC(O)=C3N=CN(C4C=C(O)C(=C=O)O4)C3=N2)C2=CC(O)=C(O)CC2C1 Intermediate Fragment 
173 109.1011768361 C#[CH+]C1(C)CCCC1
174 534.2347101641 CC1(CCC2CCC(=[NH+]C3=NC(O)=C4N=CN(C5C=C(O)C(=C=O)O5)C4=N3)C3=CC(O)=C(O)CC32)CCCC1 Intermediate Fragment 
175 111.1168269001 C=[CH2+]C1(C)CCCC1
176 534.2347101641 CCCC1C(C)CCC2C3CC(O)=C(O)C=C3C(=[NH+]C3=NC(O)=C4N=CN(C5C=C(O)C(=C=O)O5)C4=N3)CC12 Intermediate Fragment 
177 534.2347101641 CCC1C2CC(=[NH+]C3=NC(O)=C4N=CN(C5C=C(O)C(=C=O)O5)C4=N3)C3=CC(O)=C(O)CC3C2CCC1(C)C Intermediate Fragment 
178 534.2347101641 CC12CCC3C4CCC(O)C=C4C(=[NH+]C4=NC(O)=C5N=CN(C6C=C(O)C(=C=O)O6)C5=N4)CC3C1CCC2O
179 552.2452748481 CC12CCC3C4CC(O)C(O)CC4C(=[NH+]C(=N)N=C4C(=C=O)N=CN4C4C=C(O)C(=C=O)O4)CC3C1CCC2O Intermediate Fragment 
180 524.2503602281 CC12CCC3C4CC(O)C(O)C=C4C(=NC(=[NH2+])N=C4C=NCN4C4C=C(O)C(=C=O)O4)CC3C1CCC2O
181 552.2452748481 CC12CCC3C4CC(O)C(O)C=C4C(=[NH+]C(=NC=O)N=C4C=NCN4C4C=C(O)C(=C=O)O4)CC3C1CCC2O Intermediate Fragment 
182 509.2394611961 CC12CCC3C4CC(O)C(O)CC4=C(N=C=[NH+]C4=CN=CN4C4C=C(O)C(=C=O)O4)CC3C1CCC2O
183 507.2238111321 CC12CCC3C4CC(O)C(O)=CC4=C(N=C=[NH+]C4=CN=CN4C4C=C(O)C(=C=O)O4)CC3C1CCC2O
184 505.2081610681 CC12CCC3C4C=C(O)C(O)=CC4=C(N=C=[NH+]C4=CN=CN4C4C=C(O)C(=C=O)O4)CC3C1CCC2O
185 183.0764186321 OC1CC(N2C=CN=C2)OC1=C[OH2+]
186 181.0607685681 OC1CC(N2C=CN=C2)OC1=C=[OH+]
187 181.0607685681 O=C(C=[OH+])C(O)=CCN1C=CN=C1 Intermediate Fragment 
188 522.2347101641 CC12CCC3C4CC(O)=C(O)C=C4C(=NC(=[NH2+])N=C4C=NCN4C4C=C(O)C(=C=O)O4)CC3C1CCC2O
189 552.2452748481 CC12CCC3C4CC(O)C(O)C=C4C(=[NH+]C(N)=NC(=O)C4=CN(C5C=C(O)C(=C=O)O5)C=N4)CC3C1CCC2O Intermediate Fragment 
190 552.2452748481 CC12CCC3C4CC(O)C(O)CC4=C([NH+]=C=NC(O)=C4N=CN(C5C=C(O)C(=C=O)O5)C4=N)CC3C1CCC2O Intermediate Fragment 
191 198.0873176641 [NH2+]=C1C=NCN1C1CC(O)C(=CO)O1
192 198.0873176641 [NH2+]=C1C=NCN1C(=O)CC(O)C=CO Intermediate Fragment 
193 552.2452748481 CC12CCC3C4CC(O)C(O)CC4=C([NH+]=C=NC4=C(C(N)=O)N=CN4C4C=C(O)C(=C=O)O4)CC3C1CCC2O Intermediate Fragment 
194 552.2452748481 CC12CCC3C4CC(O)C(O)C=C4C(=[NH+]C4=NC(O)=CC(N(C=N)C5C=C(O)C(=C=O)O5)=N4)CC3C1CCC2O Intermediate Fragment 
195 525.2343758161 CC12CCC3C4CC(O)C(O)CC4C(=NC4=NC(O)=CC([NH+]=C5C=C(O)C(=C=O)O5)=N4)CC3C1CCC2O
196 525.2343758161 CC12CCC3C4CC(O)C(O)C=C4C(=NC4=NC(O)=CC([NH+]=C(O)C=C(O)C#CO)=N4)CC3C1CCC2O Intermediate Fragment 
197 159.0764186321 [NH+]#CNC1CC(O)C(CO)O1
198 142.0498695361 C=[NH+]C1CC(O)C(=C=O)O1
199 159.0764186321 [NH+]#CN=C(O)CC(O)CCO Intermediate Fragment 
200 159.0764186321 CC(O)C(CO)OC=NC#[NH+] Intermediate Fragment 
201 161.0920686961 [NH2+]=CNC1CC(O)C(CO)O1
202 144.0655196001 C=[NH+]C1CC(O)C(=CO)O1
203 552.2452748481 CC12CCC3C4CC(O)C(O)CC4C(=[NH+]C4=NC(O)=C(N=C=NC5C=C(O)C(=C=O)O5)C=N4)CC3C1CCC2O Intermediate Fragment 
204 552.2452748481 C=NC1=C(O)N=C([NH+]=C2CC3C(CCC4(C)C(O)CCC34)C3CC(O)C(O)CC23)N=C1N=C1C=C(O)C(=C=O)O1 Intermediate Fragment 
205 552.2452748481 CC12CCC3C4CC(O)C(O)C=C4C(=[NH+]C4=NC(O)=C5N=CN(CC=C(O)C(=O)C=O)C5=N4)CC3C1CCC2O Intermediate Fragment 
206 534.2347101641 CC12CCC3C4CC(O)=C(O)C=C4C(=[NH+]C4=NC(O)=C5N=CN(CC=C(O)C#CO)C5=N4)CC3C1CCC2O
207 516.2241454801 CC12CCCC1C1CC(=[NH+]C3=NC(O)=C4N=CN(CC=C(O)C#CO)C4=N3)C3=CC(O)=C(O)C=C3C1CC2
208 534.2347101641 CC1(CCC2CCC(=[NH+]C3=NC(O)=C4N=CN(CC=C(O)C#CO)C4=N3)C3=CC(O)=C(O)C=C32)CCCC1O Intermediate Fragment 
209 496.2554456081 CC12CCC3C4CC(O)C(O)CC4C(=[NH+]C4=NC(O)=C5N=CN(CC#CO)C5=N4)CC3C1CCC2O
210 494.2397955441 CC12CCC3C4CC(O)C(O)C=C4C(=[NH+]C4=NC(O)=C5N=CN(CC#CO)C5=N4)CC3C1CCC2O
211 476.2292308601 CC12CCCC1C1CC(=[NH+]C3=NC(O)=C4N=CN(CC#CO)C4=N3)C3=CC(O)=C(O)CC3C1CC2
212 492.2241454801 CC12CCC3C4CC(O)=C(O)C=C4C(=[NH+]C4=NC(O)=C5N=CN(CC#CO)C5=N4)CC3C1CCC2O
213 492.2241454801 CC1(CCC2CCC(=[NH+]C3=NC(O)=C4N=CN(CC#CO)C4=N3)C3=CC(O)=C(O)C=C32)CCCC1O Intermediate Fragment 
214 490.2084954161 CC12CCC3C4=CC(O)=C(O)C=C4C(=[NH+]C4=NC(O)=C5N=CN(CC#CO)C5=N4)CC3C1CCC2O
215 472.1979307321 CC12CCCC1C1=C(CC2)C2=CC(O)=C(O)C=C2C(=[NH+]C2=NC(O)=C3N=CN(CC#CO)C3=N2)C1
216 490.2084954161 CC1(CCC2=CCC(=[NH+]C3=NC(O)=C4N=CN(CC#CO)C4=N3)C3=CC(O)=C(O)C=C23)CCCC1O Intermediate Fragment 
217 464.2292308601 CCN1C=NC2=C(O)N=C([NH+]=C3CC4C(CCC5(C)C(O)CCC45)C4=CC(O)=C(O)C=C43)N=C21
218 446.2186661761 CCN1C=NC2=C(O)N=C([NH+]=C3CC4=C(CCC5(C)CCCC45)C4=CC(O)=C(O)C=C43)N=C21
219 464.2292308601 CCN1C=NC2=C(O)N=C([NH+]=C3CC=C(CCC4(C)CCCC4O)C4=CC(O)=C(O)C=C43)N=C21 Intermediate Fragment 
220 462.2135807961 CCN1C=NC2=C(O)N=C([NH+]=C3CC4=C(CCC5(C)C(O)CCC45)C4=CC(O)=C(O)C=C43)N=C21
221 460.1979307321 CCN1C=NC2=C(O)N=C([NH+]=C3CC4=C(C=CC5(C)C(O)CCC45)C4=CC(O)=C(O)C=C43)N=C21
222 552.2452748481 C=C(O)C(=C=O)OCN1C=NC2=C(O)N=C([NH+]=C3CC4C(CCC5(C)C(O)CCC45)C4CC(O)C(O)C=C34)N=C21 Intermediate Fragment 
223 552.2452748481 CC(OC(=C=O)C=O)N1C=NC2=C(O)N=C([NH+]=C3CC4C(CCC5(C)C(O)CCC45)C4CC(O)C(O)C=C34)N=C21 Intermediate Fragment 
224 75.0440558841 OCC=C[OH2+]
225 73.0284058201 OCC#C[OH2+]
226 478.2084954161 CC(=O)N1C=NC2=C(O)N=C([NH+]=C3CC4C(CCC5(C)C(O)CCC45)C4=CC(O)=C(O)C=C43)N=C21
227 478.2084954161 CC(=O)N1C=NC2=C(O)N=C([NH+]=C3CC=C(CCC4(C)CCCC4O)C4=CC(O)=C(O)C=C43)N=C21 Intermediate Fragment 
228 480.2241454801 CC(=O)N1C=NC2=C(O)N=C([NH+]=C3CC4C(CCC5(C)C(O)CCC45)C4CC(O)=C(O)C=C34)N=C21
229 462.2135807961 CC(=O)N1C=NC2=C(O)N=C([NH+]=C3CC4C(CCC5(C)CCCC45)C4=CC(O)=C(O)C=C43)N=C21
230 480.2241454801 CC(=O)N1C=NC2=C(O)N=C([NH+]=C3CCC(CCC4(C)CCCC4O)C4=CC(O)=C(O)C=C43)N=C21 Intermediate Fragment 
231 480.2241454801 CC(=O)N1C=NC2=C(O)N=C([NH+]=C3CC4C(CCC(C)C4CCCO)C4=CC(O)=C(O)C=C43)N=C21 Intermediate Fragment 
232 552.2452748481 CC12CCC3C4CC(O)C(O)CC4C(=[NH+]C4=NC(O)=C5N=CN(C(C=C=O)OC#CO)C5=N4)CC3C1CCC2O Intermediate Fragment 
233 47.0491412641 CC[OH2+]
234 508.2190601001 CC12CCC3C4CC(O)C(O)C=C4C(=[NH+]C4=NC(O)=C5N=CN(C(=O)C#CO)C5=N4)CC3C1CCC2O
235 510.2347101641 CC12CCC3C4CC(O)C(O)CC4C(=[NH+]C4=NC(O)=C5N=CN(C(=O)C#CO)C5=N4)CC3C1CCC2O
236 492.2241454801 CC12CCCC1C1CC(=[NH+]C3=NC(O)=C4N=CN(C(=O)C#CO)C4=N3)C3=CC(O)C(O)CC3C1CC2
237 510.2347101641 CC1(CCC2CCC(=[NH+]C3=NC(O)=C4N=CN(C(=O)C#CO)C4=N3)C3=CC(O)C(O)CC32)CCCC1O Intermediate Fragment 
238 510.2347101641 CC12CCC3C4CC(O)C(O)C=C4C(=[NH+]C4=NC(O)=C5N=CN(COC#CO)C5=N4)CC3C1CCC2O
239 508.2190601001 CC12CCC3C4CC(O)=C(O)C=C4C(=[NH+]C4=NC(O)=C5N=CN(COC#CO)C5=N4)CC3C1CCC2O
240 552.2452748481 CC12CCC3C4CC(O)C(O)CC4C(=[NH+]C4=NC(O)=C5N=CN(C(=O)C=C(O)C#CO)C5=N4)CC3C1CCC2O Intermediate Fragment 
241 506.2034100361 CC12CCC3C4CC(O)=C(O)C=C4C(=[NH+]C4=NC(O)=C5N=CN(C(=O)C#CO)C5=N4)CC3C1CCC2O
242 488.1928453521 CC12CCCC1C1CC(=[NH+]C3=NC(O)=C4N=CN(C(=O)C#CO)C4=N3)C3=CC(O)=C(O)C=C3C1CC2
243 506.2034100361 CC1(CCC2CCC(=[NH+]C3=NC(O)=C4N=CN(C(=O)C#CO)C4=N3)C3=CC(O)=C(O)C=C32)CCCC1O Intermediate Fragment 
244 45.0334912001 C=C[OH2+]
245 484.2554456081 CC(=O)N1C=NC2=C(O)N=C([NH+]=C3CC4C(CCC5(C)C(O)CCC45)C4CC(O)C(O)CC34)N=C21
246 482.2397955441 CC(=O)N1C=NC2=C(O)N=C([NH+]=C3CC4C(CCC5(C)C(O)CCC45)C4CC(O)C(O)C=C34)N=C21
247 476.1928453521 CC(=O)N1C=NC2=C(O)N=C([NH+]=C3CC4=C(CCC5(C)C(O)CCC45)C4=CC(O)=C(O)C=C43)N=C21
248 476.1928453521 CC(=O)N1C=NC2=C(O)N=C([NH+]=C3CC=C(C=CC4(C)CCCC4O)C4=CC(O)=C(O)C=C43)N=C21 Intermediate Fragment 
249 68.9971056921 [O+]#CC#CO
250 71.0127557561 O=CC#C[OH2+]
251 43.0178411361 C#C[OH2+]
252 77.0597059481 OCCC[OH2+]
253 468.2241454801 CC12CCC3C4CC(O)C(O)C=C4C(=[NH+]C4=NC(O)=C5N=CN(C=O)C5=N4)CC3C1CCC2O
254 466.2084954161 CC12CCC3C4CC(O)=C(O)C=C4C(=[NH+]C4=NC(O)=C5N=CN(C=O)C5=N4)CC3C1CCC2O
255 448.1979307321 CN1C=NC2=C(O)N=C([NH+]=C3CC4=C(CCC5(C)C(O)CCC45)C4=CC(O)=C(O)C=C43)N=C21
256 448.1979307321 CC12CCCC1C1CC(=[NH+]C3=NC(O)=C4N=CN(C=O)C4=N3)C3=CC(O)=C(O)C=C3C1CC2
257 466.2084954161 CC1(CCC2CCC(=[NH+]C3=NC(O)=C4N=CN(C=O)C4=N3)C3=CC(O)=C(O)C=C32)CCCC1O Intermediate Fragment 
258 464.1928453521 CC12CCC3C4=CC(O)=C(O)C=C4C(=[NH+]C4=NC(O)=C5N=CN(C=O)C5=N4)CC3C1CCC2O
259 464.1928453521 CC1(CCC2=CCC(=[NH+]C3=NC(O)=C4N=CN(C=O)C4=N3)C3=CC(O)=C(O)C=C23)CCCC1O Intermediate Fragment 
260 462.1771952881 CC12CCC3=C(CC(=[NH+]C4=NC(O)=C5N=CN(C=O)C5=N4)C4=CC(O)=C(O)C=C43)C1CCC2O
261 462.1771952881 CC1(C=CC2=CCC(=[NH+]C3=NC(O)=C4N=CN(C=O)C4=N3)C3=CC(O)=C(O)C=C23)CCCC1O Intermediate Fragment 
262 87.0440558841 CC(O)C#C[OH2+]
263 71.0491412641 C#CC(C)[OH2+]
264 71.0491412641 CCC#C[OH2+]
265 91.0753560121 CC(O)CC[OH2+]
266 73.0647913281 C=CC(C)[OH2+]
267 552.2452748481 C=C1C(=[NH+]C2=NC(O)=C3N=CN(C4C=C(O)C(=C=O)O4)C3=N2)CC2C(CCC3(C)C(O)CCC23)C1CC(O)CO Intermediate Fragment 
268 524.2503602281 CC1C(=[NH+]C2=NC(O)=C3N=CN(C4C=C(O)C(=C=O)O4)C3=N2)CC2C(CCC3(C)C(O)CCC23)C1CCO
269 522.2347101641 C=C1C(=[NH+]C2=NC(O)=C3N=CN(C4C=C(O)C(=C=O)O4)C3=N2)CC2C(CCC3(C)C(O)CCC23)C1CCO
270 496.2554456081 CC1C([NH2+]C2=NC(O)=C3N=CN(C4C=C(O)C(=C=O)O4)C3=N2)CC2C(CCC3(C)C(O)CCC23)C1C
271 478.2448809241 CC1C(=[NH+]C2=NC(O)=C3N=CN(C4C=C(O)C(=C=O)O4)C3=N2)CC2C(CCC3(C)CCCC23)C1C
272 494.2397955441 CC1C(=[NH+]C2=NC(O)=C3N=CN(C4C=C(O)C(=C=O)O4)C3=N2)CC2C(CCC3(C)C(O)CCC23)C1C
273 492.2241454801 C=C1C(=[NH+]C2=NC(O)=C3N=CN(C4C=C(O)C(=C=O)O4)C3=N2)CC2C(CCC3(C)C(O)CCC23)C1C
274 490.2084954161 C=C1C(=C)C2CCC3(C)C(O)CCC3C2CC1=[NH+]C1=NC(O)=C2N=CN(C3C=C(O)C(=C=O)O3)C2=N1
275 478.2084954161 C=C1CC2CCC3(C)C(O)CCC3C2CC1=[NH+]C1=NC(O)=C2N=CN(C3C=C(O)C(=C=O)O3)C2=N1
276 476.1928453521 C=C1C=C2CCC3(C)C(O)CCC3C2CC1=[NH+]C1=NC(O)=C2N=CN(C3C=C(O)C(=C=O)O3)C2=N1
277 552.2452748481 CC12CCC3C(CCO)C(=CCO)C(=[NH+]C4=NC(O)=C5N=CN(C6C=C(O)C(=C=O)O6)C5=N4)CC3C1CCC2O Intermediate Fragment 
278 506.2034100361 CC12CCC3C(C#CO)CC(=[NH+]C4=NC(O)=C5N=CN(C6C=C(O)C(=C=O)O6)C5=N4)CC3C1CCC2O
279 508.2190601001 CC12CCC3C(C=CO)CC(=[NH+]C4=NC(O)=C5N=CN(C6C=C(O)C(=C=O)O6)C5=N4)CC3C1CCC2O
280 510.2347101641 CC12CCC3C(CCO)CC(=[NH+]C4=NC(O)=C5N=CN(C6C=C(O)C(=C=O)O6)C5=N4)CC3C1CCC2O
281 524.2503602281 CC1C(CCO)C(=[NH+]C2=NC(O)=C3N=CN(C4C=C(O)C(=C=O)O4)C3=N2)CC2C1CCC1(C)C(O)CCC21
282 524.2503602281 CC1C(CCO)C(=[NH+]C2=NC(O)=C3N=CN(C(=O)C=C(O)C#CO)C3=N2)CC2C1CCC1(C)C(O)CCC21 Intermediate Fragment 
283 552.2452748481 CC1C(=CC(O)CO)C(=[NH+]C2=NC(O)=C3N=CN(C4C=C(O)C(=C=O)O4)C3=N2)CC2C1CCC1(C)C(O)CCC21 Intermediate Fragment 
284 520.2190601001 CC1C(=C=CO)C(=[NH+]C2=NC(O)=C3N=CN(C4C=C(O)C(=C=O)O4)C3=N2)CC2C1CCC1(C)C(O)CCC21
285 522.2347101641 CC1C(=CCO)C(=[NH+]C2=NC(O)=C3N=CN(C4C=C(O)C(=C=O)O4)C3=N2)CC2C1CCC1(C)C(O)CCC21
286 552.2452748481 CC(O)C(O)C=C1CC2CCC3(C)C(O)CCC3C2CC1=[NH+]C1=NC(O)=C2N=CN(C3C=C(O)C(=C=O)O3)C2=N1 Intermediate Fragment 
287 552.2452748481 C=C(O)C(O)CC1CC(=[NH+]C2=NC(O)=C3N=CN(C4C=C(O)C(=C=O)O4)C3=N2)CC2C1CCC1(C)C(O)CCC21 Intermediate Fragment 
288 480.2241454801 CC1CC(=[NH+]C2=NC(O)=C3N=CN(C4C=C(O)C(=C=O)O4)C3=N2)CC2C1CCC1(C)C(O)CCC21
289 476.1928453521 C=C1CC(=[NH+]C2=NC(O)=C3N=CN(C4C=C(O)C(=C=O)O4)C3=N2)CC2=C1CCC1(C)C(O)CCC21
290 464.1928453521 CC12CCC3=CCC(=[NH+]C4=NC(O)=C5N=CN(C6C=C(O)C(=C=O)O6)C5=N4)CC3C1CCC2O
291 552.2452748481 CC1C(C2CC(O)C(O)CC2=C=[NH+]C2=NC(O)=C3N=CN(C4C=C(O)C(=C=O)O4)C3=N2)CCC2(C)C(O)CCC12 Intermediate Fragment 
292 388.1251597161 O=C=C1OC(N2C=NC3=C(O)N=C([NH+]=CC4CCC(O)C(O)C4)N=C32)C=C1O
293 552.2452748481 CC12CCC(C3C=CC(O)C(O)C3)C(CC=[NH+]C3=NC(O)=C4N=CN(C5C=C(O)C(=C=O)O5)C4=N3)C1CCC2O Intermediate Fragment 
294 444.2241454801 CC12CCCC(CC[NH2+]C3=NC(O)=C4N=CN(C5CC(O)C(=C=O)O5)C4=N3)C1CCC2O
295 442.2084954161 CC12CCCC(CC[NH2+]C3=NC(O)=C4N=CN(C5C=C(O)C(=C=O)O5)C4=N3)C1CCC2O
296 328.1768013641 CC12CCC=C(CC=[NH+]C3=NC(O)=C4N=CNC4=N3)C1CCC2O
297 440.1928453521 CC12CCCC(CC=[NH+]C3=NC(O)=C4N=CN(C5C=C(O)C(=C=O)O5)C4=N3)C1CCC2O
298 438.1771952881 CC12CCC=C(CC=[NH+]C3=NC(O)=C4N=CN(C5C=C(O)C(=C=O)O5)C4=N3)C1CCC2O
299 294.1196804121 CC[NH2+]C1=NC(O)=C2N=CN(C3CC(O)C(=CO)O3)C2=N1
300 294.1196804121 CC[NH2+]C1=NC(O)=C2N=CN(C(=O)CC(O)C=CO)C2=N1 Intermediate Fragment 
301 552.2452748481 CC12CCCC(CC(=[NH+]C3=NC(O)=C4N=CN(C5C=C(O)C(=C=O)O5)C4=N3)C3=CC(O)C(O)CC3)C1CCC2O Intermediate Fragment 
302 404.1564598441 CC([NH2+]C1=NC(O)=C2N=CN(C(=O)C=C(O)C#CO)C2=N1)C1CCC(O)C(O)C1 Intermediate Fragment 
303 152.0706049801 CC(=[NH2+])C1=CC(O)C(=O)C=C1
304 402.1408097801 CC(=[NH+]C1=NC(O)=C2N=CN(C(=O)C=C(O)C#CO)C2=N1)C1CCC(O)C(O)C1 Intermediate Fragment 
305 398.1095096521 CC(=[NH+]C1=NC(O)=C2N=CN(C3C=C(O)C(=C=O)O3)C2=N1)C1=CC(O)=C(O)CC1
306 147.0804413921 CC12C=CC=CC1C=CC2=[OH+]
307 147.0804413921 CC1=C(CC=C=[OH+])C=CC=C1 Intermediate Fragment 
308 131.0855267721 [CH4+]C12C=CC=CC1=CC=C2
309 149.0960914561 CC1=C(CCC=[OH+])C=CC=C1 Intermediate Fragment 
310 151.1117415201 CC1=C(CCC[OH2+])C=CC=C1 Intermediate Fragment 
311 153.1273915841 CC12CCC=CC1CCC2[OH2+]
312 390.1408097801 O=C(C=C(O)C#CO)N1C=NC2=C(O)N=C([NH2+]CC3CCC(O)C(O)C3)N=C21 Intermediate Fragment 
313 163.1117415201 CC1=CC=CC2(C)C(=[OH+])CCC12
314 552.2452748481 CC(=[NH+]C1=NC(O)=C2N=CN(C3C=C(O)C(=C=O)O3)C2=N1)C1=CC(O)C(O)CC1C1CCC2(C)C(O)CCC2C1 Intermediate Fragment 
315 552.2452748481 CC1C2CC(O)C(O)C=C2C(=[NH+]C2=NC(O)=C3N=CN(C4C=C(O)C(=C=O)O4)C3=N2)CC1C1CCC(O)C1(C)C Intermediate Fragment 
316 438.1408097801 CC1CCC(=[NH+]C2=NC(O)=C3N=CN(C4C=C(O)C(=C=O)O4)C3=N2)C2=CC(O)=C(O)CC21
317 440.1564598441 CC1CCC(=[NH+]C2=NC(O)=C3N=CN(C4C=C(O)C(=C=O)O4)C3=N2)C2=CC(O)C(O)CC21
318 442.1721099081 CC1CCC(=[NH+]C2=NC(O)=C3N=CN(C4C=C(O)C(=C=O)O4)C3=N2)C2CC(O)C(O)CC12
319 442.1721099081 CC1CCC(=[NH+]C2=NC(O)=C3N=CN(C(=O)C=C(O)C#CO)C3=N2)C2CC(O)C(O)CC12 Intermediate Fragment 
320 552.2452748481 CCC1(C)C(O)CCC1C1CC(=[NH+]C2=NC(O)=C3N=CN(C4C=C(O)C(=C=O)O4)C3=N2)C2=CC(O)C(O)CC2C1 Intermediate Fragment 
321 107.0855267721 C#[CH+]C1(C)C=CCC1
322 125.0960914561 C#CC(C)=CCCC[OH2+] Intermediate Fragment 
323 426.1408097801 O=C=C1OC(N2C=NC3=C(O)N=C([NH+]=C4CCCC5CC(O)C(O)C=C45)N=C32)C=C1O
324 428.1564598441 O=C=C1OC(N2C=NC3=C(O)N=C([NH+]=C4CCCC5CC(O)C(O)CC45)N=C32)C=C1O
325 552.2452748481 CC1(CCC2CCC(=[NH+]C3=NC(O)=C4N=CN(C5C=C(O)C(=C=O)O5)C4=N3)C3=CC(O)C(O)CC32)CCCC1O Intermediate Fragment 
326 454.1721099081 CCC1CCC(=[NH+]C2=NC(O)=C3N=CN(C4C=C(O)C(=C=O)O4)C3=N2)C2=CC(O)C(O)CC21
327 452.1564598441 CCC1CCC(=[NH+]C2=NC(O)=C3N=CN(C4C=C(O)C(=C=O)O4)C3=N2)C2=CC(O)=C(O)CC21
328 77.0385765801 [CH+]=C1C=CC=C1
329 95.0491412641 C=C=C=C(C)C=[OH+] Intermediate Fragment 
330 67.0542266441 C=C[CH+]#CC
331 95.0491412641 C=CC(=O)C(=C)[CH2+] Intermediate Fragment 
332 79.0542266441 [CH2+]C1=CCC=C1
333 97.0647913281 CC=CCC=C=[OH+] Intermediate Fragment 
334 81.0698767081 [CH4+]C1=CCC=C1
335 99.0804413921 CC=CCCC=[OH+] Intermediate Fragment 
336 83.0855267721 [CH4+]C1=CCCC1
337 109.0647913281 C=C=CC(=[OH+])C(=C)C Intermediate Fragment 
338 109.0647913281 CC(C)=CC=C=C=[OH+] Intermediate Fragment 
339 95.0855267721 CC1([CH4+])C=CC=C1
340 113.0960914561 C=CC(C)(C)C(C)=[OH+] Intermediate Fragment 
341 432.1877599721 O=C=C1OC(N2C=NC3=C(O)N=C([NH2+]C4CCCC5CC(O)C(O)CC54)N=C32)CC1O
342 430.1721099081 O=C=C1OC(N2C=NC3=C(O)N=C([NH2+]C4CCCC5CC(O)C(O)CC54)N=C32)C=C1O
343 412.1615452241 C=C1OC(N2C=NC3=C(O)N=C([NH+]=C4CCCC5CC(O)C(O)C=C45)N=C32)C=C1O
344 412.1615452241 O=C=C1C=CC(N2C=NC3=C(O)N=C([NH+]=C4CCCC5CC(O)C(O)CC45)N=C32)O1
345 178.0862550441 [NH2+]=C1CC=CC2=CC(O)C(O)C=C12
346 430.1721099081 O=C(C=C(O)C#CO)N1C=NC2=C(O)N=C([NH2+]C3CCCC4CC(O)C(O)CC43)N=C21 Intermediate Fragment 
347 412.1615452241 OC#CC(O)=CCN1C=NC2=C(O)N=C([NH+]=C3CCCC4CC(O)C(O)C=C34)N=C21
348 386.1458951601 O=C(C#CO)N1C=NC2=C(O)N=C([NH+]=C3CCCC4CC(O)C(O)CC34)N=C21
349 384.1302450961 O=C(C#CO)N1C=NC2=C(O)N=C([NH+]=C3CCCC4CC(O)C(O)C=C34)N=C21
350 424.1251597161 O=C=C1OC(N2C=NC3=C(O)N=C([NH+]=C4CCCC5CC(O)=C(O)C=C45)N=C32)C=C1O
351 119.0491412641 [C+]#CC1(C)CC=CC1=O
352 93.0334912001 [CH2+]C1=CC=CC1=O
353 121.0647913281 C#CC(C)=CCC=C=[OH+] Intermediate Fragment 
354 105.0698767081 C#[CH+]C1(C)C=CC=C1
355 129.1273915841 C=CC(C)(CC)C(C)[OH2+] Intermediate Fragment 
356 552.2452748481 CCC1C2CC(O)C(O)C=C2C(=[NH+]C2=NC(O)=C3N=CN(C4C=C(O)C(=C=O)O4)C3=N2)CC1C1CCC(O)C1C Intermediate Fragment 
357 552.2452748481 CC1CCC2C3CC(O)C(O)C=C3C(=[NH+]C3=NC(O)=C4N=CN(C5C=C(O)C(=C=O)O5)C4=N3)CC2C1CCCO Intermediate Fragment 
358 524.2503602281 CCC1C(C)CCC2C3CC(O)C(O)CC3C(=[NH+]C3=NC(O)=C4N=CN(C5C=C(O)C(=C=O)O5)C4=N3)CC12
359 408.2030161121 CCC1C(C)CCC2C3=CC(O)=C(O)C=C3C(=[NH+]C3=NC(O)=C4N=CNC4=N3)CC21
360 524.2503602281 CCC1C(C)CCC2C3CC(O)C(O)CC3C(=[NH+]C3=NC(O)=C4N=CN(C(=O)C=C(O)C#CO)C4=N3)CC12 Intermediate Fragment 
361 522.2347101641 CCC1C(C)CCC2C3CC(O)C(O)C=C3C(=[NH+]C3=NC(O)=C4N=CN(C5C=C(O)C(=C=O)O5)C4=N3)CC12
362 520.2190601001 CCC1C(C)CCC2C3CC(O)=C(O)C=C3C(=[NH+]C3=NC(O)=C4N=CN(C5C=C(O)C(=C=O)O5)C4=N3)CC12
363 510.2347101641 CC1CCC2C3CC(O)C(O)CC3C(=[NH+]C3=NC(O)=C4N=CN(C5C=C(O)C(=C=O)O5)C4=N3)CC2C1C
364 394.1873660481 CC1CCC2C3=CC(O)=C(O)C=C3C(=[NH+]C3=NC(O)=C4N=CNC4=N3)CC2C1C
365 510.2347101641 CC1CCC2C3CC(O)C(O)CC3C(=[NH+]C3=NC(O)=C4N=CN(C(=O)C=C(O)C#CO)C4=N3)CC2C1C Intermediate Fragment 
366 508.2190601001 CC1CCC2C3CC(O)C(O)C=C3C(=[NH+]C3=NC(O)=C4N=CN(C5C=C(O)C(=C=O)O5)C4=N3)CC2C1C
367 506.2034100361 CC1CCC2C3CC(O)=C(O)C=C3C(=[NH+]C3=NC(O)=C4N=CN(C5C=C(O)C(=C=O)O5)C4=N3)CC2C1C
368 500.2503602281 CC1CCC2C(C1)CC([NH2+]C1=NC(O)=C3N=CN(C4CC(O)C(=C=O)O4)C3=N1)C1CC(O)C(O)CC21
369 498.2347101641 CC1CCC2C(C1)CC([NH2+]C1=NC(O)=C3N=CN(C4C=C(O)C(=C=O)O4)C3=N1)C1CC(O)C(O)CC21
370 498.2347101641 CC1CCC2C(C1)CC([NH2+]C1=NC(O)=C3N=CN(C(=O)C=C(O)C#CO)C3=N1)C1CC(O)C(O)CC21 Intermediate Fragment 
371 496.2190601001 CC1CCC2C(CC(=[NH+]C3=NC(O)=C4N=CN(C5C=C(O)C(=C=O)O5)C4=N3)C3CC(O)C(O)CC32)C1
372 496.2190601001 CC1CCC2C(CC(=[NH+]C3=NC(O)=C4N=CN(C(=O)C=C(O)C#CO)C4=N3)C3CC(O)C(O)CC32)C1 Intermediate Fragment 
373 494.2034100361 CC1CCC2C(CC(=[NH+]C3=NC(O)=C4N=CN(C5C=C(O)C(=C=O)O5)C4=N3)C3=CC(O)C(O)CC32)C1
374 492.1877599721 CC1CCC2C(CC(=[NH+]C3=NC(O)=C4N=CN(C5C=C(O)C(=C=O)O5)C4=N3)C3=CC(O)=C(O)CC32)C1
375 552.2452748481 CCC(O)C1(C)CCC2C(CC(=[NH+]C3=NC(O)=C4N=CN(C5C=C(O)C(=C=O)O5)C4=N3)C3=CC(O)C(O)CC32)C1 Intermediate Fragment 
376 552.2452748481 CC(O)C1(C)CCC2C3CC(O)C(O)C=C3C(=[NH+]C3=NC(O)=C4N=CN(C5C=C(O)C(=C=O)O5)C4=N3)CC2C1C Intermediate Fragment 
377 552.2452748481 CCC1C2CC(=[NH+]C3=NC(O)=C4N=CN(C5C=C(O)C(=C=O)O5)C4=N3)C3=CC(O)C(O)CC3C2CCC1(C)CO Intermediate Fragment 
