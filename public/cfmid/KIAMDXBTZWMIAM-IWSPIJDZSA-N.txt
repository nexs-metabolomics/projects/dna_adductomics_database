#In-silico ESI-MS/MS [M+H]+ Spectra
#PREDICTED BY CFM-ID 4.0.8
#SMILES=n1cnc2c(c1NCCO)ncn2[C@@H]1O[C@H](CO)[C@@H](C1)O
#InChiKey=KIAMDXBTZWMIAM-IWSPIJDZSA-N
energy0
162.07742 33.84 38 (16.144)
180.08799 100.00 49 82 60 85 95 88 75 55 78 91 (46.99 0.42164 0.073483 0.057936 0.042433 0.034267 0.030141 0.029069 0.021409 0.004113)
296.13533 59.30 0 163 146 160 141 122 144 110 142 143 140 157 (27.122 0.94354 0.15898 0.029832 0.0095308 0.00924 0.0064102 0.0048298 0.00097608 0.00078683 0.00066198 0.00045479)
energy1
134.04612 27.63 10 (10.127)
136.06177 40.04 11 (14.678)
162.07742 58.05 38 (21.282)
180.08799 100.00 49 78 88 60 82 91 85 95 55 75 (34.552 0.45553 0.41819 0.36434 0.27434 0.16849 0.13029 0.10971 0.098928 0.088698)
energy2
75.04406 2.75 26 (1.2269)
94.03997 8.94 64 (3.9927)
107.03522 4.75 72 (2.1194)
121.05087 4.13 2 (1.8428)
134.04612 13.70 10 (6.1158)
136.06177 100.00 11 (44.645)
138.07742 2.92 53 (1.3026)
148.06177 4.91 31 (2.192)
150.07742 6.20 32 (2.7663)
151.06144 2.49 93 81 83 (1.0396 0.035164 0.034871)
153.07709 3.53 94 76 57 89 (0.8457 0.33781 0.28023 0.11384)
162.07742 8.89 38 (3.9694)
163.06144 4.43 77 90 96 56 (0.56822 0.51039 0.46294 0.43724)
180.08799 6.39 60 49 91 82 78 75 55 95 85 88 (0.86688 0.69674 0.47475 0.20526 0.17237 0.14565 0.11413 0.080595 0.050478 0.045818)
234.09855 6.42 14 21 13 153 (1.5485 0.81156 0.50375 0.0016754)

0 296.1353304761 OCC[NH2+]c1ncnc2c1ncn2C1CC(O)C(CO)O1
1 235.0825666321 OC1CC(N2C=NC3=CN=CN=C32)OC1=C[OH2+]
2 121.0508725801 C1=NC2=NC=[NH+]CC2=N1
3 237.0982166961 OC1CC(N2C=NC3=CN=CN=C32)OC1C[OH2+]
4 219.0876520121 [OH2+]C=C1CCC(N2C=NC3=CN=CN=C32)O1
5 237.0982166961 O=C(CC(O)CC[OH2+])N1C=NC2=CN=CN=C21 Intermediate Fragment 
6 239.1138667601 OC1CC(N2CNC3=CN=CN=C32)OC1C[OH2+]
7 241.1295168241 OC1CC(N2CNC3CN=CN=C32)OC1C[OH2+]
8 45.0334912001 C=C[OH2+]
9 250.0934656641 [NH3+]C1=C2N=CN(C3CC(O)C(=CO)O3)C2=NC=N1
10 134.0461215481 [NH2+]=C1N=CN=C2N=CN=C12
11 136.0617716121 [NH2+]=C1N=CN=C2NCN=C12
12 252.1091157281 [NH3+]C1=C2N=CN(C3CC(O)C(CO)O3)C2=NC=N1
13 234.0985510441 C=C1OC(N2C=NC3=C([NH3+])N=CN=C32)CC1O
14 234.0985510441 [NH3+]C1=C2N=CN(C3CCC(=CO)O3)C2=NC=N1
15 252.1091157281 N=CN(C1=NC=NC([NH3+])=C1)C1CC(O)C(=CO)O1 Intermediate Fragment 
16 252.1091157281 [NH3+]C1=C2N=CN(CCC(O)C(=O)CO)C2=NC=N1 Intermediate Fragment 
17 252.1091157281 CC(OC(=CO)CO)N1C=NC2=C([NH3+])N=CN=C21 Intermediate Fragment 
18 178.0723362961 CC(=O)N1C=NC2=C([NH3+])N=CN=C21
19 252.1091157281 [NH3+]C1=C2N=CN(C(CCO)OC=CO)C2=NC=N1 Intermediate Fragment 
20 252.1091157281 [NH3+]C1=C2N=CN(C(=O)CC(O)CCO)C2=NC=N1 Intermediate Fragment 
21 234.0985510441 [NH3+]C1=C2N=CN(CCC(O)C#CO)C2=NC=N1
22 208.0829009801 [NH3+]C1=C2N=CN(C(=O)CCO)C2=NC=N1
23 206.0672509161 [NH3+]C1=C2N=CN(C(=O)C=CO)C2=NC=N1
24 182.1036364241 CC(O)N1CNC2=C([NH3+])N=CN=C21
25 73.0284058201 OCC#C[OH2+]
26 75.0440558841 OCC=C[OH2+]
27 164.0566862321 [NH3+]C1=C2N=CN(C=O)C2=NC=N1
28 89.0597059481 CC(O)C=C[OH2+]
29 254.1247657921 [NH3+]C1=C2NCN(C3CC(O)C(CO)O3)C2=NC=N1
30 264.1091157281 C=[NH+]C1=C2N=CN(C3CC(O)C(CO)O3)C2=NC=N1
31 148.0617716121 C=[NH+]C1=C2N=CNC2=NC=N1
32 150.0774216761 C=[NH+]C1=C2NCNC2=NC=N1
33 266.1247657921 C=[NH+]C1=C2NCN(C3CC(O)C(CO)O3)C2=NC=N1
34 266.1247657921 C=[NH+]C1=C2N=CN(C(C)OC(CO)CO)C2=NC=N1 Intermediate Fragment 
35 266.1247657921 C=[NH+]C1=C2N=CN(C(O)CC(O)CCO)C2=NC=N1 Intermediate Fragment 
36 268.1404158561 C[NH2+]C1=C2NCN(C3CC(O)C(CO)O3)C2=NC=N1
37 278.1247657921 C=C=[NH+]C1=C2NCN(C3CC(O)C(CO)O3)C2=NC=N1
38 162.0774216761 C=C=[NH+]C1=C2NCNC2=NC=N1
39 164.0930717401 CC=[NH+]C1=C2NCNC2=NC=N1
40 119.0702706321 OC1CCOC1C[OH2+]
41 119.0702706321 O=CCC(O)CC[OH2+] Intermediate Fragment 
42 117.0546205681 OC1CCOC1=C[OH2+]
43 99.0440558841 C=C1OCC=C1[OH2+]
44 117.0546205681 CCC(O)C(=O)C=[OH+] Intermediate Fragment 
45 99.0440558841 CC=C(O)C#C[OH2+]
46 117.0546205681 O=CCC(O)C=C[OH2+] Intermediate Fragment 
47 115.0389705041 OC1CCOC1=C=[OH+]
48 178.0723362961 OC=C=[NH+]C1=C2NCNC2=NC=N1
49 180.0879863601 OCC=[NH+]C1=C2NCNC2=NC=N1
50 123.0665226441 C1=[NH+]CC2=NCNC2=N1
51 125.0821727081 C1=[NH+]CC2NCNC2=N1
52 47.0491412641 CC[OH2+]
53 138.0774216761 [NH2+]=C1N=CN=C2NCNC12
54 152.0930717401 C[NH2+]C1=C2NCNC2=NC=N1
55 180.0879863601 N=C=NC1=C(C=[NH+]C=CO)NCN1 Intermediate Fragment 
56 163.0614372641 C=[NH+]C1=C(C=NC#CO)N=CN1
57 153.0770873281 [NH2+]=C1NCNC1=C=NC=CO
58 138.0661882961 OC#C[NH+]=CC1=CNCN1
59 136.0505382321 OC#C[NH+]=CC1=CNC=N1
60 180.0879863601 OC=C[NH+]=CN=C=NC1=CNCN1 Intermediate Fragment 
61 100.0869237401 C=[NH+]C1CNCN1
62 98.0712736761 C=[NH+]C1=CNCN1
63 96.0556236121 C=[NH+]C1=CN=CN1
64 94.0399735481 C#[N+]C1=CN=CN1
65 85.0396392001 [NH3+]C=NC=C=O
66 87.0552892641 [NH3+]C=NC=CO
67 82.0399735481 [NH2+]=C1C=NC=N1
68 99.0552892641 C=[NH+]C=NC=CO
69 110.0348881681 N=C=[NH+]C=NC#CO
70 112.0505382321 N=C=[NH+]C=NC=CO
71 109.0508725801 [NH+]#CN=C1C=NCN1
72 107.0352225161 [NH+]#CN=C1C=NC=N1
73 72.0443902321 C=[NH+]C=CO
74 178.0723362961 OC#CN=C[NH+]=C=NC1=CNCN1
75 180.0879863601 N=C=NC(=[NH+]C=CO)C1=CNCN1 Intermediate Fragment 
76 153.0770873281 [NH3+]C(=NC=C=O)C1=CNCN1
77 163.0614372641 C=[NH+]C(=NC#CO)C1=CNC=N1
78 180.0879863601 C=NC([NH+]=C=CO)=C1NCNC1=N Intermediate Fragment 
79 97.0396392001 C=[NH+]C=NC#CO
80 95.0239891361 C#[N+]C=NC#CO
81 151.0614372641 [NH2+]=C1NCNC1=C=NC#CO
82 180.0879863601 C=NC1=C(C(N)=[NH+]C#CO)NCN1 Intermediate Fragment 
83 151.0614372641 [NH3+]C(=NC=C=O)C1=CNC=N1
84 178.0723362961 C=NC1=C(C([NH3+])=NC#CO)N=CN1
85 180.0879863601 NCNC1=NC=NC([NH+]=C=CO)=C1 Intermediate Fragment 
86 138.0661882961 OCC=[NH+]C1=CC=NC=N1
87 136.0505382321 OC=C=[NH+]C1=CC=NC=N1
88 180.0879863601 NCNC1=C([NH+]=C=CO)N=CN=C1 Intermediate Fragment 
89 153.0770873281 NC1=C([NH+]=CCO)N=CN=C1
90 163.0614372641 CNC1=C([NH+]=C=C=O)N=CN=C1
91 180.0879863601 CNC1=C([NH+]=C=CO)N=CN=C1N Intermediate Fragment 
92 149.0457872001 NC1=NC=NC([NH+]=C=C=O)=C1
93 151.0614372641 NC1=NC=NC([NH+]=C=CO)=C1
94 153.0770873281 NC1=NC=NC([NH+]=CCO)=C1
95 180.0879863601 CNC1=NC=NC([NH+]=C=CO)=C1N Intermediate Fragment 
96 163.0614372641 CNC1=NC=NC([NH+]=C=C=O)=C1
97 182.1036364241 OCC[NH2+]C1=C2NCNC2=NC=N1
98 140.0930717401 [NH3+]C1N=CN=C2NCNC21
99 182.1036364241 OCC[NH+]=CN=C=NC1=CNCN1 Intermediate Fragment 
100 184.1192864881 OCC[NH2+]C1N=CN=C2NCNC21
101 166.1087218041 CC[NH2+]C1=C2NCNC2=NC=N1
102 186.1349365521 OCC[NH2+]C1N=CNC2NCNC21
103 168.1243718681 CC[NH2+]C1N=CN=C2NCNC21
104 264.1091157281 OC=C=[NH+]C1=C2NCN(C3CC(O)CO3)C2=NC=N1
105 266.1247657921 OCC=[NH+]C1=C2NCN(C3CC(O)CO3)C2=NC=N1
106 268.1404158561 OCC[NH2+]C1=C2NCN(C3CC(O)CO3)C2=NC=N1
107 278.1247657921 CC1OC(N2CNC3=C([NH+]=C=CO)N=CN=C32)CC1O
108 278.1247657921 OC=C=[NH+]C1=C2NCN(C3CCC(CO)O3)C2=NC=N1
109 236.1142011081 [NH3+]C1=C2N=CN(C3CCC(CO)O3)C2=NC=N1
110 296.1353304761 N=C=NC1=C(C=[NH+]C=CO)NCN1C1CC(O)C(CO)O1 Intermediate Fragment 
111 279.1087813801 C=[NH+]C1=C(C=NC#CO)N=CN1C1CC(O)C(CO)O1
112 251.1138667601 C=[NH+]C1=C(C=NC)N=CN1C1CC(O)C(=CO)O1
113 167.0927373921 C=[NH+]C1=C(C=NC=CO)NCN1
114 269.1244314441 [NH2+]=C1C(=C=NC=CO)NCN1C1CC(O)C(CO)O1
115 254.1135324121 OC#C[NH+]=CC1=CN(C2CC(O)C(CO)O2)CN1
116 76.0756903601 C[NH2+]CCO
117 223.0825666321 [NH+]#CN=C1C=NCN1C1CC(O)C(=CO)O1
118 225.0982166961 [NH+]#CN=C1C=NCN1C1CC(O)C(CO)O1
119 225.0982166961 [NH+]#CN=C1C=NCN1C(=O)CC(O)CCO Intermediate Fragment 
120 227.1138667601 [NH+]#CN=C1CNCN1C1CC(O)C(CO)O1
121 229.1295168241 [NH+]#CNC1CNCN1C1CC(O)C(CO)O1
122 296.1353304761 OC=C[NH+]=CN=C=NC1=CNCN1C1CC(O)C(CO)O1 Intermediate Fragment 
123 216.1342678561 C=[NH+]C1CNCN1C1CC(O)C(CO)O1
124 214.1186177921 C=[NH+]C1=CNCN1C1CC(O)C(CO)O1
125 212.1029677281 C=[NH+]C1=CN=CN1C1CC(O)C(CO)O1
126 210.0873176641 C=[NH+]C1=CN=CN1C1CC(O)C(=CO)O1
127 210.0873176641 C=[NH+]C1=CN=CN1CCC(O)C(=O)C=O Intermediate Fragment 
128 210.0873176641 C=[NH+]C1=CN=CN1C(=O)CC(O)C=CO Intermediate Fragment 
129 208.0716676001 C=[NH+]C1=CN=CN1C1CC(O)C(=C=O)O1
130 208.0716676001 C=[NH+]C1=CN=CN1CC=C(O)C(=O)C=O Intermediate Fragment 
131 206.0560175361 C=[NH+]C1=CN=CN1C1C=C(O)C(=C=O)O1
132 198.0873176641 [NH2+]=C1C=NCN1C1CC(O)C(=CO)O1
133 198.0873176641 [NH2+]=C1C=NCN1C(=O)CC(O)C=CO Intermediate Fragment 
134 183.0764186321 OC1CC(N2C=CN=C2)OC1=C[OH2+]
135 183.0764186321 O=C(C=[OH+])C(O)CCN1C=CN=C1 Intermediate Fragment 
136 183.0764186321 O=C(CC(O)C=C[OH2+])N1C=CN=C1 Intermediate Fragment 
137 181.0607685681 OC1CC(N2C=CN=C2)OC1=C=[OH+]
138 181.0607685681 O=C(C=[OH+])C(O)=CCN1C=CN=C1 Intermediate Fragment 
139 114.0661882961 N=C=[NH+]C=NCCO
140 296.1353304761 N=C=NC(=[NH+]C=CO)C1=CN(C2CC(O)C(CO)O2)CN1 Intermediate Fragment 
141 296.1353304761 C=NC1=C(C(N)=[NH+]C#CO)NCN1C1CC(O)C(CO)O1 Intermediate Fragment 
142 296.1353304761 NCN(C1=NC=NC([NH+]=C=CO)=C1)C1CC(O)C(CO)O1 Intermediate Fragment 
143 296.1353304761 OC=C=[NH+]C1=C(NCNC2CC(O)C(CO)O2)C=NC=N1 Intermediate Fragment 
144 296.1353304761 CNC1=C([NH+]=C=CO)N=CN=C1NC1CC(O)C(CO)O1 Intermediate Fragment 
145 165.0770873281 CNC1=C([NH+]=C=CO)N=CN=C1
146 296.1353304761 OC=C=[NH+]C1=C2NCN(CCC(O)C(O)CO)C2=NC=N1 Intermediate Fragment 
147 278.1247657921 O=C=C=[NH+]C1=C2NCN(CCC(O)CCO)C2=NC=N1
148 101.0597059481 CCC(O)C#C[OH2+]
149 260.1142011081 CCC(O)CCN1C=NC2=C([NH+]=C=C=O)N=CN=C21
150 240.1455012361 OCCCN1CNC2=C([NH2+]CCO)N=CN=C21
151 238.1298511721 OCC=[NH+]C1=C2NCN(CCCO)C2=NC=N1
152 236.1142011081 OC=C=[NH+]C1=C2NCN(CCCO)C2=NC=N1
153 234.0985510441 O=C=C=[NH+]C1=C2NCN(CCCO)C2=NC=N1
154 208.1192864881 CCN1CNC2=C([NH+]=CCO)N=CN=C21
155 206.1036364241 CCN1CNC2=C([NH+]=C=CO)N=CN=C21
156 204.0879863601 CCN1CNC2=C([NH+]=C=C=O)N=CN=C21
157 296.1353304761 CC(OC(CO)CO)N1CNC2=C([NH+]=C=CO)N=CN=C21 Intermediate Fragment 
158 222.0985510441 CC(O)N1CNC2=C([NH+]=C=CO)N=CN=C21
159 224.1142011081 CC(O)N1CNC2=C([NH+]=CCO)N=CN=C21
160 296.1353304761 OC=C=[NH+]C1=C2NCN(C(CCO)OCCO)C2=NC=N1 Intermediate Fragment 
161 252.1091157281 OC=C=[NH+]C1=C2NCN(C(O)CCO)C2=NC=N1
162 254.1247657921 OCC=[NH+]C1=C2NCN(C(O)CCO)C2=NC=N1
163 296.1353304761 OC=C=[NH+]C1=C2NCN(C(O)CC(O)CCO)C2=NC=N1 Intermediate Fragment 
164 226.1298511721 CC(O)N1CNC2=C([NH2+]CCO)N=CN=C21
165 208.0829009801 OC=C=[NH+]C1=C2NCN(CO)C2=NC=N1
166 206.0672509161 O=C=C=[NH+]C1=C2NCN(CO)C2=NC=N1
