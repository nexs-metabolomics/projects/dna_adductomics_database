#In-silico ESI-MS/MS [M+H]+ Spectra
#PREDICTED BY CFM-ID 4.0.8
#SMILES=O1[C@H](CO)[C@@H](C[C@@H]1n1cc(c(nc1=O)N)C)O
#InChiKey=LUCHPKXVUGJYGU-BWZBUEFSSA-N
energy0
81.04472 9.90 53 (5.7303)
83.06037 18.45 34 57 (8.9038 1.7781)
109.03964 9.98 40 42 63 (3.5762 1.646 0.55442)
126.06619 100.00 2 41 65 84 56 70 80 (44.353 6.69 2.4717 1.8453 1.33 0.92667 0.26604)
energy1
43.01784 7.34 97 (1.901)
43.02907 3.33 77 (0.86113)
45.03349 10.68 98 (2.7648)
54.03383 3.61 48 (0.93387)
56.04948 5.58 22 (1.4453)
57.03349 12.80 106 (3.3138)
69.03349 3.49 172 173 (0.56719 0.3358)
81.04472 25.51 53 (6.6049)
82.02874 5.18 61 (1.341)
83.06037 61.28 34 57 (14.047 1.8191)
87.04406 4.36 88 100 (0.97001 0.15838)
98.07127 5.06 54 (1.3108)
99.04406 33.90 102 8 93 (5.7677 2.321 0.68804)
109.03964 16.71 40 42 63 (2.932 1.2307 0.16382)
117.05462 4.26 112 94 90 103 108 111 (0.42715 0.29249 0.25061 0.098746 0.024376 0.0084729)
124.05054 4.90 55 7 79 69 (0.72997 0.37061 0.16423 0.0034309)
126.06619 100.00 2 41 70 56 65 84 80 (13.709 8.441 1.5654 1.2837 0.43305 0.3197 0.14133)
152.08184 3.56 134 (0.92102)
energy2
41.03858 3.51 71 (1.0913)
43.01784 4.06 97 (1.2609)
45.03349 8.29 98 (2.575)
54.03383 4.44 48 (1.3773)
55.01784 3.62 156 (1.1245)
56.04948 10.95 22 (3.401)
57.03349 10.20 106 (3.1669)
70.06513 2.79 28 (0.86674)
71.02399 2.52 45 (0.78154)
71.04914 10.48 174 175 (2.2553 0.99988)
81.04472 23.60 53 (7.3282)
83.06037 25.18 34 57 (5.4042 2.4143)
108.05562 4.92 39 (1.5283)
109.03964 24.68 40 42 63 (5.4463 2.0798 0.13577)
110.07127 2.80 36 (0.86823)
111.05529 4.32 136 164 118 (0.98978 0.30963 0.040418)
114.10257 3.01 17 (0.93514)
124.05054 6.95 55 7 79 69 (0.83924 0.72998 0.52678 0.061288)
126.06619 100.00 41 2 70 56 65 80 84 (15.578 5.6073 3.2753 2.8583 1.7388 1.3059 0.68591)
184.10805 3.00 189 (0.9309)

0 242.1135324121 Cc1cn(C2CC(O)C(CO)O2)c(=O)nc1[NH3+]
1 210.0873176641 CC1=CN(C2=CC(O)=CO2)C(O)N=C1[NH3+]
2 126.0661882961 CC1C=NC(=O)N=C1[NH3+]
3 212.1029677281 CC1=CN(C2=CC(O)=CO2)C(O)NC1[NH3+]
4 214.1186177921 CC1CN(C2=CC(O)=CO2)C(O)NC1[NH3+]
5 224.1029677281 C=C1OC(N2C=C(C)C([NH3+])=NC2O)C=C1O
6 128.0818383601 CC1C=NC(O)N=C1[NH3+]
7 124.0505382321 C=C1C=NC(=O)N=C1[NH3+]
8 99.0440558841 C=C1OC=CC1[OH2+]
9 101.0597059481 C=C1OCCC1[OH2+]
10 206.0924030441 C=C1C=CC(N2C=C(C)C([NH3+])=NC2=O)O1
11 224.1029677281 CC(=O)C(O)=C=CN1C=C(C)C([NH3+])=NC1O Intermediate Fragment 
12 154.0974884241 C#CN1C=C(C)C([NH3+])NC1O
13 224.1029677281 C=C1OC(N(C=CC)C(=O)N=C[NH3+])C=C1O Intermediate Fragment 
14 224.1029677281 C=C1OC(N(C=C(C)C=[NH2+])C(=N)O)C=C1O Intermediate Fragment 
15 224.1029677281 C=C1OC(=NC=C(C)C([NH3+])=NCO)C=C1O Intermediate Fragment 
16 132.1131384881 CC1CNC(O)NC1[NH3+]
17 114.1025738041 CC1C=NCNC1[NH3+]
18 115.0865893921 CC1C=NC([OH2+])NC1
19 132.1131384881 CC(C=N)C([NH3+])NCO Intermediate Fragment 
20 115.0865893921 C=C(C)C(N)=[NH+]CO
21 73.0396392001 NC=[NH+]C=O
22 56.0494756121 CCC#[NH+]
23 60.0807757401 CCC[NH3+]
24 85.0760247081 CC(C#[NH+])CN
25 89.1073248361 CC(CN)C[NH3+]
26 132.1131384881 CC(C=NC(N)O)C[NH3+] Intermediate Fragment 
27 72.0807757401 C=C(C)C[NH3+]
28 70.0651256761 C=C(C)C=[NH2+]
29 87.0916747721 CC(C=[NH2+])CN
30 132.1131384881 CCC=NC(O)NC[NH3+] Intermediate Fragment 
31 132.1131384881 C=NC(O)NC([NH3+])CC Intermediate Fragment 
32 130.0974884241 CC1C=NC(O)NC1[NH3+]
33 130.0974884241 CC(=C=N)C([NH3+])NCO Intermediate Fragment 
34 83.0603746441 CC(C#[NH+])=CN
35 130.0974884241 CC=C=NC(O)NC[NH3+] Intermediate Fragment 
36 110.0712736761 CC1=C([NH3+])N=CN=C1
37 112.0505382321 [NH3+]C1=CC=NC(O)=N1
38 128.0818383601 CC(=C=N)C([NH3+])=NCO Intermediate Fragment 
39 108.0556236121 [CH2+]C1=C(N)N=CN=C1
40 109.0396392001 [CH2+]C1=CN=C(O)N=C1
41 126.0661882961 CC(=C=N)C([NH3+])=NC=O Intermediate Fragment 
42 109.0396392001 [CH+]=C(C)C(=N)N=C=O
43 99.0552892641 CC=C(N)[NH+]=C=O
44 75.0552892641 NC=[NH+]CO
45 71.0239891361 N=C=[NH+]C=O
46 69.0083390721 N=C=[N+]=C=O
47 52.0181754841 C#CC#[NH+]
48 54.0338255481 C=CC#[NH+]
49 58.0651256761 CCC=[NH2+]
50 46.0287401681 [NH2+]=CO
51 44.0130901041 [NH+]#CO
52 79.0290745161 [CH2+]C(=C=N)C#N
53 81.0447245801 CC(=C=N)C#[NH+]
54 98.0712736761 CC(C#[NH+])C(=N)N
55 124.0505382321 CC(C#[NH+])=C(N)N=C=O
56 126.0661882961 CC(=C=NC=O)C(=N)[NH3+] Intermediate Fragment 
57 83.0603746441 [CH+]=C(C)C(=N)N
58 71.0603746441 C=CC(N)=[NH2+]
59 69.0447245801 C#CC(N)=[NH2+]
60 56.0130901041 C#[N+]C=O
61 82.0287401681 C=C=C=[NH+]C=O
62 84.0443902321 CC=C=[NH+]C=O
63 109.0396392001 CC(=C=[NH+]C=O)C#N
64 96.0556236121 C=C(C#[NH+])C(=N)N
65 126.0661882961 CC(=C=NC(=N)O)C=[NH2+] Intermediate Fragment 
66 66.0338255481 [CH+]=C(C)C#N
67 97.0396392001 C=C=C=[NH+]C(=N)O
68 99.0552892641 CC=C=[NH+]C(=N)O
69 124.0505382321 CC(=C=[NH+]C(=N)O)C#N
70 126.0661882961 CC=C=NC(=O)N=C[NH3+] Intermediate Fragment 
71 41.0385765801 [CH2+]#CC
72 39.0229265161 [C+]#CC
73 86.0348881681 N=C=NC([NH3+])=O
74 98.0348881681 C=[NH+]C(=O)N=C=N
75 100.0505382321 C=[NH+]C(=O)N=CN
76 80.0130901041 C=C=C=NC#[O+]
77 43.0290745161 [NH+]#CN
78 45.0447245801 NC=[NH2+]
79 124.0505382321 CC=C=[NH+]C(=O)N=C=N
80 126.0661882961 C=C=C([NH3+])N=C(O)N=C Intermediate Fragment 
81 58.0287401681 C=[NH+]C=O
82 95.0239891361 [CH+]=C=C(N)N=C=O
83 97.0396392001 C=C=C(N)[NH+]=C=O
84 126.0661882961 C=C([CH2+])C(N)=NC(N)=O Intermediate Fragment 
85 111.0076703761 O=C1C=COC1=C=[OH+]
86 113.0233204401 OC1C=COC1=C=[OH+]
87 115.0389705041 OC1CCOC1=C=[OH+]
88 87.0440558841 [OH2+]C1=COCC1
89 115.0389705041 CC=C(O)C(=O)C=[OH+] Intermediate Fragment 
90 117.0546205681 OC1CCOC1=C[OH2+]
91 85.0284058201 [OH2+]C1=COC=C1
92 89.0597059481 [OH2+]C1CCOC1
93 99.0440558841 [OH+]=C=C1CCCO1
94 117.0546205681 OCCC(O)C#C[OH2+] Intermediate Fragment 
95 73.0284058201 OC#CC[OH2+]
96 71.0127557561 OC#CC=[OH+]
97 43.0178411361 C#C[OH2+]
98 45.0334912001 C=C[OH2+]
99 47.0491412641 CC[OH2+]
100 87.0440558841 CC(O)C#C[OH2+]
101 89.0597059481 CC(O)C=C[OH2+]
102 99.0440558841 CC=C(O)C#C[OH2+]
103 117.0546205681 OCCCOC#C[OH2+] Intermediate Fragment 
104 75.0440558841 OC=CC[OH2+]
105 61.0284058201 OCC=[OH+]
106 57.0334912001 CC#C[OH2+]
107 59.0491412641 CC=C[OH2+]
108 117.0546205681 CCOC(=C=[OH+])CO Intermediate Fragment 
109 89.0597059481 CC[OH+]C=CO
110 91.0389705041 OCC(=[OH+])CO
111 117.0546205681 COC(=C=[OH+])C(C)O Intermediate Fragment 
112 117.0546205681 CCC(O)C(=O)C=[OH+] Intermediate Fragment 
113 61.0647913281 CCC[OH2+]
114 59.0127557561 OC=C=[OH+]
115 31.0542266441 C[CH4+]
116 29.0385765801 C=[CH3+]
117 225.0869833161 CC1=CN(C2C=C(O)C(=C=[OH+])O2)C(O)NC1
118 111.0552892641 CC1C=NC(=[OH+])N=C1
119 225.0869833161 CC1=CN(C=C=C(O)C(=O)C=[OH+])C(O)NC1 Intermediate Fragment 
120 226.0822322841 [NH3+]C1C=CN(C2C=C(O)C(=C=O)O2)C(O)N1
121 224.1029677281 CC1=CN(C2C=CC(=C=O)O2)C(O)NC1[NH3+]
122 224.1029677281 CC1=CN(C=C=CC(=O)C=O)C(O)NC1[NH3+] Intermediate Fragment 
123 242.1135324121 CC1CN(C(O)=C=C(O)C#CO)C(O)NC1[NH3+] Intermediate Fragment 
124 200.1029677281 CC1=CN(C(O)C#CO)C(O)NC1[NH3+]
125 198.0873176641 CC1=CN(C(O)C#CO)C(O)N=C1[NH3+]
126 180.0767529801 C#CC(O)N1C=C(C)C([NH3+])=NC1=O
127 196.0716676001 CC1=CN(C(O)C#CO)C(=O)N=C1[NH3+]
128 168.0767529801 C=C(O)N1C=C(C)C([NH3+])=NC1=O
129 178.0611029161 C#CC(=O)N1C=C(C)C([NH3+])=NC1=O
130 174.1237031721 C=C(O)N1CC(C)C([NH3+])NC1O
131 174.1237031721 C=C(O)N=CC(C)C([NH3+])NCO Intermediate Fragment 
132 172.1080531081 C=C(O)N1C=C(C)C([NH3+])NC1O
133 170.0924030441 C=C(O)N1C=C(C)C([NH3+])=NC1O
134 152.0818383601 C#CN1C=C(C)C([NH3+])=NC1O
135 170.0924030441 C=C(O)N=C=C(C)C([NH3+])=NCO Intermediate Fragment 
136 111.0552892641 C=C(C)C(=N)[NH+]=C=O
137 125.0709393281 C=C(O)[NH+]=C=C(C)C=N
138 140.0818383601 C=C(O)[NH+]=C=C(C)C(=N)N
139 170.0924030441 C=C(O)N(C=O)C=C(C)C(=N)[NH3+] Intermediate Fragment 
140 170.0924030441 C=C(O)N(C=C(C)C=[NH2+])C(=N)O Intermediate Fragment 
141 68.0494756121 C=C(C)C#[NH+]
142 153.0658539481 C=C(O)[NH+](C=O)C=C(C)C#N
143 127.0865893921 C=C(O)[NH+]=C=C(C)CN
144 170.0924030441 C=C(O)N(C=CC)C(=O)N=C[NH3+] Intermediate Fragment 
145 170.0924030441 C=C=C([NH3+])N=C(O)N(C)C(=C)O Intermediate Fragment 
146 170.0924030441 C=C(O)NC(=O)N=C([NH3+])C(=C)C Intermediate Fragment 
147 150.0661882961 C#CN1C=C(C)C([NH3+])=NC1=O
148 168.0767529801 C=C(O)N=C=C(C)C([NH3+])=NC=O Intermediate Fragment 
149 168.0767529801 C=C(O)N(C#[O+])C=C(C)C(=N)N Intermediate Fragment 
150 168.0767529801 C=C(O)N(C=C(C)C#[NH+])C(=N)O Intermediate Fragment 
151 168.0767529801 C=C(O)N(C=CC)C(=O)N=C=[NH2+] Intermediate Fragment 
152 166.0611029161 [CH+]=C(O)N1C=C(C)C(N)=NC1=O
153 166.0611029161 C=C(O)N=C=C(C)C(N)=NC#[O+] Intermediate Fragment 
154 166.0611029161 C=C=CN(C(=C)O)C(=O)N=C=[NH2+] Intermediate Fragment 
155 68.9971056921 [O+]#CC#CO
156 55.0178411361 C#CC=[OH+]
157 158.0924030441 CC1=CN(CO)C(O)N=C1[NH3+]
158 156.0767529801 CC1=CN(CO)C(=O)N=C1[NH3+]
159 138.0661882961 [CH2+]N1C=C(C)C(N)=NC1=O
160 156.0767529801 CC(=CN=CO)C([NH3+])=NC=O Intermediate Fragment 
161 154.0611029161 CC1=CN(C=O)C(=O)N=C1[NH3+]
162 154.0611029161 CC(=CN=C=O)C([NH3+])=NC=O Intermediate Fragment 
163 86.0600402961 CC=C[NH+]=CO
164 111.0552892641 CC(C#N)=C[NH+]=CO
165 154.0611029161 CC(=CN(C#[O+])C=O)C(=N)N Intermediate Fragment 
166 137.0345538201 CC(=C=[N+](C=O)C=O)C#N
167 154.0611029161 C=C=CN(CO)C(=O)N=C=[NH2+] Intermediate Fragment 
168 152.0454528521 CC1=CN(C#[O+])C(=O)N=C1N
169 152.0454528521 CC(=CN=C=O)C(N)=NC#[O+] Intermediate Fragment 
170 152.0454528521 C=C=CN(C=O)C(=O)N=C=[NH2+] Intermediate Fragment 
171 59.0491412641 C=C(C)[OH2+]
172 69.0334912001 C#CC(=C)[OH2+]
173 69.0334912001 C=CC#C[OH2+]
174 71.0491412641 C#CC(C)[OH2+]
175 71.0491412641 CCC#C[OH2+]
176 91.0753560121 CC(O)CC[OH2+]
177 73.0647913281 C=CC(C)[OH2+]
178 73.0647913281 CCC=C[OH2+]
179 224.1029677281 CC1=CN(C=C=C(O)C#CO)C(O)NC1[NH3+]
180 206.0924030441 C#CC(O)=C=CN1C=C(C)C([NH3+])=NC1O
181 180.0767529801 CC1=CN(CC#CO)C(=O)N=C1[NH3+]
182 178.0611029161 CC1=CN([CH+]C#CO)C(=O)N=C1N
183 224.1029677281 CC(=CN=C=C=C(O)C#CO)C([NH3+])NCO Intermediate Fragment 
184 240.0978823481 CC1=CN(C(O)=C=C(O)C#CO)C(O)NC1[NH3+]
185 242.1135324121 CC1CN(C(=C=CO)OC#CO)C(O)NC1[NH3+] Intermediate Fragment 
186 212.1029677281 C=C(OC#CO)N1C=C(C)C([NH3+])NC1O
187 63.0440558841 OCC[OH2+]
188 182.0924030441 CC1=CN(CC#CO)C(O)N=C1[NH3+]
189 184.1080531081 CC1=CN(CC#CO)C(O)NC1[NH3+]
190 242.1135324121 C=C(OC(=C=O)CO)N1C=C(C)C([NH3+])NC1O Intermediate Fragment 
191 150.0661882961 C#CN=C=C(C)C([NH3+])=NC=O Intermediate Fragment 
192 156.1131384881 C#CN1CC(C)C([NH3+])NC1O
193 158.1287885521 C=CN1CC(C)C([NH3+])NC1O
194 242.1135324121 C=C(O)C(=C=O)OCN1C=C(C)C([NH3+])NC1O Intermediate Fragment 
195 242.1135324121 CC1CN(C=C=C(O)C(=O)C=O)C(O)NC1[NH3+] Intermediate Fragment 
196 186.1237031721 CC1CN(CC#CO)C(O)NC1[NH3+]
197 89.0233204401 O=CC(=[OH+])CO
198 71.0127557561 CC(=O)C#[O+]
199 73.0284058201 CC(=[OH+])C=O
200 93.0546205681 OCC([OH2+])CO
201 75.0440558841 CC(=[OH+])CO
202 140.0818383601 CC1=CN(C)C(=O)N=C1[NH3+]
203 140.0818383601 CC(=CN(C)C#[O+])C(=N)N Intermediate Fragment 
204 140.0818383601 CC(C#[NH+])=CN(C)C(=N)O Intermediate Fragment 
205 240.0978823481 CC1=CN(C=C=C(O)C(=O)C=O)C(O)NC1[NH3+]
206 242.1135324121 CC=C([NH3+])NC(O)N(C)C1C=C(O)C(=C=O)O1 Intermediate Fragment 
207 242.1135324121 CC=CN(C(O)NC[NH3+])C1C=C(O)C(=C=O)O1 Intermediate Fragment 
208 215.1026333801 CCC[NH+](C(N)O)C1C=C(O)C(=C=O)O1
209 174.1124697921 CCC[NH2+]C1CC(O)C(=CO)O1
210 172.0968197281 CCC[NH2+]C1CC(O)C(=C=O)O1
211 172.0968197281 CCC[NH2+]C(O)C=C(O)C#CO Intermediate Fragment 
212 170.0811696641 CCC[NH2+]C1C=C(O)C(=C=O)O1
213 242.1135324121 CC(=CN(C(N)O)C1C=C(O)C(=C=O)O1)C[NH3+] Intermediate Fragment 
214 197.0920686961 CC(CN)C[NH+]=C1C=C(O)C(=C=O)O1
215 242.1135324121 CC(=CN(CO)C1C=C(O)C(=C=O)O1)C(N)[NH3+] Intermediate Fragment 
216 225.0869833161 CC(=C[NH+](C=O)C1C=C(O)C(=C=O)O1)CN
217 214.1186177921 CC(C[NH2+]C1C=C(O)C(=C=O)O1)C(N)N
218 212.1029677281 CC(C[NH+]=C1C=C(O)C(=C=O)O1)C(N)N
219 242.1135324121 CC(CN=C1C=C(O)C(=C=O)O1)C([NH3+])NCO Intermediate Fragment 
220 117.1022394561 C=C(C)C(N)[NH2+]CO
221 113.0709393281 C=C(C)C(N)[NH+]=C=O
222 95.0603746441 C#[N+]C(=N)C(=C)C
223 199.1077187601 CC(CN)C[NH2+]C1C=C(O)C(=C=O)O1
224 242.1135324121 CC(C)C([NH3+])NC(O)N=C1C=C(O)C(=C=O)O1 Intermediate Fragment 
