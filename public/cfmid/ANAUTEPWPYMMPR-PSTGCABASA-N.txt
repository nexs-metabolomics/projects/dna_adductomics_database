#In-silico ESI-MS/MS [M+H]+ Spectra
#PREDICTED BY CFM-ID 4.0.8
#SMILES=[C@H]1(C[C@@H](O[C@@H]1CO)n1c(cc(nc1=O)N)Nc1nc2c(cc1)nc1n2cccc1)O
#InChiKey=ANAUTEPWPYMMPR-PSTGCABASA-N
energy0
294.10978 100.00 6 35 41 57 26 61 46 64 52 58 55 51 53 59 (57.937 2.078 1.7195 0.10689 0.044423 0.025036 0.017788 0.017077 0.0058889 0.0024525 0.0020085 0.0010995 0.00041097 0.00033939)
410.15713 48.01 0 178 222 102 246 98 226 252 240 262 87 223 250 242 268 249 245 263 243 264 248 267 (28.932 0.50676 0.081091 0.05959 0.051225 0.031627 0.017988 0.014094 0.01095 0.0085917 0.0076585 0.0070299 0.0050639 0.0039634 0.002677 0.0014062 0.0014058 0.00028782 0.00025471 3.8792e-05 2.635e-05 7.035e-06)
energy1
249.08832 3.92 31 37 (1.9414 0.76267)
251.10397 10.90 32 36 (4.28 3.2322)
277.08324 3.97 40 27 56 (1.9735 0.66911 0.090852)
294.10978 100.00 6 35 26 41 46 57 64 61 52 55 51 58 59 53 (59.252 3.887 3.7929 1.0712 0.20911 0.20784 0.15087 0.11539 0.091948 0.091388 0.029571 0.020289 0.015704 0.0014126)
energy2
45.03349 7.74 75 (1.7129)
55.01784 4.57 182 (1.0122)
57.03349 22.41 73 (4.9611)
61.02841 4.31 118 (0.95479)
71.04914 4.97 212 213 (0.70357 0.39606)
73.02841 3.00 121 120 (0.61029 0.054359)
185.08217 15.60 22 81 (2.9521 0.50128)
207.06652 4.19 48 (0.92726)
236.09307 5.46 134 (1.208)
243.09889 3.98 261 (0.88083)
251.10397 2.95 32 36 (0.34584 0.3071)
279.09889 4.67 188 129 79 (0.56105 0.44039 0.03169)
281.11454 3.45 244 237 (0.44283 0.3201)
294.10978 25.84 26 6 41 35 51 46 52 57 61 59 55 53 58 64 (2.1086 1.8642 0.41176 0.39514 0.2897 0.25195 0.21914 0.051808 0.040623 0.027127 0.025006 0.02319 0.0099531 0.0039592)
296.12543 4.40 5 (0.97383)
305.07815 2.91 184 195 (0.53936 0.10441)
306.10978 12.66 128 100 133 136 135 137 (0.96393 0.66001 0.49637 0.44499 0.11885 0.11804)
318.10978 14.60 88 (3.2337)
320.08905 6.06 205 209 (1.0307 0.31163)
320.12543 100.00 89 90 91 92 (7.3794 5.667 4.8212 4.2738)
322.10470 52.27 191 101 186 202 199 190 196 198 200 206 207 208 201 (3.0602 2.7551 1.9563 1.2793 0.85609 0.49806 0.42605 0.18335 0.12448 0.11441 0.10972 0.10524 0.10453)
322.14108 6.83 12 159 (1.333 0.1793)
330.10978 4.27 170 168 (0.73488 0.2103)
332.12543 8.53 156 (1.8894)
334.14108 8.40 151 (1.86)
336.15673 3.88 148 (0.85924)
340.14042 6.05 225 (1.3393)
350.13600 4.56 217 15 167 161 157 165 163 160 162 (0.45747 0.11523 0.1066 0.089479 0.061676 0.061147 0.058107 0.039847 0.019312)
365.13566 3.03 239 18 (0.63433 0.036612)
393.13058 3.56 251 247 80 236 78 (0.48445 0.29713 0.0045874 0.00095803 0.00036564)

0 410.1571285401 [NH3+]c1cc(Nc2ccc3nc4ccccn4c3n2)n(C2CC(O)C(CO)O2)c(=O)n1
1 378.1309137921 [NH3+]C1=NC(=O)N(C2C=C(O)CO2)C(N=C2CC=C3N=C4C=CC=CN4C3=N2)=C1
2 380.1465638561 [NH3+]C1=NC(=O)N(C2C=C(O)CO2)C(N=C2CC=C3N=C4CCC=CN4C3=N2)=C1
3 382.1622139201 [NH3+]C1=NC(=O)N(C2C=C(O)CO2)C(N=C2CC=C3N=C4CCCCN4C3=N2)=C1
4 392.1465638561 C=C1OC(N2C(=O)N=C([NH3+])C=C2N=C2CC=C3N=C4CCC=CN4C3=N2)C=C1O
5 296.1254344881 [NH3+]C1=CC(N=C2CC=C3N=C4CCC=CN4C3=N2)=NC(O)=N1
6 294.1097844241 [NH3+]C1=CC(N=C2CC=C3N=C4C=CC=CN4C3=N2)=NC(O)=N1
7 99.0440558841 C=C1OC=CC1[OH2+]
8 374.1359991721 [CH+]=C1OC(N2CN=C(N)C=C2N=C2CC=C3N=C4C=CC=CN4C3=N2)C=C1O
9 375.1200147601 C=C1OC(N2C(=O)N=CC=C2[NH+]=C2CC=C3N=C4C=CC=CN4C3=N2)C=C1O
10 374.1359991721 C=C1C=CC(N2C(=O)N=C([NH3+])C=C2N=C2CC=C3N=C4C=CC=CN4C3=N2)O1
11 392.1465638561 CC(=O)C(O)=C=CN1C(=O)N=C([NH3+])C=C1N=C1CC=C2N=C3CCC=CN3C2=N1 Intermediate Fragment 
12 322.1410845521 C#CN1C(=O)N=C([NH3+])C=C1N=C1CC=C2N=C3CCCCN3C2=N1
13 392.1465638561 C#CC(O)=C=C(O)N1C(=O)N=C([NH3+])C=C1N=C1CC=C2N=C3CCCCN3C2=N1 Intermediate Fragment 
14 392.1465638561 C#COC(=C=CO)N1C(=O)N=C([NH3+])C=C1N=C1CC=C2N=C3CCCCN3C2=N1 Intermediate Fragment 
15 350.1359991721 [NH3+]C1=NC(=O)N(CC#CO)C(N=C2CC=C3N=C4CCC=CN4C3=N2)=C1
16 366.1309137921 [NH3+]C1=NC(=O)N(C(=O)C#CO)C(N=C2CC=C3N=C4CCCCN4C3=N2)=C1
17 392.1465638561 C=C1OC(N(C(=N)O)C(=C=C=[NH2+])N=C2CC=C3N=C4CCC=CN4C3=N2)C=C1O Intermediate Fragment 
18 365.1356648241 C=C1OC([NH+](C(=C)N=C2CC=C3N=C4C=CC=CN4C3=N2)C(=N)O)=CC1O
19 392.1465638561 C=C1OC(=NC(=C=C([NH3+])N=C=O)N=C2CC=C3N=C4CCCCN4C3=N2)C=C1O Intermediate Fragment 
20 392.1465638561 C=C1OC(N2C(=O)N=C([NH3+])C=C2N=C2CC=C(N=C3CC=CC=N3)C=N2)C=C1O Intermediate Fragment 
21 300.1567346161 [NH3+]C1=CC(N=C2CC=C3NC4CCCCN4C3=N2)=NC(O)=N1
22 185.0821727081 [NH2+]=C1CC=C2N=C3C=CC=CN3C2=N1
23 300.1567346161 N=C(O)N=C(C=C=[NH2+])N=C1CC=C2NC3CCCCN3C2=N1 Intermediate Fragment 
24 283.1301855201 NC#CC(N=C1CC=C2N=C3CCCCN3C2=N1)=[NH+]C=O
25 298.1410845521 [NH3+]C1=CC(N=C2CC=C3N=C4CCCCN4C3=N2)=NC(O)=N1
26 294.1097844241 N=C(C=C([NH3+])N=C=O)N=C1CC=C2N=C3C=CC=CN3C2=N1 Intermediate Fragment 
27 277.0832353281 NC(C#CN=C1CC=C2N=C3C=CC=CN3C2=N1)=[N+]=C=O
28 210.0774216761 [NH+]#CN=C1CC=C2N=C3C=CC=CN3C2=N1
29 224.0930717401 [CH2+]C(=N)N=C1CC=C2N=C3C=CC=CN3C2=N1
30 226.1087218041 CC(=[NH2+])N=C1CC=C2N=C3C=CC=CN3C2=N1
31 249.0883207081 N=C=[C+]C(=N)N=C1CC=C2N=C3C=CC=CN3C2=N1
32 251.1039707721 N=C=CC(=[NH2+])N=C1CC=C2N=C3C=CC=CN3C2=N1
33 266.1148698041 N=C([C+]=C(N)N)N=C1CC=C2N=C3C=CC=CN3C2=N1
34 292.0941343601 N=C([C+]=C(N)N=C=O)N=C1CC=C2N=C3C=CC=CN3C2=N1
35 294.1097844241 NC(N)=CC(=NC#[O+])N=C1CC=C2N=C3C=CC=CN3C2=N1 Intermediate Fragment 
36 251.1039707721 NC(=[NH2+])C=C=NC1=NC2=C(C=C1)N=C1C=CC=CN12
37 249.0883207081 N=C(N)[C+]=C=NC1=NC2=C(C=C1)N=C1C=CC=CN12
38 236.0566862321 [O+]#CN=C=NC1=NC2=C(C=C1)N=C1C=CC=CN12
39 252.0879863601 C=C(N=C1CC=C2N=C3C=CC=CN3C2=N1)[NH+]=C=O
40 277.0832353281 NC#CC(=NC#[O+])N=C1CC=C2N=C3C=CC=CN3C2=N1
41 294.1097844241 N=C(O)N=C(C=C=[NH2+])N=C1CC=C2N=C3C=CC=CN3C2=N1 Intermediate Fragment 
42 234.0774216761 [NH+]#CC=C=NC1=NC2=C(C=C1)N=C1C=CC=CN12
43 253.0832353281 N=C(O)[NH+]=C=NC1=NC2=C(C=C1)N=C1C=CC=CN12
44 267.0988853921 [CH2+]C(=NC(=N)O)N=C1CC=C2N=C3C=CC=CN3C2=N1
45 292.0941343601 N=C=[C+]C(=NC(=N)O)N=C1CC=C2N=C3C=CC=CN3C2=N1
46 294.1097844241 CC(=NC(=O)N=C=[NH2+])N=C1CC=C2N=C3C=CC=CN3C2=N1 Intermediate Fragment 
47 209.0821727081 C#C[NH+]=C1CC=C2N=C3C=CC=CN3C2=N1
48 207.0665226441 [C+]#CN=C1CC=C2N=C3C=CC=CN3C2=N1
49 250.0723362961 [CH+]=C(N=C=O)N=C1CC=C2N=C3C=CC=CN3C2=N1
50 292.0941343601 [CH2+]C(=NC(=O)N=C=N)N=C1CC=C2N=C3C=CC=CN3C2=N1
51 294.1097844241 C=C([NH3+])N=C(O)N=C=NC1=NC2=C(C=C1)N=C1C=CC=CN12 Intermediate Fragment 
52 294.1097844241 NC(O)=NC([NH3+])=C=C=NC1=NC2=C(C=C1)N=C1C=CC=CN12 Intermediate Fragment 
53 294.1097844241 C=CC1=C(N=C=NC2=NC(O)=NC([NH3+])=C2)N2C=CC=CC2=N1 Intermediate Fragment 
54 152.0566862321 N=C=NC1=NC(O)=NC([NH3+])=C1
55 294.1097844241 N=C1C(=C=CC=NC2=NC(O)=NC([NH3+])=C2)N=C2C=CC=CN12 Intermediate Fragment 
56 277.0832353281 NC1=CC([N+]#CC#CC2=CN3C=CC=CC3=N2)=NC(O)=N1
57 294.1097844241 [NH3+]C1=CC(N=C2C=CC(=NC3=NC=CC=C3)C=N2)=NC(O)=N1 Intermediate Fragment 
58 294.1097844241 C=C=C=C1N=C2C=CC(=NC3=NC(O)=NC([NH3+])=C3)N=C2N1C Intermediate Fragment 
59 294.1097844241 C=C=C1N=C2C=CC(=NC3=NC(O)=NC([NH3+])=C3)N=C2N1C=C Intermediate Fragment 
60 268.0941343601 C=CC1=NC2=NC(=NC3=NC(O)=NC([NH3+])=C3)C=CC2=N1
61 294.1097844241 C=C=C=CN1C=NC2=CCC(=NC3=NC(O)=NC([NH3+])=C3)N=C21 Intermediate Fragment 
62 242.0784842961 [NH3+]C1=CC(N=C2C=CC3=NC=NC3=N2)=NC(O)=N1
63 244.0941343601 [NH3+]C1=CC(N=C2C=CC3=NCNC3=N2)=NC(O)=N1
64 294.1097844241 C=C=C=CC1=NC2=CCC(=NC3=NC(O)=NC([NH3+])=C3)N=C2N1 Intermediate Fragment 
65 115.0389705041 OC1C=COC1=C[OH2+]
66 87.0440558841 [OH2+]C1C=COC1
67 115.0389705041 O=C=C=C(O)CC[OH2+] Intermediate Fragment 
68 117.0546205681 OC1C=COC1C[OH2+]
69 99.0440558841 [OH2+]C=C1C=CCO1
70 117.0546205681 C=COC(=C[OH2+])CO Intermediate Fragment 
71 117.0546205681 C=C=C(O)C(O)C[OH2+] Intermediate Fragment 
72 99.0440558841 C=C=C(O)C=C[OH2+]
73 57.0334912001 CC#C[OH2+]
74 117.0546205681 OC=C=C(O)CC[OH2+] Intermediate Fragment 
75 45.0334912001 C=C[OH2+]
76 117.0546205681 OC=C=COCC[OH2+] Intermediate Fragment 
77 392.1465638561 [NH3+]C1=NCN(C2C=C(O)C(=C=O)O2)C(N=C2CC=C3N=C4CCC=CN4C3=N2)=C1
78 393.1305794441 O=C=C1OC(N2C(=O)N=CC=C2[NH+]=C2CC=C3N=C4CCCCN4C3=N2)C=C1O
79 279.0988853921 OC1=NC=CC([NH+]=C2CC=C3N=C4C=CC=CN4C3=N2)=N1
80 393.1305794441 O=CC(=O)C(O)=C=CN1C(=O)N=CC=C1[NH+]=C1CC=C2N=C3CCCCN3C2=N1 Intermediate Fragment 
81 185.0821727081 N=C1C(=C=CC#[NH+])N=C2CCC=CN12 Intermediate Fragment 
82 226.0822322841 [NH3+]C1C=CN(C2=CC(O)C(=C=O)O2)C(O)N1
83 392.1465638561 [NH3+]C1=NC(=O)N(C2C=CC(=C=O)O2)C(N=C2CC=C3N=C4CCCCN4C3=N2)=C1
84 360.1203491081 [NH3+]C1=NC(=O)N(C2=CC=CO2)C(N=C2CC=C3N=C4C=CC=CN4C3=N2)=C1
85 362.1359991721 [NH3+]C1=NC(=O)N(C2=CCCO2)C(N=C2CC=C3N=C4C=CC=CN4C3=N2)=C1
86 364.1516492361 [NH3+]C1=NC(=O)N(C2=CCCO2)C(N=C2CC=C3N=C4CCC=CN4C3=N2)=C1
87 410.1571285401 C=C(OC(=C=O)CO)N1C(=O)N=C([NH3+])C=C1N=C1CC=C2N=C3CCCCN3C2=N1 Intermediate Fragment 
88 318.1097844241 C#CN1C(=O)N=C([NH3+])C=C1N=C1CC=C2N=C3C=CC=CN3C2=N1
89 320.1254344881 C#CN1C(=O)N=C([NH3+])C=C1N=C1CC=C2N=C3CCC=CN3C2=N1
90 320.1254344881 C#CN(C(=N)O)C(=C=C=[NH2+])N=C1CC=C2N=C3CCC=CN3C2=N1 Intermediate Fragment 
91 320.1254344881 C#CNC(O)=NC([NH3+])=C=C=NC1=NC2=C(C=C1)N=C1CCC=CN12 Intermediate Fragment 
92 320.1254344881 C#CN1C(=O)N=C([NH3+])C=C1N=C1CC=C(N=C2CC=CC=N2)C=N1 Intermediate Fragment 
93 75.0440558841 OCC=C[OH2+]
94 334.1046990441 [CH+]=C(O)N1C(=O)N=C(N)C=C1N=C1CC=C2N=C3C=CC=CN3C2=N1
95 336.1203491081 C=C(O)N1C(=O)N=C([NH3+])C=C1N=C1CC=C2N=C3C=CC=CN3C2=N1
96 338.1359991721 C=C(O)N1C(=O)N=C([NH3+])C=C1N=C1CC=C2N=C3CCC=CN3C2=N1
97 380.1465638561 C=C(OC#CO)N1C(=O)N=C([NH3+])C=C1N=C1CC=C2N=C3CCCCN3C2=N1
98 410.1571285401 C=C(O)C(=C=O)OCN1C(=O)N=C([NH3+])C=C1N=C1CC=C2N=C3CCCCN3C2=N1 Intermediate Fragment 
99 324.1203491081 [NH3+]C1=NC(=O)N(C=O)C(N=C2CC=C3N=C4CCC=CN4C3=N2)=C1
100 306.1097844241 [CH2+]N1C(=O)N=C(N)C=C1N=C1CC=C2N=C3C=CC=CN3C2=N1
101 322.1046990441 [NH3+]C1=NC(=O)N(C=O)C(N=C2CC=C3N=C4C=CC=CN4C3=N2)=C1
102 410.1571285401 [NH3+]C1=NC(=O)N(C=C=C(O)C(=O)C=O)C(N=C2CC=C3NC4CCCCN4C3=N2)=C1 Intermediate Fragment 
103 326.1723846801 C#CN1C(=O)N=C([NH3+])C=C1N=C1CCC2NC3CCCCN3C2=N1
104 308.1618199961 C#CN1CN=C([NH3+])C=C1N=C1CC=C2N=C3CCCCN3C2=N1
105 326.1723846801 C#CN(C(=N)O)C(=C=C=[NH2+])N=C1CCC2NC3CCCCN3C2=N1 Intermediate Fragment 
106 326.1723846801 C#CN(C(=C)N=C1CCC2NC3CCCCN3C2=N1)C(=O)N=C=[NH2+] Intermediate Fragment 
107 324.1567346161 C#CN1C(=O)N=C([NH3+])C=C1N=C1CC=C2NC3CCCCN3C2=N1
108 306.1461699321 C#CN1CN=C([NH3+])C=C1N=C1CC=C2N=C3CCC=CN3C2=N1
109 324.1567346161 C#CN(C(=N)O)C(=C=C=[NH2+])N=C1CC=C2NC3CCCCN3C2=N1 Intermediate Fragment 
110 84.9920203121 O=CC(=O)C#[O+]
111 56.9971056921 O=C=C=[OH+]
112 87.0076703761 O=CC(=[OH+])C=O
113 68.9971056921 [O+]#CC#CO
114 59.0127557561 O=C=C[OH2+]
115 89.0233204401 O=CC(=[OH+])CO
116 71.0127557561 O=CC#C[OH2+]
117 71.0127557561 CC(=O)C#[O+]
118 61.0284058201 OCC=[OH+]
119 91.0389705041 OCC(=[OH+])CO
120 73.0284058201 OCC#C[OH2+]
121 73.0284058201 CC(=[OH+])C=O
122 93.0546205681 OCC([OH2+])CO
123 75.0440558841 CC(=[OH+])CO
124 312.1567346161 CN1C(=O)N=C([NH3+])C=C1N=C1CC=C2N=C3CCCCN3C2=N1
125 310.1410845521 CN1C(=O)N=C([NH3+])C=C1N=C1CC=C2N=C3CCC=CN3C2=N1
126 308.1254344881 CN1C(=O)N=C([NH3+])C=C1N=C1CC=C2N=C3C=CC=CN3C2=N1
127 308.1254344881 C=NC(=C=C([NH3+])N=C=O)N=C1CC=C2N=C3CCC=CN3C2=N1 Intermediate Fragment 
128 306.1097844241 C=NC(=C=C([NH3+])N=C=O)N=C1CC=C2N=C3C=CC=CN3C2=N1 Intermediate Fragment 
129 279.0988853921 N=C(C=C=NC1=NC2=C(C=C1)N=C1CCC=CN12)[NH+]=C=O
130 261.0883207081 C#[N+]C(=C=C=N)N=C1CC=C2N=C3C=CC=CN3C2=N1
131 263.1039707721 C=[NH+]C(=C=C=N)N=C1CC=C2N=C3C=CC=CN3C2=N1
132 278.1148698041 C#[N+]C(=C=C(N)N)N=C1CC=C2N=C3C=CC=CN3C2=N1
133 306.1097844241 [CH2+]N(C(=N)O)C(=C=C=N)N=C1CC=C2N=C3C=CC=CN3C2=N1 Intermediate Fragment 
134 236.0930717401 [NH+]#CC=C=NC1=NC2=C(C=C1)N=C1CCC=CN12
135 306.1097844241 C=C(N=C1CC=C2N=C3C=CC=CN3C2=N1)N([CH2+])C(=O)N=C=N Intermediate Fragment 
136 306.1097844241 C=NC(O)=NC([NH3+])=C=C=NC1=NC2=C(C=C1)N=C1C=CC=CN12 Intermediate Fragment 
137 306.1097844241 [CH2+]N1C(=O)N=C(N)C=C1N=C1C=CC(=NC2=NC=CC=C2)C=N1 Intermediate Fragment 
138 103.0389705041 C=C(O)C(=[OH+])CO
139 392.1465638561 [NH3+]C1=NC(=O)N(C=C=C(O)C#CO)C(N=C2CC=C3N=C4CCCCN4C3=N2)=C1
140 364.1516492361 CC(O)=C=CN1C(=O)N=C([NH3+])C=C1N=C1CC=C2N=C3CCC=CN3C2=N1
141 374.1359991721 C#CC(O)=C=CN1C(=O)N=C([NH3+])C=C1N=C1CC=C2N=C3CCC=CN3C2=N1
142 375.1200147601 O=C1N=CC=C([NH+]=C2CC=C3N=C4CCC=CN4C3=N2)N1C=C=C(O)C#CO
143 374.1359991721 [NH3+]C1=NC(=O)N(CC#CC#CO)C(N=C2CC=C3N=C4CCC=CN4C3=N2)=C1
144 348.1203491081 [NH3+]C1=NC(=O)N(CC#CO)C(N=C2CC=C3N=C4C=CC=CN4C3=N2)=C1
145 346.1046990441 NC1=NC(=O)N([CH+]C#CO)C(N=C2CC=C3N=C4C=CC=CN4C3=N2)=C1
146 392.1465638561 N=C(O)N(C=C=C(O)C#CO)C(=C=C=[NH2+])N=C1CC=C2N=C3CCCCN3C2=N1 Intermediate Fragment 
147 354.1672993001 [NH3+]C1=NC(=O)N(CC#CO)C(N=C2CC=C3NC4CCCCN4C3=N2)=C1
148 336.1567346161 C#CCN1C(=O)N=C([NH3+])C=C1N=C1CC=C2N=C3CCCCN3C2=N1
149 352.1516492361 [NH3+]C1=NC(=O)N(CC#CO)C(N=C2CC=C3N=C4CCCCN4C3=N2)=C1
150 335.1251001401 O=C1N=CC=C([NH+]=C2CC=C3N=C4CCC=CN4C3=N2)N1CC#CO
151 334.1410845521 C#CCN1C(=O)N=C([NH3+])C=C1N=C1CC=C2N=C3CCC=CN3C2=N1
152 352.1516492361 [NH3+]C(=C=C(N=CC#CO)N=C1CC=C2NC3CCCCN3C2=N1)N=C=O Intermediate Fragment 
153 352.1516492361 N=C(O)N(CC#CO)C(=C=C=[NH2+])N=C1CC=C2N=C3CCCCN3C2=N1 Intermediate Fragment 
154 352.1516492361 [NH3+]C1=NC(=O)N(CC#CO)C(N=C2CC=C(N=C3CCCC=N3)C=N2)=C1 Intermediate Fragment 
155 333.1094500761 O=C1N=CC=C([NH+]=C2CC=C3N=C4C=CC=CN4C3=N2)N1CC#CO
156 332.1254344881 C#CCN1C(=O)N=C([NH3+])C=C1N=C1CC=C2N=C3C=CC=CN3C2=N1
157 350.1359991721 [NH3+]C(=C=C(N=CC#CO)N=C1CC=C2N=C3CCCCN3C2=N1)N=C=O Intermediate Fragment 
158 307.1301855201 N=C=C=C(N=C1CC=C2N=C3CCCCN3C2=N1)[NH+]=CC#CO
159 322.1410845521 NC(N)=C=C(N=C1CC=C2N=C3CCC=CN3C2=N1)[NH+]=CC#CO
160 350.1359991721 NC([NH3+])=C=C(N=C1CC=C2N=C3C=CC=CN3C2=N1)N(C=O)CC#CO Intermediate Fragment 
161 350.1359991721 N=C(O)N(CC#CO)C(=C=C=[NH2+])N=C1CC=C2N=C3CCC=CN3C2=N1 Intermediate Fragment 
162 350.1359991721 C=C(N=C1CC=C2N=C3CCC=CN3C2=N1)N(CC#CO)C(=O)N=C=[NH2+] Intermediate Fragment 
163 350.1359991721 C=C([NH3+])N=C(O)N(C=NC1=NC2=C(C=C1)N=C1C=CC=CN12)CC#CO Intermediate Fragment 
164 266.1036364241 OC#CC=[NH+]C=NC1=NC2=C(C=C1)N=C1CCC=CN12
165 350.1359991721 [NH3+]C(=C=C=NC1=NC2=C(C=C1)N=C1CCCCN12)N=C(O)N=CC#CO Intermediate Fragment 
166 253.1196208361 NC(=[NH2+])C=C=NC1=NC2=C(C=C1)N=C1CCC=CN12
167 350.1359991721 [NH3+]C1=NC(=O)N(CC#CO)C(N=C2CC=C(N=C3CC=CC=N3)C=N2)=C1 Intermediate Fragment 
168 330.1097844241 NC1=NCN([C+]=C=C=O)C(N=C2CC=C3N=C4C=CC=CN4C3=N2)=C1
169 331.0938000121 O=C1N=CC=C(N=C2CC=C3N=C4C=CC=CN4C3=N2)N1[CH+]C#CO
170 330.1097844241 [C+]#CCN1C(=O)N=C(N)C=C1N=C1CC=C2N=C3C=CC=CN3C2=N1
171 348.1203491081 [NH3+]C(=C=C(N=CC#CO)N=C1CC=C2N=C3CCC=CN3C2=N1)N=C=O Intermediate Fragment 
172 348.1203491081 N=C(O)N(CC#CO)C(=C=C=[NH2+])N=C1CC=C2N=C3C=CC=CN3C2=N1 Intermediate Fragment 
173 348.1203491081 C=C(N=C1CC=C2N=C3C=CC=CN3C2=N1)N(CC#CO)C(=O)N=C=[NH2+] Intermediate Fragment 
174 348.1203491081 C=C(N)N=C(O)N(C#[N+]C1=NC2=C(C=C1)N=C1C=CC=CN12)CC#CO Intermediate Fragment 
175 41.0021910721 [CH+]=C=O
176 63.0440558841 OCC[OH2+]
177 408.1414784761 [NH3+]C1=NC(=O)N(C=C=C(O)C(=O)C=O)C(N=C2CC=C3N=C4CCCCN4C3=N2)=C1
178 410.1571285401 [NH3+]C1=NC(=O)N(C(O)=C=C(O)C#CO)C(N=C2CC=C3NC4CCCCN4C3=N2)=C1 Intermediate Fragment 
179 342.1672993001 C=C(O)N1C(=O)N=C([NH3+])C=C1N=C1CC=C2NC3CCCCN3C2=N1
180 340.1516492361 C=C(O)N1C(=O)N=C([NH3+])C=C1N=C1CC=C2N=C3CCCCN3C2=N1
181 43.0178411361 C=C=[OH+]
182 55.0178411361 C#CC=[OH+]
183 326.1359991721 [NH3+]C1=NC(=O)N(C=O)C(N=C2CC=C3N=C4CCCCN4C3=N2)=C1
184 305.0781499481 O=C1N=CC=C(N=C2CC=C3N=C4C=CC=CN4C3=N2)N1C#[O+]
185 153.0407018201 NC1=CC(N)=NC(=O)N1C#[O+]
186 322.1046990441 [NH3+]C(=C=C(N=C=O)N=C1CC=C2N=C3CCC=CN3C2=N1)N=C=O Intermediate Fragment 
187 254.1036364241 C=C(N=C1CC=C2N=C3CCC=CN3C2=N1)[NH+]=C=O
188 279.0988853921 N=C=C=C(N=C1CC=C2N=C3CCC=CN3C2=N1)[NH+]=C=O
189 292.0941343601 N=C(N)[C+]=C(N=C=O)N=C1CC=C2N=C3C=CC=CN3C2=N1
190 322.1046990441 NC(N)=C=C(N=C1CC=C2N=C3C=CC=CN3C2=N1)N(C#[O+])C=O Intermediate Fragment 
191 322.1046990441 N=C(O)N(C=O)C(=C=C=[NH2+])N=C1CC=C2N=C3C=CC=CN3C2=N1 Intermediate Fragment 
192 87.0189037561 N=C(O)[NH+]=C=O
193 281.0781499481 N=C(O)[N+](=C=NC1=NC2=C(C=C1)N=C1C=CC=CN12)C=O
194 295.0938000121 C=C(N=C1CC=C2N=C3C=CC=CN3C2=N1)N(C#[O+])C(=N)O
195 305.0781499481 N=C=C=C(N=C1CC=C2N=C3C=CC=CN3C2=N1)N(C#[O+])C=O
196 322.1046990441 C=C(N=C1CC=C2N=C3C=CC=CN3C2=N1)N(C=O)C(=O)N=C=[NH2+] Intermediate Fragment 
197 280.0829009801 C=C(N=C1CC=C2N=C3C=CC=CN3C2=N1)N(C#[O+])C=O
198 322.1046990441 C=C(N)N=C(O)N(C#[O+])C=NC1=NC2=C(C=C1)N=C1C=CC=CN12 Intermediate Fragment 
199 322.1046990441 [NH3+]C(=C=C=NC1=NC2=C(C=C1)N=C1CCC=CN12)N=C(O)N=C=O Intermediate Fragment 
200 322.1046990441 NC1=NC(=O)N(C#[O+])C(N=C(N)C=CC2=CN3C=CC=CC3=N2)=C1 Intermediate Fragment 
201 322.1046990441 N=C1C=CC=CN1C1=NC(=NC2=CC(N)=NC(=O)N2C#[O+])CC=C1 Intermediate Fragment 
202 322.1046990441 NC1=NC(=O)N(C#[O+])C(N=C2CC=C(N=C3CC=CC=N3)C=N2)=C1 Intermediate Fragment 
203 80.0494756121 C1=CC=[NH+]C=C1
204 243.0624998841 N=C1C=CC(=NC2=CC(N)=NC(=O)N2C#[O+])N=C1
205 320.0890489801 NC1=NC(=O)N(C#[O+])C(N=C2C=CC(=NC3=NC=CC=C3)C=N2)=C1
206 322.1046990441 N=C1C=CC(=NC2=CC(N)=NC(=O)N2C#[O+])N=C1N1C=CC=CC1 Intermediate Fragment 
207 322.1046990441 C=CC1=NC2=CCC(=NC3=CC(N)=NC(=O)N3C#[O+])N=C2N1C=C Intermediate Fragment 
208 322.1046990441 C=C=C=CN1C=NC2=CCC(=NC3=CC([NH3+])=NC(=O)N3C=O)N=C21 Intermediate Fragment 
209 320.0890489801 N=C=C=C(N=C1CC=C2N=C3C=CC=CN3C2=N1)N(C#[O+])C(=N)O Intermediate Fragment 
210 87.0440558841 C=C(O)C=C[OH2+]
211 89.0597059481 C=C(O)CC[OH2+]
212 71.0491412641 C=CC(=C)[OH2+]
213 71.0491412641 C#CCC[OH2+]
214 91.0753560121 CC(O)CC[OH2+]
215 73.0647913281 C=C([OH2+])CC
216 368.1465638561 [NH3+]C1=NC(=O)N(C(=O)C#CO)C(N=C2CC=C3NC4CCCCN4C3=N2)=C1
217 350.1359991721 C#CC(=O)N1C(=O)N=C([NH3+])C=C1N=C1CC=C2N=C3CCCCN3C2=N1
218 366.1309137921 [NH3+]C1=NC(=O)N(C(=O)C#CO)C(N=C2CC=C(N=C3CCCC=N3)C=N2)=C1 Intermediate Fragment 
219 364.1152637281 [NH3+]C1=NC(=O)N(C(=O)C#CO)C(N=C2CC=C3N=C4CCC=CN4C3=N2)=C1
220 47.0491412641 CC[OH2+]
221 29.0385765801 C=[CH3+]
222 410.1571285401 [NH3+]C1=NC(=O)N(C(=C=CO)OC#CO)C(N=C2CC=C3NC4CCCCN4C3=N2)=C1 Intermediate Fragment 
223 410.1571285401 C=C(N=C1CC=C2NC3CCCCN3C2=N1)N(C(=O)N=C=[NH2+])C1C=C(O)C(=C=O)O1 Intermediate Fragment 
224 342.1560659201 C=C(N=C1CCC2NC3CCCCN3C2=N1)[NH+]=C1C=C(O)C(=C=O)O1
225 340.1404158561 C=C(N=C1CC=C2NC3CCCCN3C2=N1)[NH+]=C1C=C(O)C(=C=O)O1
226 410.1571285401 N=C(O)N(C(=C=C=[NH2+])N=C1CC=C2NC3CCCCN3C2=N1)C1C=C(O)C(=C=O)O1 Intermediate Fragment 
227 238.1087218041 [NH+]#CC=C=NC1=NC2=C(C=C1)N=C1CCCCN12
228 171.0400331241 NC(O)[NH+]=C1C=C(O)C(=C=O)O1
229 175.0713332521 NC(O)[NH2+]C1C=C(O)C(=CO)O1
230 367.1149293801 N=C(O)[NH+](C=NC1=NC2=C(C=C1)N=C1CCC=CN12)C1C=C(O)C(=C=O)O1
231 369.1305794441 N=C(O)[NH+](C=NC1=NC2=C(C=C1)N=C1CCCCN12)C1C=C(O)C(=C=O)O1
232 381.1305794441 C=C(N=C1CC=C2N=C3CCC=CN3C2=N1)[NH+](C(=N)O)C1C=C(O)C(=C=O)O1
233 383.1462295081 C=C(N=C1CC=C2N=C3CCCCN3C2=N1)[NH+](C(=N)O)C1C=C(O)C(=C=O)O1
234 366.1196804121 C=C(N=C1CC=C2N=C3CCC=CN3C2=N1)[NH+](C=O)C1C=C(O)C(=C=O)O1
235 383.1462295081 C=C(N=C1CC=C2N=C3CCCCN3C2=N1)[NH+](C(=N)O)C(O)=C=C(O)C#CO Intermediate Fragment 
236 393.1305794441 N=C=C=C(N=C1CC=C2N=C3CCCCN3C2=N1)[NH+](C=O)C1C=C(O)C(=C=O)O1
237 281.1145354561 N#CC=C(N=C1CC=C2N=C3CCC=CN3C2=N1)[NH2+]C=O
238 367.1513148881 N=C=C=C(N=C1CCC2NC3CCCCN3C2=N1)[NH+]=C1C=C(O)C(=C=O)O1
239 365.1356648241 N=C=C=C(N=C1CC=C2NC3CCCCN3C2=N1)[NH+]=C1C=C(O)C(=C=O)O1
240 410.1571285401 NC([NH3+])=C=C(N=C1CC=C2N=C3CCCCN3C2=N1)N(C=O)C1C=C(O)C(=C=O)O1 Intermediate Fragment 
241 382.1622139201 NC(N)=C=C(N=C1CC=C2NC3CCCCN3C2=N1)[NH+]=C1C=C(O)C(=C=O)O1
242 410.1571285401 [NH3+]C(=C=C(N=C1CCC2NC3CCCCN3C2=N1)N=C1C=C(O)C(=C=O)O1)N=C=O Intermediate Fragment 
243 410.1571285401 [NH3+]C(=C=C=NC1=NC2=C(CC1)NC1CCCCN21)N=C(O)N=C1C=C(O)C(=C=O)O1 Intermediate Fragment 
244 281.1145354561 NC(C#CN=C1CC=C2N=C3CCC=CN3C2=N1)=[NH+]C=O
245 410.1571285401 C=C([NH3+])N=C(O)N(C=NC1=NC2=C(C=C1)N=C1CCCCN12)C1C=C(O)C(=C=O)O1 Intermediate Fragment 
246 410.1571285401 NC(C=CC1=CN2CCCCC2=N1)=NC1=CC([NH3+])=NC(=O)N1C1C=C(O)C(=C=O)O1 Intermediate Fragment 
247 393.1305794441 [NH3+]C1=NC(=O)N(C2C=C(O)C(=C=O)O2)C(N=CC#CC2=CN3CCCCC3=N2)=C1
248 410.1571285401 C=CC1=C(N=C=NC2=CC([NH3+])=NC(=O)N2C2C=C(O)C(=C=O)O2)N2CCCCC2N1 Intermediate Fragment 
249 410.1571285401 N=C1C(=C=CC=NC2=CC([NH3+])=NC(=O)N2C2C=C(O)C(=C=O)O2)NC2CCCCN12 Intermediate Fragment 
250 410.1571285401 N=C1CCCCN1C1=NC(=NC2=CC([NH3+])=NC(=O)N2C2C=C(O)C(=C=O)O2)CC=C1 Intermediate Fragment 
251 393.1305794441 [NH3+]C1=NC(=O)N(C2C=C(O)C(=C=O)O2)C(N=C2CC=CC(N3C=CC=CC3)=N2)=C1
252 410.1571285401 [NH3+]C1=NC(=O)N(C2C=C(O)C(=C=O)O2)C(N=C2CC=C(N=C3CCCCN3)C=N2)=C1 Intermediate Fragment 
253 95.0603746441 [NH2+]=C1CC=CC=N1
254 316.1040303481 [NH3+]C1=NC(=O)N(C2=CC(O)C(=C=O)O2)C(N=C2CCCC=N2)=C1
255 316.1040303481 [NH3+]C1=NC(=O)N(C=C=C(O)C(=O)C=O)C(N=C2CCCC=N2)=C1 Intermediate Fragment 
256 327.0836292521 NC1=CCC(=NC2=CC([NH3+])=NC(=O)N2C2=CC(=O)C(=C=O)O2)N=C1
257 217.0832353281 N=C1C=CC(=NC2=NC(O)=NC([NH3+])=C2)N=C1
258 329.0992793161 NC1=CCC(=NC2=CC([NH3+])=NC(=O)N2C2=CC(O)C(=C=O)O2)N=C1
259 331.1149293801 NC1C=NC(=NC2=CC([NH3+])=NC(=O)N2C2=CC(O)C(=C=O)O2)CC1
260 331.1149293801 NC1C=NC(=NC2=CC([NH3+])=NC(=O)N2C=C=C(O)C(=O)C=O)CC1 Intermediate Fragment 
261 243.0988853921 C#CN1C(=O)N=C([NH3+])C=C1N=C1C=CC(N)C=N1
262 410.1571285401 NC1=CCC(=NC2=CC([NH3+])=NC(=O)N2C2C=C(O)C(=C=O)O2)N=C1N1C=CCCC1 Intermediate Fragment 
263 410.1571285401 CCCC1=NC2=CCC(=NC3=CC([NH3+])=NC(=O)N3C3C=C(O)C(=C=O)O3)N=C2N1C Intermediate Fragment 
264 410.1571285401 CCC1=NC2=CCC(=NC3=CC([NH3+])=NC(=O)N3C3C=C(O)C(=C=O)O3)N=C2N1CC Intermediate Fragment 
265 384.1414784761 CCC1NC2=CCC(=NC3=CC([NH3+])=NC(=O)N3C3C=C(O)C(=C=O)O3)N=C2N1
266 384.1414784761 CCC1NC2=CCC(=NC3=CC([NH3+])=NC(=O)N3C=C=C(O)C(=O)C=O)N=C2N1 Intermediate Fragment 
267 410.1571285401 CCCN1C(C)=NC2=CCC(=NC3=CC([NH3+])=NC(=O)N3C3C=C(O)C(=C=O)O3)N=C21 Intermediate Fragment 
268 410.1571285401 CCCCN1C=NC2=CCC(=NC3=CC([NH3+])=NC(=O)N3C3C=C(O)C(=C=O)O3)N=C21 Intermediate Fragment 
269 358.1258284121 [NH3+]C1=NC(=O)N(C2=CC(O)C(=C=O)O2)C(N=C2CCC3NCNC3=N2)=C1
270 360.1414784761 [NH3+]C1=NC(=O)N(C2=CC(O)C(=C=O)O2)C(N=C2CCC3NCNC3N2)=C1
271 360.1414784761 [NH3+]C1=NC(=O)N(C(O)=C=C(O)C#CO)C(N=C2CCC3NCNC3N2)=C1 Intermediate Fragment 
