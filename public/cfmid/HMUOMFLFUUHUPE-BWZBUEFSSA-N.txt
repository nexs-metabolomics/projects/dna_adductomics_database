#In-silico ESI-MS/MS [M+H]+ Spectra
#PREDICTED BY CFM-ID 4.0.8
#SMILES=O1[C@H](CO)[C@@H](C[C@@H]1n1cc(c(nc1=O)N)CO)O
#InChiKey=HMUOMFLFUUHUPE-BWZBUEFSSA-N
energy0
124.05054 100.00 39 35 (55.601 0.052902)
142.06110 23.22 2 46 61 55 58 63 40 (12.304 0.19905 0.16075 0.089009 0.072649 0.05255 0.042964)
258.10845 33.90 0 190 116 180 222 188 212 203 230 184 240 206 (17.8 0.69028 0.21039 0.057996 0.047183 0.026754 0.0084532 0.0071309 0.0071044 0.0061777 0.0055962 0.0011676)
energy1
45.03349 4.19 80 (1.8297)
57.03349 3.76 86 (1.6388)
99.04406 9.50 84 9 78 (2.5424 1.3395 0.26136)
99.05529 2.39 32 47 (0.85414 0.18864)
101.05971 2.64 10 99 97 (0.53271 0.32989 0.28733)
124.05054 100.00 39 35 (43.493 0.13968)
125.03455 3.25 36 53 66 (1.2577 0.087325 0.073132)
126.06619 3.41 34 (1.4879)
140.04545 3.03 8 57 45 70 (0.77345 0.42931 0.075255 0.043677)
142.06110 13.29 2 55 40 46 58 61 63 (5.2088 0.13735 0.13438 0.12026 0.10576 0.068029 0.025754)
154.06110 2.31 178 156 132 (0.51243 0.39997 0.09738)
166.06110 6.02 145 146 186 (2.4694 0.14889 0.007665)
168.07675 7.86 133 134 (2.8435 0.58412)
184.07167 2.74 124 147 149 151 148 (1.0182 0.12798 0.030134 0.012793 0.0058774)
196.07167 3.09 121 122 176 (0.6916 0.34406 0.31424)
222.08732 7.97 14 15 109 113 174 (2.7747 0.34685 0.28267 0.055376 0.015928)
240.09788 3.84 6 108 173 112 17 111 20 19 16 110 21 18 (0.71636 0.52195 0.10615 0.078739 0.05857 0.051121 0.047857 0.030089 0.022844 0.015344 0.014652 0.012829)
241.08190 2.36 227 101 102 (0.91717 0.098148 0.015058)
258.10845 3.27 0 116 184 190 222 180 188 212 203 230 240 206 (0.64049 0.58523 0.082667 0.042414 0.023793 0.018996 0.010625 0.0082054 0.0074053 0.0055113 0.00091791 0.0007304)
energy2
45.03349 12.01 80 (3.2572)
55.01784 3.68 65 (0.99754)
57.03349 10.53 86 (2.8537)
59.04914 3.85 93 167 (0.84865 0.19408)
71.04914 16.47 168 169 (3.094 1.3717)
73.06479 3.86 172 171 (0.52786 0.51798)
84.04439 5.97 140 (1.6188)
97.03964 10.27 43 48 (1.6434 1.1408)
99.05529 11.33 47 32 (1.8513 1.2214)
101.05971 4.90 10 97 99 (0.68451 0.61598 0.027444)
111.05529 2.98 235 236 (0.46703 0.34204)
112.05054 8.55 37 54 (2.2235 0.094161)
114.06619 3.82 44 38 (0.60151 0.4338)
124.05054 100.00 39 35 (26.63 0.48133)
125.03455 19.33 36 53 66 (4.8714 0.30404 0.066243)
126.06619 17.62 34 (4.7758)
130.09749 3.73 24 (1.0124)
142.06110 19.07 46 2 40 55 58 63 61 (1.8628 1.0662 0.87484 0.47281 0.44766 0.25252 0.19328)
152.08184 8.31 115 (2.2526)
154.06110 4.41 178 156 132 (0.70385 0.26981 0.22176)
168.07675 15.86 134 133 (2.2328 2.0671)
198.08732 7.38 175 (2.0003)
200.10297 2.99 183 (0.81142)

0 258.1084470321 [NH3+]c1nc(=O)n(C2CC(O)C(CO)O2)cc1CO
1 226.0822322841 [NH3+]C1=NC(O)N(C2=CC(O)=CO2)C=C1CO
2 142.0611029161 [NH3+]C1=C(CO)C=NC(O)=N1
3 228.0978823481 [NH3+]C1NC(O)N(C2=CC(O)=CO2)C=C1CO
4 210.0873176641 CC1=CN(C2=CC(O)=CO2)C(O)N=C1[NH3+]
5 230.1135324121 [NH3+]C1NC(O)N(C2=CC(O)=CO2)CC1CO
6 240.0978823481 C=C1OC(N2C=C(CO)C([NH3+])=NC2O)C=C1O
7 144.0767529801 [NH3+]C1=C(CO)C=NC(O)N1
8 140.0454528521 [NH3+]C1=C(C=O)C=NC(O)=N1
9 99.0440558841 C=C1OC=CC1[OH2+]
10 101.0597059481 C=C1OCCC1[OH2+]
11 103.0753560121 CC1OCCC1[OH2+]
12 210.0873176641 C=C1OC(N2C=CC([NH3+])=NC2O)C=C1O
13 212.1029677281 C=C1OC(N2C=CC([NH3+])NC2O)C=C1O
14 222.0873176641 C=C1OC(N2C=C(C)C([NH3+])=NC2=O)C=C1O
15 222.0873176641 C=C1C=CC(N2C=C(CO)C([NH3+])=NC2=O)O1
16 240.0978823481 C#COC(=C=CO)N1C=C(CO)C([NH3+])NC1O Intermediate Fragment 
17 240.0978823481 C=C(CO)OC(=C)N1C=C(CO)C([NH3+])=NC1=O Intermediate Fragment 
18 240.0978823481 CC(=O)C(O)=C=CN1C=C(CO)C([NH3+])=NC1O Intermediate Fragment 
19 240.0978823481 C=C1OC(N(C)C(O)=NC([NH3+])=C=CO)C=C1O Intermediate Fragment 
20 240.0978823481 C=C1OC(N(C=C(C=[NH2+])CO)C(=N)O)C=C1O Intermediate Fragment 
21 240.0978823481 C=C1OC(=NC=C(CO)C([NH3+])=NCO)C=C1O Intermediate Fragment 
22 148.1080531081 [NH3+]C1NC(O)NCC1CO
23 116.0818383601 [NH3+]C1CC=NC(O)N1
24 130.0974884241 CC1C=NC(O)NC1[NH3+]
25 148.1080531081 N=CC(CO)C([NH3+])NCO Intermediate Fragment 
26 148.1080531081 [NH3+]CNC(O)N=CCCO Intermediate Fragment 
27 148.1080531081 C=NC(O)NC([NH3+])CCO Intermediate Fragment 
28 87.0552892641 NC([NH3+])=C=CO
29 146.0924030441 [NH3+]C1NC(O)N=CC1CO
30 128.0818383601 CC1=C([NH3+])NC(O)N=C1
31 146.0924030441 N=C=C(CO)C([NH3+])NCO Intermediate Fragment 
32 99.0552892641 [NH+]#CC(=CN)CO
33 146.0924030441 C=NC(O)NC([NH3+])=CCO Intermediate Fragment 
34 126.0661882961 CC1=C([NH3+])N=C(O)N=C1
35 124.0505382321 [NH3+]C1=C(C=O)C=NC=N1
36 125.0345538201 O=CC1=CN=C([OH2+])N=C1
37 112.0505382321 [NH3+]C1=CC=NC(O)=N1
38 114.0661882961 [NH3+]C1=CC=NC(O)N1
39 124.0505382321 [CH2+]C1=C(N)N=C(O)N=C1
40 142.0611029161 N=C=C(CO)C([NH3+])=NC=O Intermediate Fragment 
41 73.0396392001 NC=[NH+]C=O
42 71.0239891361 N=C=[NH+]C=O
43 97.0396392001 N=C=C(C#[NH+])CO
44 114.0661882961 [NH+]#CC(CO)=C(N)N
45 140.0454528521 [NH+]#CC(CO)=C(N)N=C=O
46 142.0611029161 N=C([NH3+])C(=C=NC=O)CO Intermediate Fragment 
47 99.0552892641 C=C(C=O)C(N)=[NH2+]
48 97.0396392001 C=C(C#[O+])C(=N)N
49 56.0130901041 C#[N+]C=O
50 45.0447245801 NC=[NH2+]
51 43.0290745161 [NH+]#CN
52 100.0393048521 O=C[NH+]=C=CCO
53 125.0345538201 N#CC(=C=[NH+]C=O)CO
54 112.0505382321 [NH+]#CC(C=O)=C(N)N
55 142.0611029161 N=C(O)N=C=C(C=[NH2+])CO Intermediate Fragment 
56 72.0443902321 [NH+]#CCCO
57 140.0454528521 N#CC(=C=[NH+]C(=N)O)CO
58 142.0611029161 [NH3+]C=NC(=O)N=C=CCO Intermediate Fragment 
59 100.0505382321 C=[NH+]C(=O)N=CN
60 98.0236547881 O=C[NH+]=C=C=CO
61 142.0611029161 C=NC(O)=NC([NH3+])=C=CO Intermediate Fragment 
62 70.0287401681 [NH+]#CC=CO
63 142.0611029161 C=C(C=O)C([NH3+])=NC(N)=O Intermediate Fragment 
64 86.0348881681 N=C=NC([NH3+])=O
65 55.0178411361 C#CC=[OH+]
66 125.0345538201 C=C(C=O)C(N)=[N+]=C=O
67 122.0348881681 [CH+]=C1C=NC(O)=NC1=N
68 113.0345538201 NC(O)=[NH+]C=C=C=O
69 95.0239891361 N=C=C(C#[NH+])C=O
70 140.0454528521 [NH2+]=C=NC(=O)N=C=CCO Intermediate Fragment 
71 111.0076703761 O=C1C=COC1=C=[OH+]
72 113.0233204401 OC1C=COC1=C=[OH+]
73 115.0389705041 OC1CCOC1=C=[OH+]
74 87.0440558841 [OH2+]C1=COCC1
75 115.0389705041 CC=C(O)C(=O)C=[OH+] Intermediate Fragment 
76 117.0546205681 OC1CCOC1=C[OH2+]
77 89.0597059481 [OH2+]C1CCOC1
78 99.0440558841 [OH+]=C=C1CCCO1
79 117.0546205681 OCCC(O)C#C[OH2+] Intermediate Fragment 
80 45.0334912001 C=C[OH2+]
81 43.0178411361 C#C[OH2+]
82 87.0440558841 CC(O)C#C[OH2+]
83 89.0597059481 CC(O)C=C[OH2+]
84 99.0440558841 CC=C(O)C#C[OH2+]
85 117.0546205681 OCCCOC#C[OH2+] Intermediate Fragment 
86 57.0334912001 CC#C[OH2+]
87 117.0546205681 CCOC(=C=[OH+])CO Intermediate Fragment 
88 75.0440558841 OCC=C[OH2+]
89 73.0284058201 OCC#C[OH2+]
90 91.0389705041 OCC(=[OH+])CO
91 117.0546205681 COC(=C=[OH+])C(C)O Intermediate Fragment 
92 117.0546205681 CCC(O)C(=O)C=[OH+] Intermediate Fragment 
93 59.0491412641 CC=C[OH2+]
94 61.0284058201 OCC=[OH+]
95 29.0385765801 C=[CH3+]
96 119.0702706321 OC1CCOC1C[OH2+]
97 101.0597059481 [OH2+]C=C1CCCO1
98 119.0702706321 OCCC(O)C=C[OH2+] Intermediate Fragment 
99 101.0597059481 CCC(O)C#C[OH2+]
100 119.0702706321 OCCCOC=C[OH2+] Intermediate Fragment 
101 241.0818979361 OCC1=CN(C2C=C(O)C(=C=[OH+])O2)C(O)NC1
102 241.0818979361 O=C(C=[OH+])C(O)=C=CN1C=C(CO)CNC1O Intermediate Fragment 
103 226.0822322841 [NH3+]C1C=CN(C2C=C(O)C(=C=O)O2)C(O)N1
104 226.0822322841 [NH3+]C1C=CN(C=C=C(O)C(=O)C=O)C(O)N1 Intermediate Fragment 
105 228.0978823481 [NH3+]C1CCN(C2C=C(O)C(=C=O)O2)C(O)N1
106 228.0978823481 [NH3+]C1CCN(C(O)=C=C(O)C#CO)C(O)N1 Intermediate Fragment 
107 230.1135324121 [NH3+]C1CCN(C2CC(O)C(=C=O)O2)C(O)N1
108 240.0978823481 CC1=CN(C2C=C(O)C(=C=O)O2)C(O)NC1[NH3+]
109 222.0873176641 CC1=CN(C2C=CC(=C=O)O2)C(O)N=C1[NH3+]
110 240.0978823481 CC1=CN(C(O)=C=C(O)C#CO)C(O)NC1[NH3+] Intermediate Fragment 
111 240.0978823481 C=C(OC(=C=O)CO)N1C=C(C)C([NH3+])=NC1O Intermediate Fragment 
112 240.0978823481 CC1=CN(C=C=C(O)C(=O)C=O)C(O)NC1[NH3+] Intermediate Fragment 
113 222.0873176641 CC1=CN(C=C=C(O)C#CO)C(O)N=C1[NH3+]
114 180.0767529801 CC1=CN(CC#CO)C(=O)N=C1[NH3+]
115 152.0818383601 C#CN1C=C(C)C([NH3+])=NC1O
116 258.1084470321 [NH3+]C1NC(O)N(C(O)=C=C(O)C#CO)CC1CO Intermediate Fragment 
117 216.0978823481 [NH3+]C1NC(O)N(C(O)C#CO)C=C1CO
118 214.0822322841 [NH3+]C1=NC(O)N(C(O)C#CO)C=C1CO
119 172.0716676001 [NH3+]C1=NC(=O)N(CO)C=C1CO
120 170.0560175361 [NH3+]C1=NC(=O)N(CO)C=C1C=O
121 196.0716676001 CC1=CN(C(O)C#CO)C(=O)N=C1[NH3+]
122 196.0716676001 C#CC(O)N1C=C(CO)C([NH3+])=NC1=O
123 212.0665822201 [NH3+]C1=NC(=O)N(C(O)C#CO)C=C1CO
124 184.0716676001 C=C(O)N1C=C(CO)C([NH3+])=NC1=O
125 194.0560175361 CC1=CN(C(=O)C#CO)C(=O)N=C1[NH3+]
126 194.0560175361 C#CC(O)N1C=C(C=O)C([NH3+])=NC1=O
127 212.0665822201 [NH3+]C(=NC=O)C(=CN=C(O)C#CO)CO Intermediate Fragment 
128 47.0491412641 CC[OH2+]
129 190.1186177921 C=C(O)N1CC(CO)C([NH3+])NC1O
130 188.1029677281 C=C(O)N1C=C(CO)C([NH3+])NC1O
131 186.0873176641 C=C(O)N1C=C(CO)C([NH3+])=NC1O
132 154.0611029161 C=C(O)N1C=CC([NH3+])=NC1=O
133 168.0767529801 C=C(O)N1C=C(C)C([NH3+])=NC1=O
134 168.0767529801 C#CN1C=C(CO)C([NH3+])=NC1O
135 186.0873176641 C=C(O)N=C=C(CO)C([NH3+])=NCO Intermediate Fragment 
136 127.0502038841 C=C(C=O)C(N)=[NH+]C=O
137 141.0658539481 C=C(O)[NH+]=C=C(C=N)CO
138 186.0873176641 C=C(O)N(C=O)C=C(CO)C(=N)[NH3+] Intermediate Fragment 
139 186.0873176641 C=C(O)N(C=C(C=[NH2+])CO)C(=N)O Intermediate Fragment 
140 84.0443902321 C=C(C#[NH+])CO
141 143.0815040121 C=C(O)[NH+]=C=C(CN)CO
142 186.0873176641 C=C(O)N(C=CCO)C(=O)N=C[NH3+] Intermediate Fragment 
143 186.0873176641 C=C(O)N(C)C(O)=NC([NH3+])=C=CO Intermediate Fragment 
144 113.0345538201 NC(=C=CO)[NH+]=C=O
145 166.0611029161 [CH+]=C(O)N1C=C(C)C(N)=NC1=O
146 166.0611029161 C#CN1C=C(CO)C([NH3+])=NC1=O
147 184.0716676001 C=C(O)N=C=C(CO)C([NH3+])=NC=O Intermediate Fragment 
148 184.0716676001 C=C(O)N(C=O)C=C(C=O)C(=N)[NH3+] Intermediate Fragment 
149 184.0716676001 C=C(O)N(C=C(C#[NH+])CO)C(=N)O Intermediate Fragment 
150 157.0607685681 C=C(O)[NH+](C#CCO)C(=N)O
151 184.0716676001 C=C(O)N(C=CCO)C(=O)N=C=[NH2+] Intermediate Fragment 
152 182.0560175361 C=C(O)N1C=C(C=O)C([NH3+])=NC1=O
153 71.0127557561 O=CC#C[OH2+]
154 77.0597059481 OCCC[OH2+]
155 174.0873176641 [NH3+]C1=NC(O)N(CO)C=C1CO
156 154.0611029161 CC1=CN(C=O)C(=O)N=C1[NH3+]
157 152.0454528521 CC1=CN(C#[O+])C(=O)N=C1N
158 152.0454528521 CN1C=C(C#[O+])C(N)=NC1=O
159 170.0560175361 N=C([NH3+])C(C=O)=CN(C=O)C=O Intermediate Fragment 
160 170.0560175361 N=C(O)N(C=C(C#[NH+])C=O)CO Intermediate Fragment 
161 143.0451185041 N=C(O)[NH+](C#CC=O)CO
162 170.0560175361 C=C(C=O)C([NH3+])=NC(=O)N=CO Intermediate Fragment 
163 168.0403674721 [NH3+]C1=NC(=O)N(C=O)C=C1C=O
164 168.0403674721 N=C(O)N(C=O)C=C(C#[NH+])C=O Intermediate Fragment 
165 69.0334912001 C#CC(=C)[OH2+]
166 69.0334912001 C=CC#C[OH2+]
167 59.0491412641 C=C(C)[OH2+]
168 71.0491412641 C#CC(C)[OH2+]
169 71.0491412641 CCC#C[OH2+]
170 91.0753560121 CC(O)CC[OH2+]
171 73.0647913281 C=CC(C)[OH2+]
172 73.0647913281 CCC=C[OH2+]
173 240.0978823481 [NH3+]C1NC(O)N(C=C=C(O)C#CO)C=C1CO
174 222.0873176641 C#CC(O)=C=CN1C=C(CO)C([NH3+])=NC1O
175 198.0873176641 [NH3+]C1=NC(O)N(CC#CO)C=C1CO
176 196.0716676001 [NH3+]C1=NC(=O)N(CC#CO)C=C1CO
177 194.0560175361 [NH3+]C1=NC(=O)N(CC#CO)C=C1C=O
178 154.0611029161 CN1C=C(C=O)C([NH3+])=NC1=O
179 256.0927969681 [NH3+]C1NC(O)N(C(O)=C=C(O)C#CO)C=C1CO
180 258.1084470321 [NH3+]C1NC(O)N(C(=C=CO)OC#CO)CC1CO Intermediate Fragment 
181 228.0978823481 C=C(OC#CO)N1C=C(CO)C([NH3+])NC1O
182 63.0440558841 OCC[OH2+]
183 200.1029677281 [NH3+]C1NC(O)N(CC#CO)C=C1CO
184 258.1084470321 C=C(OC(=C=O)CO)N1C=C(CO)C([NH3+])NC1O Intermediate Fragment 
185 148.0505382321 [C+]#CN1C=C(C)C(N)=NC1=O
186 166.0611029161 C#CN(C=C(C#[NH+])CO)C(=N)O Intermediate Fragment 
187 170.0924030441 C#CN1C=C(CO)C([NH3+])NC1O
188 258.1084470321 C=C(O)C(=C=O)OCN1C=C(CO)C([NH3+])NC1O Intermediate Fragment 
189 152.0454528521 CN(C=C(C#N)C#[O+])C(=N)O Intermediate Fragment 
190 258.1084470321 [NH3+]C1NC(O)N(C=C=C(O)C(=O)C=O)CC1CO Intermediate Fragment 
191 202.1186177921 [NH3+]C1NC(O)N(CC#CO)CC1CO
192 174.1237031721 C=CN1CC(CO)C([NH3+])NC1O
193 172.1080531081 C#CN1CC(CO)C([NH3+])NC1O
194 89.0233204401 O=CC(=[OH+])CO
195 73.0284058201 CC(=[OH+])C=O
196 93.0546205681 OCC([OH2+])CO
197 75.0440558841 CC(=[OH+])CO
198 156.0767529801 CN1C=C(CO)C([NH3+])=NC1=O
199 138.0661882961 [CH2+]N1C=C(C)C(N)=NC1=O
200 105.0546205681 CC(O)C(=[OH+])CO
201 107.0702706321 CC(O)C([OH2+])CO
202 256.0927969681 [NH3+]C1NC(O)N(C=C=C(O)C(=O)C=O)C=C1CO
203 258.1084470321 CN(C(O)NC([NH3+])=CCO)C1C=C(O)C(=C=O)O1 Intermediate Fragment 
204 146.0811696641 C[NH2+]C1CC(O)C(=CO)O1
205 142.0498695361 C[NH2+]C1C=C(O)C(=C=O)O1
206 258.1084470321 [NH3+]CNC(O)N(C=CCO)C1C=C(O)C(=C=O)O1 Intermediate Fragment 
207 231.0975480001 NC(O)[NH+](CCCO)C1C=C(O)C(=C=O)O1
208 190.1073844121 OC=C1OC([NH2+]CCCO)CC1O
209 188.0917343481 O=C=C1OC([NH2+]CCCO)CC1O
210 188.0917343481 OC#CC(O)=CC(O)[NH2+]CCCO Intermediate Fragment 
211 186.0760842841 O=C=C1OC([NH2+]CCCO)C=C1O
212 258.1084470321 NC(O)N(C=C(C[NH3+])CO)C1C=C(O)C(=C=O)O1 Intermediate Fragment 
213 183.0400331241 C=[N+](C(=N)O)C1C=C(O)C(=C=O)O1
214 140.0342194721 C[NH+]=C1C=C(O)C(=C=O)O1
215 185.0556831881 C[NH+](C(=N)O)C1C=C(O)C(=C=O)O1
216 187.0713332521 C[NH+](C(N)O)C1C=C(O)C(=C=O)O1
217 144.0655196001 C[NH2+]C1CC(O)C(=C=O)O1
218 187.0713332521 C[NH+](C=C=C(O)C(=O)C=O)C(N)O Intermediate Fragment 
219 189.0869833161 C[NH+](C(N)O)C1CC(O)C(=C=O)O1
220 229.0818979361 NC(O)[NH+](C=CCO)C1C=C(O)C(=C=O)O1
221 213.0869833161 NCC(CO)C[NH+]=C1C=C(O)C(=C=O)O1
222 258.1084470321 NC([NH3+])C(=CN(CO)C1C=C(O)C(=C=O)O1)CO Intermediate Fragment 
223 158.0447841561 O=C=C1OC([NH2+]CO)C=C1O
224 160.0604342201 O=C=C1OC([NH2+]CO)CC1O
225 162.0760842841 OC=C1OC([NH2+]CO)CC1O
226 214.0709989041 O=C=C1OC([NH+](C=CCO)CO)C=C1O
227 241.0818979361 NCC(=C[NH+](C=O)C1C=C(O)C(=C=O)O1)CO
228 230.1135324121 NC(N)C(CO)C[NH2+]C1C=C(O)C(=C=O)O1
229 228.0978823481 NC(N)C(CO)C[NH+]=C1C=C(O)C(=C=O)O1
230 258.1084470321 [NH3+]C(NCO)C(CO)CN=C1C=C(O)C(=C=O)O1 Intermediate Fragment 
231 133.0971540761 C=C(CO)C(N)[NH2+]CO
232 131.0815040121 C=C(CO)C(N)=[NH+]CO
233 129.0658539481 C=C(CO)C(N)=[NH+]C=O
234 86.0600402961 C=C(C=[NH2+])CO
235 111.0552892641 C=[N+]=C(N)C(=C)C=O
236 111.0552892641 C=C(C)C(N)=[N+]=C=O
237 211.0713332521 NCC(=C[NH+]=C1C=C(O)C(=C=O)O1)CO
238 215.1026333801 NCC(CO)C[NH2+]C1C=C(O)C(=C=O)O1
239 215.1026333801 NCC(CO)C[NH2+]C=C=C(O)C(=O)C=O Intermediate Fragment 
240 258.1084470321 CC(CO)C([NH3+])NC(O)N=C1C=C(O)C(=C=O)O1 Intermediate Fragment 
