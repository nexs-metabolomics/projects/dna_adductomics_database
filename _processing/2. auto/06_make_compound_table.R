library(DT)
library(readxl)
library(readr)
library(dplyr)
library(tidyr)
library(htmlwidgets)
library(conflicted)

conflict_prefer("filter", "dplyr")

# system("cp -lr ChemDraw public/") # fast copy
#system("cp -lr mol public/") # fast copy




tab <- read_excel("public/Database for MS.xlsx",
                  col_types = c("text", rep("text", 4), rep("numeric", 3), rep("text", 10))
                  ) #%>% 
        # select( -contains("Reference"))



tab <- tab %>% 
          mutate(across("Structure",         ~if_else(is.na(.x),'',paste0('<a href="Chemdraw/',.x,'" download target="_blank">','Chemdraw','</a>')))) %>% 
          mutate(across(matches(".*- mol$"), ~if_else(is.na(.x),'',paste0('<a href="mol/',.x,'" download target="_blank">','mol','</a>')))) %>% 
          mutate(Reference = gsub("\u00A0", " ", Reference, fixed = TRUE)) %>% # replace non-breaking spaces
          mutate(Reference = if_else(grepl("DOI:", Reference, ignore.case = TRUE),paste0('<a href="http://dx.doi.org/',gsub("DOI:\\s*(.*)","\\1", Reference, ignore.case = TRUE),'" download target="_blank">',Reference,'</a>'), Reference)) %>% 
          mutate(Reference = if_else(grepl("^http", Reference, ignore.case = TRUE),paste0('<a href="',Reference,'" download target="_blank">','Link','</a>'), Reference))






DT <- tab %>% 
            select(Entry = "new entry", "Name", "Short name", "Alternative name", "Formula", "Monoisotopic mass", "Charged monoisotopic mass", "Charged monoisotopic mass -dR", "Source", "Adduct 1", "Structure", "Structure - mol", "SMILES", "InChI", "InChIKey", "Reference") %>% 
                datatable(
                          rownames = FALSE,
                          escape = FALSE, 
                          filter = "top",
                          class = 'display dataTable no-footer white-space: nowrap',
                          
                          options = list(pageLength = 25,
                                         buttons = c('copy', 'csv', 'excel'),
                                         dom = "lfBrtip"
                                         ),
                          extensions = 'Buttons',
                         ) %>% 
                formatRound(columns=c("Monoisotopic mass", "Charged monoisotopic mass", "Charged monoisotopic mass -dR"), digits=4)


dir.create("public")
saveWidget(DT, "public/index.html")

