library(purrr)
library(dplyr)
library(readxl)


dir.create("public/cfmid", recursive = TRUE, showWarnings = FALSE)

data <- read_excel("public/Database for MS.xlsx")


write("#!/bin/bash" ,  file = file("public/cfmid/cfmid_cmd.sh", "wb"), append=TRUE)





cmd <- data %>% 
          filter(!(InChIKey %in% gsub("\\.txt$", "",list.files("public/cfmid", "\\.txt$")))) %>% # Don't recalculate already done
          distinct(SMILES, InChIKey) %>% 
          {map2_chr(.$SMILES, .$InChIKey,  ~ paste0('cfm-predict \"', ..1, '\" 0.001 /trained_models_cfmid4.0/[M+H]+/param_output.log /trained_models_cfmid4.0/[M+H]+/param_config.txt 1 ', ..2,'.txt'))}



con <- file("public/cfmid/cfmid_cmd.sh", "wb")

paste(cmd, collapse = "\n") %>% 
write(file = con, append=TRUE)

close(con)


shell(paste0("bash -c '", 'docker run --rm=true -v "/$(pwd -W)/public/cfmid:/cfmid/public/" -i wishartlab/cfmid:latest sh -c "cd /cfmid/public/; sh ./cfmid_cmd.sh"', "'"))



unlink("cfmid/cfmid_cmd.sh")
